# Idea
## Efitter allows a user to:
- conveniently run fitting of multiple structures against multiple maps with multiple sets of parameters
- run fitting with alternative "backend" software, such as [Chimera Fit Map](https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/midas/fitmap.html) or [Situs colores](https://situs.biomachina.org/fguide.html#colores) (and PowerFit?) using the same user interface
- assess the statistical significance of the fits
- conveniently summarize and analyze the results (current bash_scripts)
- draw plots, generate reports
- if new maps or structures become available, allows for easy update of the fitting by simple adding of the new filenames to the project
- generate systematic fitting input for our integrative modeling pipeline

## The expected features of the software:
- share-able as a standalone package
- command line and script user interface 
- runs on linux workstation and a cluster
- usage manual [draft](README.md)
- [optional] interactive user interface of some sort (interactive commandline, Chimera plugin, web application)

## The expected implementation details:
- includes automated  test framework (e.g. unittest)
- makes further development by other or future members of the group relatively easy (documented code? readable code? Python instead of bash and R when possible?)

# Current program flow:
https://docs.google.com/drawings/d/1_xANRcLMJA554vyaOAES6_r_NjXMZp6mMKmIQL_pkRs/edit?usp=sharing

<img src="images/efitter_action_diagram.jpg">

# Fitting input and parameters
## Input
1. A list of **reference maps** - maps to **fit into** in MRC format
1. Query structures or EM maps to **be fitted**
   1. Atomic structures in PDB format
   1. EM maps in MRC format

## Fitting parameters
1. For each **map to fit into**:
   1. Optional density `threshold` . 
      The usage of this threshold depends on the backend fitting software.
      * For Chimera FitMap, if `envelope` option is set to `true`, it would be used for defining contour surface `inside` option, which is defined as: *inside < fraction >: The fraction is what proportion of fit atoms or fit map grid points must lie inside the reference map contour surface for the fit to be retained (default 0.1). The envelope setting determines whether all nonzero-valued fit map grid points or only those above the contour level (default) are considered
      * For Situs colores, it would be used for the `-cutoff` option which is *Density map cutoff value. All density levels below this value are set to zero. You can use volhist to rescale the density levels or to shift the background peak in the voxel histogram to the origin. [default -cutoff 0.0]*
   1. Optional `resolution` . 
      The usage of this threshold depends on the backend fitting software.
       * For Chimera FitMap, it would be used as a resolution for simulating the map from the fitted PDB structure (if `correlation` or `cam_score` is used)
       * For Situs colores, it would be used as a resolution for simulating the map from the fitted PDB structur **AND**, if **[-corr](https://situs.biomachina.org/fguide.html#colores) option is not specified** for defining whether the Laplacian filter should be applied to the map before fitting (automatically applied for resolution >10A if -corr is not specified)
   1. Symmetry specification. The symmetry information is used for clustering solutions, i.e. equivalent symmetry related fits will be clustered together (Chimera only)  
      **TODO** This is working but the specification is in a wrong place (in fitting parameters)
1. For each **PDB structure to be fitted**
   1. `CA_only` - whether to use on Calpha atoms of a protein
   1. `backbone_only` - whether to use on backbone atoms of a protein or DNA
   1. `subselection` - Chimera-only for now. Whether to restrict fitting to a subselection specified in Chimera atom_spec syntax e.g. `subselection &nucleic acid`. This may be handy - by default everything what is in the PDB files will be fitted, including waters, ions.
1. For each **EM map to be fitted**
   1. `move_to_center` - whether to move the query structures or EM maps to the center of the reference map (defined with (cofr command of Chimera)[https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/midas/cofr.html]) - only applicable to Chimera.
   1. `query_threshold` - density threshold for the query map to be used for cross-correlation calculations (for Chimera only if `envelope` is set to `true`.
1. Fitting parameters   
   A user can define multiple parameter sets that will be used for fitting runs with alternative options. For example, one parameter set can use `crosscorrelation` and another `overlap` as fitting score. The fitting script will automatically run all combinations of reference maps, queries, and parameter sets.   
   For each parameter set, all parameters supported by the backend software should be accessible, possibly in the raw format accepted by the software (string of options to be appended to the respective command).
    1. For Chimera FitMap, `fitmap_args` option is used for this.  
      Extra option:  
      `max` - maximum number of ouput fits to be saved in PDB or MRC format if `saveFiles true`
    1. For Situs colores, it is to be implemented, but it could be just a string like: `-corr 1 -deg 15`
1. Run parameters
   1. `cluster_submission_command`, optional, only for running on a cluster
   1. `run_script_templ`, required, templates provided in the documentation
      Current templates:
       * bash (for multicore machines)
       * [Slurm cluster queuing system](https://slurm.schedmd.com/documentation.html)
       * Other commonly used queuing: LSF, Condor (other https://en.wikipedia.org/wiki/Comparison_of_cluster_software#General_information). We don't have to support all, by adjusting `run_script_templ` and `cluster_submission_command`, a user could write a template for target queuing system, it is common practice, e.g. for RELION.

# Output
## For each fitting run (i.e. for each reference map x query x parameter set combination):
* **List of solutions with transformation matrices and fitting scores**, current `solutions.csv`
* **List of solutions with statistical evaluation columns added (Z-scores, p-values)**, current `solutions_pvalues.csv`
* **Plots from statistical evaluation (histograms, P-value plots)**, current `Rplots.pdf
* **Error messages from the backend fitting software**, current `log_err.txt
* **Output messages from the backend fitting software**, current `log_out.txt`
* **Script used to run the fitting software**, current `run.sh`
* **A symbolic link to query PDB or EM map**, current `ori_pdb.pdb`
* **A symbolic link to the reference EM map**, current `<reference_map_name>.mrc`

## Current directory structure
In the output, you should have got the following directory structure:

```
master_outdir/ # Directory specified with "master_outdir" parameter in params.py
    config_prefix1/ # named after config_prefix specifications in fitmap_args in params.py
        map1.mrc/   # named after the filenames of the EM maps used
            map1.mrc  # symbolic link to the reference map
            pdb_file_name1.pdb/     # named after the pdb file names used for fitting
                solutions.csv   # the list of solutions and their scores
                solutions_pvalues.csv
                Rplots.pdf
                log_err.txt     # standard error log
                log_out.txt     # standeard output log
                run.sh          # sbatch script used for running the job
                ori_pdb.pdb     # symbolic link to the original query file
                map1.mrc  # symbolic link to the reference map
            pdb_file_name2.pdb/
            pdb_file_name3.pdb/
            config.txt          # A config file for fitting, saved FYI.
        map2.mrc/
    config_prefix2/
    config_prefix3/
```

**!! Note: we may need to change this !!** This directory structure breaks down currenty if the users specifies EM maps with same filenames.

# Evaluation

## Example questions:
1. Which structures fit unambiguously?
   1. What is the p-value of the best fit for the given structure?
   1. Does the top hit have p-value much better than the next best hit?
1. If the p-values are not "significant", what could be the reasons?
   1. Was the number of fits for the background distribution sufficient?
   1. Is there anything wrong with the background distribution?
      1. The current test assumes normal distribution, was it so?
      1. Is the background distribution affected by specific type of fits? E.g. fits of protein into a membrane 
   1. Where there any warnings from the R script?
1. Which mirror image is correct?
   "More evaluation tools that you would like to have realized"
    * Was the sampling during fitting exhaustive?
    * Could we evaluate the statistical faster? E.g. colores can find best solutions very quickly but to generate all other fits for the background null distrubution takes a lot of time both with Chimera and colores. Is it possible to do it faster? E.g. using cross-correlation values from the fast on-grid search in colores? Or by resampling/jacknifing/bootstrapping the density voxels of the fit?
    * Use other scores to evaluate fits? E.g. many scores can be calculated for already existing solutions using TEMPy library http://tempy.ismb.lon.ac.uk/

## Example plots:
### Score histogram
<img src="images/example_score_histogram.png"  width="300">

### P-value plot
<img src="images/example_pvalue_plot.png"  width="300">

### Example fit summary for publication (usually assembled manually). From: In situ architecture of the algal nuclear pore complex, Supplementary Figures 7-9, https://www.nature.com/articles/s41467-018-04739-y
![](images/publication_fit_summary.jpg)
