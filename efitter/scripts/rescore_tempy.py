import sys
import csv
import math
from numpy import sum as numsum, copy as npcopy,mean as npmean, log10 as np_log10, median as npmedian
from numpy import square,sqrt,absolute,histogram,argwhere,amin,count_nonzero,shape,size, array as nparray,\
                  transpose, mgrid,indices,meshgrid,nonzero,real,searchsorted,newaxis,where,matrix,ravel,ma,\
                  amax,ones,arange,floor,ceil,zeros, conjugate


def scale_median(arr1,arr2):
    """
    Scale one list/array of scores with respect to another based on distribution around median.
    Arguments:
        *arr1, arr2*
            scale an array of scores (arr1) based on another (arr2).
    Return:
        scaled arr1.
    """
    scaled_arr = []
    nparr1 = nparray(arr1)
    nparr2 = nparray(arr2)
    med_dev_1 = npmedian(absolute(nparr1 - npmedian(nparr1)))
    #MI OVR    
    med_dev_2 = npmedian(absolute(nparr2 - npmedian(nparr2)))
    if med_dev_1 == 0.0: scale_factor = 0.0
    else: scale_factor = med_dev_2/med_dev_1
    shift_factor = npmedian(nparr2)-(scale_factor*npmedian(nparr1))
    
    #TODO: find a better way to avoid outliers in general
    if (max(nparr1) - min(nparr1)) > 0.1: 
            scaled_arr = ((scale_factor*nparr1+shift_factor) + nparr2)/2.
    else: scaled_arr = nparr2
    return scaled_arr
    
def read_solutions(solutions_filename, maxN=None):

    with open(solutions_filename, 'rU') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.readline(), [','])
        csvfile.seek(0)

        solutions = csv.DictReader(csvfile, dialect=dialect)

        for row in solutions:
            yield row

def get_fieldnames(solutions_filename):
    with open(solutions_filename, 'rU') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.readline(), [','])
        csvfile.seek(0)

        solutions = csv.DictReader(csvfile, dialect=dialect)

        return solutions.fieldnames

def main(solutions_filename, outfilename):
    out = []
    cam_scores = []
    ov_scores = []
    sccc_scores = []
    sccc_ov_scores = []
    for sol in read_solutions(solutions_filename):
        out.append(sol)
        cam_scores.append(float(sol['cam_score']))
        ov_scores.append(float(sol['OVR']))
        sccc_scores.append(float(sol['SCCC']))
        sccc_ov_scores.append(float(sol['SCCC_OV']))

    cam_ov_scores = scale_median(ov_scores,cam_scores)
    # print len(sccc_scores)
    # print len(cam_ov_scores)
    # for a, b in zip(cam_ov_scores, sccc_ov_scores):
    #     print a, b

    for sol, cam_ov in zip(out, cam_ov_scores):
        sol['CAM_OV'] = cam_ov


    with open(outfilename, 'w') as csvfile:
        fieldnames = get_fieldnames(solutions_filename)
        fieldnames.extend(['CAM_OV'])
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(out)



if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])



