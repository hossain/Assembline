import sys
from glob import glob
import os

files = [f for f in glob(sys.argv[1])]
print(files)

n_best = 5
score_idx = 1 #ccc
out = []

for fn in files:
    with open(fn) as f:
        f.next()
        for i in range(n_best):
            try:
                line = f.next()
                score = float(line.split()[score_idx])
                out.append([fn, i, score])

            except StopIteration:
                pass

for fit in sorted(out, key= lambda x: x[2]):
    print(os.path.dirname(fit[0])+'/fit_'+str(fit[1]+1)+'.pdb', fit[2])
