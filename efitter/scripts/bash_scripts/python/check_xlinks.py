#!/usr/bin/python

import sys
import os
from csv import DictReader,reader
from collections import defaultdict
from re import finditer
#never ever change these, same through pipeline
seq1id="QueryId"
seq2id="SubjectId"
seq1start="q.start"
seq1stop="q.end"
seq2start="s.start"
seq2stop="s.end"
_eval="e-value"
gaps="gapOpenings"
seq1="q.seq"
seq2="s.seq"
threshold=1e-40
protein1,protein2,position1,position2,xl_type ="Protein1","Protein2","AbsPos1","AbsPos2","XLType"
mono,intra,inter="monolink","intra-protein xl","inter-protein xl"

NA_RES_ID=-1
GAP_STRING="-"
TOL=1000

def lenient_int(_string):
	try:
		return int(_string)
	except:
		return NA_RES_ID

def intersects_with_tolerance(ranges,residue,tolerance):
	intersects = [l for l in map(lambda r:list(set(list(range(residue-tolerance,residue+tolerance))).intersection(set(list(range(r[0],r[1]))))),ranges)]
	any_intersect = any([bool(len(l)) for l in intersects])
	return any_intersect

if len(sys.argv)<5:
	print("No. You guessed the wrong number of arguments.")
	sys.exit()
else:
	xlink_seq_align_path = sys.argv[1]
	seq_struc_align_path = sys.argv[2]
	xlink_to_xlink_seq_mapping_path = sys.argv[3] 
	xlinks_path=sys.argv[4]	

if os.path.isfile(xlink_seq_align_path):
	xlink_seq_align_fh = open(xlink_seq_align_path,'r')
	xlink_seq_align = DictReader(xlink_seq_align_fh)
else:
	print("%s not a valid path."%xlink_seq_align_path)
	sys.exit()

if os.path.isfile(seq_struc_align_path):
	seq_struc_align = open(seq_struc_align_path,'r')
	seq_struc_align = DictReader(open(seq_struc_align_path,'r'))
else:
	print("%s not a valid path."%seq_struc_align_path)
	sys.exit()

if os.path.isfile(xlinks_path):
	xlinks_fh = open(xlinks_path,'r')
	xlinks = DictReader(xlinks_fh)
else:
	print("%s not a valid path."%xlinks_path)
	sys.exit()

if os.path.isfile(xlink_to_xlink_seq_mapping_path):
	xlink_to_xlink_seq_mapping_fh = open(xlink_to_xlink_seq_mapping_path,'r')
	xlink_to_xlink_seq_mapping = reader(xlink_to_xlink_seq_mapping_fh,delimiter='\t')
else:
	print("%s not a valid path."%xlink_to_xlink_seq_mapping_path)
	sys.exit()

#fh iterator needed just once
xl_to_xl_seq = dict([(line[0],line[1]) for line in xlink_to_xlink_seq_mapping])
#get xlink sequence ids, rely on xl id being the first!
xl_seq_to_seq_dict = defaultdict(str,[(entry[seq1id],entry[seq2id]) for entry in xlink_seq_align])
xlink_seq_align_fh.seek(0)
next(xlink_seq_align)
#create translation between indices of xlink sequences and sequences
xl_seq_index_diff = defaultdict(int)
xl_vs_seq_gaps=defaultdict(list)
for entry in xlink_seq_align:
	xlink_seq_id = entry[seq1id]
	seq_id = entry[seq2id]
	start_xlink_seq=int(entry[seq1start])
	stop_xlink_seq=int(entry[seq1stop])
	start_seq=int(entry[seq2start])
	stop_seq=int(entry[seq2stop])
	seq_seq=entry[seq2]
	xl_vs_seq_gaps[seq_id]=[m.start() for m in finditer(GAP_STRING,seq_seq)]
	xl_seq_index_diff[xlink_seq_id]=start_xlink_seq-start_seq
#create seq id to structured range dict from alignment
struc_range_dict = defaultdict(list)
for entry in seq_struc_align:
	seq1_id = entry[seq1id]
	seq2_id = entry[seq2id]
	range1 = [int(entry[seq1start]),int(entry[seq1stop])]
	range2 = [int(entry[seq2start]),int(entry[seq2stop])]
	#assumes the original sequences are under index 1, structures would be index 2. only keep the range with respect to 1 since this is the complete protein
	struc_range_dict[seq1_id].append(range1)

#TODO:check the mapping correctness. Convention: "xl_id from xl file"--[xlink_to_xlink_seq_mapping]-->"protein sequence id from xl_seq_file"
#--[xlink_seq_align:seq1id-->seq2id]-->"protein sequence_id from seq_file"

print(struc_range_dict)
passed = defaultdict(list)
for xlink in xlinks:
	prot1 = xlink[protein1]
	prot2 = xlink[protein2]
	_type = xlink[xl_type]
	xl_seq1_id=xl_to_xl_seq[prot1]
	xl_seq2_id=xl_to_xl_seq[prot2]
	seq1_id=xl_seq_to_seq_dict[xl_seq1_id]
	seq2_id=xl_seq_to_seq_dict[xl_seq2_id]
	seq1_diff=xl_seq_index_diff[xl_seq1_id]
	seq2_diff=xl_seq_index_diff[xl_seq2_id] 
	prot1_res = lenient_int(xlink[position1])-seq1_diff
	prot2_res = lenient_int(xlink[position2])-seq2_diff
	xlink_seq1_gaps = xl_vs_seq_gaps[seq1_id]
	xlink_seq2_gaps = xl_vs_seq_gaps[seq2_id]
	struct_ranges_seq1 = struc_range_dict[seq1_id]
	struct_ranges_seq2 = struc_range_dict[seq2_id]
	if _type == inter:
		print("xl-inter",prot1,prot2,xl_seq1_id,xl_seq2_id,seq1_id,seq2_id,prot1_res,prot2_res,struct_ranges_seq1,struct_ranges_seq2)
		#check if xlink ends map to a aligned gap in the seq seq
		if not (prot1_res in xlink_seq1_gaps or prot2_res in xlink_seq2_gaps):
			#check if xlink ends attach to strucs
			if intersects_with_tolerance(struct_ranges_seq1,prot1_res,TOL) and intersects_with_tolerance(struct_ranges_se2,prot2_res,TOL):
				passed[_type].append(xlink)
	elif _type == intra:
		#gaps should be the same as sequences are the same
		if not (prot1_res or prot2_res) in xlink_seq1_gaps:
			#same reasoning, prot1_res and prot2_res refer to the same prot in the case of intra link ofc
			if intersects_with_tolerance(struct_ranges_seq1,prot1_res,TOL) and intersects_with_tolerance(struct_ranges_seq1,prot2_res,TOL):
				passed[_type].append(xlink)
	elif _type == mono:
		#pick the right one
		prot_index = [_id in xl_to_xl_seq.keys() for _id in [prot1,prot2]].index(True)
		prot_res = [prot1_res,prot2_res][prot_index]
		xlink_seq_gaps = [xlink_seq1_gaps,xlink_seq2_gaps][prot_index]
		struct_ranges_seq = [struct_ranges_seq1,struct_ranges_seq2][prot_index]
		if not prot_res in xlink_seq_gaps:
			if intersects_with_tolerance(struct_ranges_seq,prot_res,TOL):
				passed[_type].append(xlink)

print("PASSED: ",passed)	
