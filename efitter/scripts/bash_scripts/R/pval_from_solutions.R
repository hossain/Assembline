#Authors: Bernd Klaus and Alessandro Ori, modified by Jan Kosinski @ EMBL
#Running:
#Rscript <path>/pval_from_solutions.R

args = commandArgs(trailingOnly=TRUE)
# test if there is at least one argument: if not, return an error

fn = args[1]
score = args[2]
out_fn = args[3]

library(fdrtool)
library(psych)

tryCatch(
	{
		hit_data <- read.table(fn, sep=",", header=TRUE)
	},error=function(e)
	{
		print(e)
		stop(sprintf("Could not read file %s.\n",fn))
	}
)

max_overlap = hit_data[1, "overlap"]
overlap_thresh = 0.0

## look at scores
hist(hit_data[,score], breaks=100)

## do a z-transform
hit_data$score_z <- fisherz(hit_data[,score])
hist(hit_data$score_z, freq = FALSE )

hit_data$score_z_c <- hit_data$score_z-mean(hit_data$score_z)
hist(hit_data$score_z_c)

fdrtool_res_shift <- fdrtool(hit_data$score_z_c , statistic =  "normal", cutoff.method="pct0", pct0=0.95)

hit_data$pvalues <- fdrtool_res_shift$pval
hit_data$BH_adjusted_pvalues <- p.adjust(fdrtool_res_shift$pval, method = "BH")
hit_data$BH_adjusted_pvalues_one_tailed <- ifelse(hit_data$score_z_c > 0, hit_data$BH_adjusted_pvalues/2, 1-hit_data$BH_adjusted_pvalues/2)

plot(sort(hit_data$pvalues))
plot(sort(hit_data$BH_adjusted_pvalues))

write.csv(hit_data, out_fn)
boxplot(hit_data$score_z_c)
boxplot.stats(hit_data$score_z_c)
