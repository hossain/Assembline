#!/bin/bash
origin=`pwd`
usage="Usage: $0 [pdb directory] [protein sequence file]"
#=============================================================================
# input data
#=============================================================================
# check number of arguments
if [ ! $# -eq 2 ]
then
    echo "Argument error" 1>&2
    echo $usage 1>&2
    exit 1
fi
# check first argument is an existing regular file
if [ ! -d $1 ] && [ ! -f $2 ]
then
    echo "$1 is not a regular file" 1>&2
    echo $usage 1>&2
    exit 1
fi

TDIR=`mktemp -d`
trap "{ cd - ; rm -rf $TDIR; exit 255; }" SIGINT

IN_PDB_DIR=`readlink -f $1`;
FASTA_FILE=`readlink -f $2`;
FASTA_NAME=`basename $FASTA_FILE`;
IN_PDB_FILES=`ls $IN_PDB_DIR | grep .pdb`;
PROTEINS="proteins";
PDBS="pdbs";
OUT="blast_out.txt";
ERROR="blast.log";
echo "Testing $IN_PDB_DIR against $FASTA_FILE ...";
cd $TDIR
mkdir $PROTEINS;
mkdir $PDBS;
awk -v tdir="$TDIR/$PROTEINS" -v fname=$FASTA_NAME '/^>/ {cnt++;F=tdir"/"fname"."cnt;print $0 > F}! /^>/ {print > F}' $FASTA_FILE
for pdb in $IN_PDB_FILES;do
	pdb2fasta $IN_PDB_DIR/$pdb | awk -v pdbs=$PDBS 'BEGIN {FS="|";} /^>/ {trim=substr($1,2,length($1)); chain=$2; sub(/ /,"_",chain);F=pdbs"/"trim"_"chain".fasta";print $0 > F} ! /^>/{print > F}';
done
for pdb_seq in `ls $TDIR/$PDBS`;
do
	for prot_seq in `ls $TDIR/$PROTEINS`;
	do	
		bl2seq -i $PDBS/$pdb_seq -j $PROTEINS/$prot_seq -p blastp -D 1 2> $ERROR | awk -v out=$OUT '!/^#/ {print $0 >> out}'
	done
done
mv $OUT $origin
mv $ERROR $origin
rm -rf $TDIR
cd $origin
exit 0

