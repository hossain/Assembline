#adapted from R script

#fisher_x = 1/2*(log(1+cam)-log(1-cam))
#x = fisher_x - mean(fisher_x)
#x0 = quantile(abs(x), probs=0.95)
#x.cens = x[ abs(x) <= x0 ]
#f0 = function(x, param, log=FALSE){return(dnorm(x, sd=param, log=log))}
#F0 = function(x, param){return( pnorm(x, sd=param))}
#get.pval = function(x, param)
#    {
#        return( ifelse(abs(x)==Inf, 0, pmax(.Machine$double.eps, 2-2*F0(abs(x), param))))  
#    }
#nlogL = function(pp)
#{
#out = rep(0, length(pp))
#for (i in 1:length(pp)){out[i] = length(x.cens)*log(1-get.pval(x0, pp[i]))- sum(f0(x.cens, pp[i], log=TRUE))}
#return(out)
#}
#start = as.double(diff(quantile(x.cens, probs=c(.25, .75))))/(2*qnorm(.75))
#param = optimize(nlogL, lower=start/1000, upper=start*1000)$minimum
##what's needed
#pval = get.pval(x, param)

import numpy
import csv
import math
from collections import defaultdict
from scipy.stats import norm
from scipy.optimize import  minimize
from scipy.optimize import  minimize_scalar

nlog = math.log
def fisherz(x):
        try:
            return 0.5*(nlog(1+x)-nlog(1-x))
        except:
            return x

def pvalues(fn):
	reader = csv.DictReader(open(fn))
	keys = reader.fieldnames
	score_keys = [k for k in keys if "_score" in k]
	score_dict = defaultdict(list)
	pval_dict = defaultdict(list)
	for line in reader:
		for sk in score_keys:
			score_dict[sk].append(float(line[sk]))
	for sk in score_keys:
		scores = score_dict[sk]
		scores = list(map(fisherz,scores))
		scores = list(map(lambda s: s-numpy.mean(scores),scores))
		x0 = numpy.quantile(numpy.abs(scores),0.95)
		scorescens = [s for s in scores if abs(s) <= x0]
		F0 = lambda x,param: norm.cdf(x,scale=param)
		f0 = lambda x,param,log: norm.pdf(x,scale=param) if not log else norm.logpdf(x,scale=param)
		getpval = lambda x,param: 0 if abs(x) == float('inf') else max(numpy.finfo(float).tiny,2-2*F0(abs(x),param))
		nlogL = lambda pp: len(scorescens)*numpy.log(1-getpval(x0,pp))-sum([f0(e,pp,True) for e in scorescens])
		start = (numpy.quantile(scorescens,0.75) - numpy.quantile(scorescens,0.25))/(2*norm.ppf(0.75))
		param = minimize_scalar(nlogL,bracket=[start/10,start,start*10]).x
		pvals = [getpval(e,param) for e in scores]
		pval_dict[sk+"_pvalues"] = pvals
	nkeys = keys
	for sk in score_keys:
		nkeys.insert(nkeys.index(sk)+1,sk+"_pvalues")
	writer = csv.DictWriter(open(fn[:-4]+"_pvalues.csv",'w'),fieldnames = nkeys)
	writer.writeheader()
	reader = csv.DictReader(open(fn))
	for i,line in enumerate(reader):
		nline = dict([(k,v) for k,v in line.items()])
		for k,v in pval_dict.items():
			nline[k] = v[i]
		writer.writerow(nline)
	return score_dict	
