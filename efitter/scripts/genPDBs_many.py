#!/usr/bin/python
import glob
import os
from optparse import OptionParser
import subprocess
import sys
# sys.path.append(os.path.join(os.path.dirname(__file__)))
# print(sys.path)
import genPDBs

def main():
    usage = "usage: %prog [options] outdir <solutions_list>\n"
    usage += """
E.g.:
* Generate fits for all maps:
Enter the "parameters set" fit directory like search100000_metric_cam_inside0.3_radius500/ and run:
%prog -n5 top5 */*/solutions.csv
this will generate a director top5 with subdirectories for each map, and top 5 fits for each map.

* Generate fits for a specific map:
Enter the directory for specific map like search100000_metric_cam_inside0.3_radius500/P_negstain_01.mrc and run:
%prog -n5 top5 */solutions.csv
this will generate a director top5 with top 5 fits for each map.
"""
    parser = OptionParser(usage=usage)

    parser.add_option("-n", type="int", dest="no_of_hits", default=1,
                      help="Number of hits [default: %default]")

    (options, args) = parser.parse_args()

    if len(sys.argv[1:]) == 0:
        parser.print_help()
        sys.exit()

    top_outdir = args[0]
    if not os.path.exists(top_outdir):
        os.makedirs(top_outdir)

    fns = args[1:]

    for fn in fns:
        # print(fn)
        dirname = os.path.dirname(fn)
        topdirname = os.path.dirname(dirname)
        # print(topdirname)
        outdir = os.path.join(top_outdir, topdirname)
        print(outdir)
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        genPDBs.genPDBs(
            fn,
            os.path.join(dirname, 'ori_pdb.pdb'),
            outdir=outdir,
            no_of_hits=options.no_of_hits
            )

if __name__ == '__main__':
    main()