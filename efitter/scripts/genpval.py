#!/usr/bin/python

import os
import subprocess

SOLUTIONS_FN= "solutions.csv"
SOLUTIONS_PVAL_FN= "solutions_pvalues.csv"
scripts_path = os.path.dirname(os.path.abspath(__file__))
PVALUE_SCRIPT= os.path.join(scripts_path,"pval_from_solutions.R")
HISTOGRAM_SCRIPT= os.path.join(scripts_path,"draw_solutions_histogram.py")
def generate_pvalues(base_dir):
	if (os.path.exists(PVALUE_SCRIPT) and os.path.exists(HISTOGRAM_SCRIPT)):
		pass
	else:
		print("ERROR! ERROR! ABORT EXECUTION!")
	if os.path.exists(base_dir):
		for r,d,fs in os.walk(base_dir):
    			for f in fs:
        			if f == SOLUTIONS_FN:
            				subprocess.call(["Rscript",PVALUE_SCRIPT,f,"cam_score",os.path.join(r,SOLUTIONS_PVAL_FN)], cwd=r)
            				#commented out due to issues with the call pattern used in the script and pyplot.histogram
					#subprocess.call(["python",HISTOGRAM_SCRIPT,os.path.join(r,f),os.path.join(r,"histogram.png")])
	
	else:
		print("ERROR! ERROR! ABORT EXECUTION!")

if __name__ == "__main__":
	import sys
	if len(sys.argv) == 2:
		generate_pvalues(sys.argv[1])	


