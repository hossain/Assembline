#!/usr/bin/python
import sys
import subprocess
import glob
import os
import itertools
import imp
import os
import os.path
import tempfile
import time
import errno
import shutil
from string import Template
import socket
if sys.version_info[0] < 3:
    input = raw_input
import argparse
import efitter

def is_string(query):
    if sys.version_info[0] < 3:
        return isinstance(query, basestring)
    else:
        return isinstance(query, str)

def load_from_file(filepath):
    mod_name,file_ext = os.path.splitext(os.path.split(filepath)[-1])

    if file_ext.lower() == '.py':
        py_mod = imp.load_source(mod_name, filepath)

    elif file_ext.lower() == '.pyc':
        py_mod = imp.load_compiled(mod_name, filepath)

    return py_mod

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def rm(path):
    if os.path.exists(path):
        shutil.rmtree(path)

def symlink(target, link_name, overwrite=False):
    '''
    Create a symbolic link named link_name pointing to target.
    If link_name exists then FileExistsError is raised, unless overwrite=True.
    When trying to overwrite a directory, IsADirectoryError is raised.

    Based on: https://stackoverflow.com/questions/8299386/modifying-a-symlink-in-python/55742015#55742015
    '''

    if not overwrite:
        os.symlink(target, link_name)
        return

    # os.replace() may fail if files are on different filesystems
    link_dir = os.path.dirname(link_name)

    # Create link to target with temporary filename
    while True:
        temp_link_name = tempfile.mktemp(dir=link_dir)

        # os.* functions mimic as closely as possible system functions
        # The POSIX symlink() returns EEXIST if link_name already exists
        # https://pubs.opengroup.org/onlinepubs/9699919799/functions/symlink.html
        try:
            os.symlink(target, temp_link_name)
            break
        except FileExistsError:
            pass

    # Replace link_name with temp_link_name
    try:
        # Pre-empt os.replace on a directory with a nicer message
        if not os.path.islink(link_name) and os.path.isdir(link_name):
            raise IsADirectoryError(f"Cannot symlink over existing directory: '{link_name}'")
        os.replace(temp_link_name, link_name)
    except:
        if os.path.islink(temp_link_name):
            os.remove(temp_link_name)
        raise

def save_script(pdb_outdir, run_script): 
    with open(os.path.join(pdb_outdir, 'run.sh'), 'w') as f:
        f.write(run_script)

parser = argparse.ArgumentParser(description="Efitter")
parser.add_argument('params_file', help='Parameter file name')
parser.add_argument('--update_links', dest="update_links", help='Update symbolic links to files like ori_pdb.pdb', action='store_true', default=False)

script_args = parser.parse_args()


params = load_from_file(script_args.params_file)
if not hasattr(params, 'run_crashed_only'):
    params.run_crashed_only = False

used = []
for cfg in params.fitmap_args:
    if cfg['config_prefix'] in used:
        print("Duplicated config prefiex in fitmap_args, duplicate is: {0}".format(cfg['config_prefix']))
        sys.exit()
    used.append(cfg['config_prefix'])



combs = []
for combination in itertools.product(params.MAPS, params.fitmap_args, params.PDB_FILES):
    combs.append(
        {
            'map': combination[0],
            'fitmap_args': combination[1],
            'pdb_file': combination[2]
        }
        )


if hasattr(params, 'CA_only') and params.CA_only: #TODO: for chimera this could be done by @CA selector
    if not params.dry_run:
        subprocess.call('mkdir -p tempmodels', shell=True)
    for f in params.PDB_FILES:
        path = os.path.join(params.models_dir, f)
        outf = os.path.join('tempmodels', f)
        if not params.dry_run:
            subprocess.call("egrep '.{{13}}CA' {0} > {1}".format(path, outf), shell=True)

    params.models_dir = 'tempmodels'

if not hasattr(params, 'run_script_templ'):
    if params.cluster_submission_command.startswith('sbatch'):

        setattr(params, 'run_script_templ', Template("""#!/bin/bash
#
#SBATCH --job-name=$job_name
#SBATCH --time=1-00:00:00
#SBATCH --error $pdb_outdir/log_err.txt
#SBATCH --output $pdb_outdir/log_out.txt
#SBATCH --mem=1000

$cmd
"""))
    else:
        setattr(params, 'run_script_templ', Template("""#!/bin/bash
#
echo $job_name
$cmd &>$pdb_outdir/log&
"""))

if socket.gethostname() != 'login.cluster.embl.de': #For backward compatibility 
    if 'SBATCH' in params.run_script_templ.safe_substitute() and (not hasattr(params, 'cluster_submission_command') or not params.cluster_submission_command.startswith('sbatch')):
        var = input("""run_script_templ in your '{0}' file:
    ------
    {1}
    ------
    looks like sbatch script,
    but cluster_submission_command is not defined as 'sbatch', proceed? [y/n]?: """.format(sys.argv[1], params.run_script_templ.safe_substitute()))
        if var != 'y':
            print("Exiting")
            sys.exit()

if socket.gethostname() == 'login.cluster.embl.de': #For backward compatibility 
    if 'SBATCH' in params.run_script_templ.safe_substitute() and (not hasattr(params, 'cluster_submission_command') or not params.cluster_submission_command.startswith('sbatch')):
        setattr(params, 'cluster_submission_command', 'sbatch')

for comb in combs:
    comb['cmds'] = []
    outdir = os.path.join(params.master_outdir, comb['fitmap_args']['config_prefix'], str(comb['map']))
    if params.method == 'chimera':
        config_fn = os.path.join(
            outdir,
            'config.txt'
            # '{0}_{1}.txt'.format(comb['fitmap_args']['config_prefix'], str(comb['map']))
            )


    # comb['cmds'].append('mkdir -p {0}'.format(outdir))
    # cmd = 'mkdir -p {0}'.format(outdir)
    # subprocess.call(cmd, shell=True)
    mkdir_p(outdir)
    if params.method == 'chimera':
        config_str = comb['fitmap_args']['template'].substitute(
                threshold=comb['map'].threshold,
                map=comb['map'].filename,
                resolution=comb['map'].resolution
            )

        with open(config_fn, 'w') as f:
            f.write(config_str) #TODO: write it to the .pdb directories?

    pdb_outdir = os.path.join(outdir, comb['pdb_file'])
    comb['pdb_outdir'] = pdb_outdir

    # comb['cmds'].append('rm -rv {0}'.format(pdb_outdir))
    comb['cmds'].append([rm, (pdb_outdir,)])

    # comb['cmds'].append('mkdir -p {0}'.format(pdb_outdir))
    comb['cmds'].append([mkdir_p, (pdb_outdir,)])

    ref_pdb_file = None
    if hasattr(params, 'ref_pdb_file'):
        ref_pdb_file = params.ref_pdb_file

    ref_models_dir = None
    if hasattr(params, 'ref_models_dir'):
        ref_models_dir = params.ref_models_dir

    start_pos = ''
    if ref_pdb_file and ref_models_dir:
        start_pos = '--ref {ref_models_dir}/{ref_pdb_file}'.format(ref_models_dir, ref_pdb_file)

    move_to_center = False
    if hasattr(params, 'move_to_center') and params.move_to_center:
        start_pos = '--move_to_center'

    chimera_opts = ''
    backbone_only = False
    if hasattr(params, 'backbone_only') and params.backbone_only:
        chimera_opts = '--backbone_only'

    job_name = '_'.join([comb['fitmap_args']['config_prefix'], str(comb['map']),comb['pdb_file']])

    if params.method == 'chimera':


        scripts_path = os.path.dirname(os.path.abspath(__file__))
        efitter_path = os.path.dirname(scripts_path)
        chimerafitmap_path = os.path.join(os.path.dirname(os.path.abspath(efitter.__file__)), 'chimerafitmap.py')
        cmd = '{scripts_path}/fit_with_chimera --chimerafitmap_path {chimerafitmap_path} {start_pos} {opts} {models_dir}/{pdb_file} {pdb_outdir} {config}'.format(
            pdb_file=comb['pdb_file'],
            pdb_outdir=pdb_outdir,
            config=config_fn,
            models_dir=params.models_dir,
            start_pos=start_pos,
            ref_pdb_file=ref_pdb_file,
            opts=chimera_opts,
            scripts_path=scripts_path,
            chimerafitmap_path=chimerafitmap_path
            )

        
        run_script = params.run_script_templ.substitute(job_name=job_name, cmd=cmd, pdb_outdir=pdb_outdir)
        comb['cmds'].append([save_script, (pdb_outdir, run_script)])

        comb['cmds'].append([symlink, (os.path.abspath(os.path.join(params.models_dir, comb['pdb_file'])), os.path.join(pdb_outdir, 'ori_pdb.pdb'), True,)])
        comb['cmds'].append([symlink, (os.path.abspath(comb['map'].filename), os.path.join(pdb_outdir, os.path.basename(comb['map'].filename)), True,)])
        comb['cmds'].append([symlink, (os.path.abspath(comb['map'].filename), os.path.join(os.path.dirname(pdb_outdir), os.path.basename(comb['map'].filename)), True,)])

        if hasattr(params, 'cluster_submission_command'):
            comb['cmds'].append(params.cluster_submission_command+' '+os.path.join(pdb_outdir, 'run.sh'))
        else:
            comb['cmds'].append('sh '+os.path.join(pdb_outdir, 'run.sh'))

    # TODO: not supported at the moment
    # elif params.method == 'powerfit':
    #     if hasattr(params, 'out_models_num'):
    #         out_models_num = params.out_models_num
    #     else:
    #         out_models_num = 10

    #     cmd = '{submission_command} -e {pdb_outdir}/log_err.txt  -o {pdb_outdir}/log_out.txt -J {job_name} "powerfit {map} {resolution} {models_dir}/{pdb_file} -d {pdb_outdir} -n 10 -p 1 &> {pdb_outdir}/log.txt"'.format(
    #         pdb_file=comb['pdb_file'],
    #         pdb_outdir=pdb_outdir,
    #         models_dir=params.models_dir,
    #         job_name=job_name,
    #         map=comb['map'].filename,
    #         resolution=comb['map'].resolution,
    #         out_models_num=out_models_num,
    #         submission_command=submission_command
    #         )

        # comb['cmds'].append(cmd)

combs_to_run = []
for comb in combs:
    if params.run_crashed_only:
        if not os.path.isfile(os.path.join(comb['pdb_outdir'], 'solutions.csv')):
            combs_to_run.append(comb)
    else:
        combs_to_run.append(comb)

#TODO:
#Check if exceeds ulimit -u (the number of spawned subprocesses)

if not script_args.update_links:
    var = input("The script will now run {0} jobs, proceed? [y/n]?: ".format(len(combs_to_run)))
    if var != 'y':
        print("Exiting")
        sys.exit()

for comb in combs_to_run:

    for cmd in comb['cmds']:
        ## Temporary code for adding links to existing jobs
        if script_args.update_links:
            if cmd[0] != symlink:
                continue

        if is_string(cmd):
            print(cmd)
            if not params.dry_run:
                subprocess.call(cmd, shell=True)
        else:
            fn = cmd[0]
            args = cmd[1]

            if not params.dry_run:
                fn(*args)
            else:
                print(fn, args)

if not script_args.update_links:
    print()
    if hasattr(params, 'cluster_submission_command'):
        print('{0} fitting jobs have been submitted to the queue.'.format(len(combs_to_run)))
    else:
        print('{0} fitting jobs have been run in the background.'.format(len(combs_to_run)))

    print()
    print('Check "{0}" directory for progress and output'.format(params.master_outdir))
    print()
