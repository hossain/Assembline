'''
First run:
grep -r "Unnormalized correlation coefficient" */*/col_best_00?.pdb > fit_scores.txt
'''
import re
import itertools
import os
import ntpath

fn = 'fit_scores.txt'

fits = []
with open(fn) as f:
    for line in f:
        m = re.match('(?P<fit_dir>.*):REMARK    Unnormalized correlation coefficient: (?P<score>.*)\n', line)
        if m:
            dirname, fitname = m.groups()[0].rsplit('/', 1)
            score = float(m.groups()[1])
            fits.append((dirname, fitname, score))

for dirname, group in itertools.groupby(fits, lambda x: x[0]):
    fits = list(group)
    target, query = dirname.split('/')
    target = ntpath.basename(target)
    query = ntpath.basename(query)

    target_no_ext = os.path.splitext(target)[0]
    query_no_ext = os.path.splitext(query)[0]
    if target_no_ext == query_no_ext:
        continue

    # print(target_no_ext, query_no_ext)
    if len(fits) > 1:
        difference = (fits[0][2]-fits[1][2])/fits[0][2]
        if difference > 0.1:
            print(difference, fits)
            cmd = 'chimera {target} {fit1} {fit2}'.format(target=os.path.join('/g/kosinski/kosinski/PFC/PFCmodeling/EM/', target),
                fit1=os.path.join(dirname, fits[0][1]),
                fit2=os.path.join(dirname, fits[1][1]),)
            print(cmd)
            print('-----')
    else:
        print(fits)



