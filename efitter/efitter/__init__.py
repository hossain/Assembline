import ntpath

class Map(object):
    def __init__(self, filename, threshold = None, resolution = None):
        self.filename = filename
        self.threshold = threshold
        self.resolution = resolution

    def __str__(self):
        return ntpath.basename(self.filename)

    def __repr__(self):
        return 'Map({0})'.format(ntpath.basename(self.filename))
