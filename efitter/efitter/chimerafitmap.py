import os.path
import re

import chimera
from chimera import runCommand as rc
from VolumeViewer.volumecommand import volume

from FitMap import fitcmd
import Midas

class SymmetryConfigNotComplete(Exception): pass

class Runner(object):
    def __init__(self):
        self.cfg = {}
        self.query_id = None
        self.query = None
        self.ref_model_id = None
        self.out_lines = []
        self.opt_metric = None
        self.sort_key = None
        self.backbone_only = False

    def make_config(self, config_filename):
        with open(config_filename) as f:
            for line in f:
                line = line.strip()
                if len(line) > 0:
                    try:
                        k, v = line.split(' ', 1)
                        if not line.startswith('#'):
                            self.cfg[k] = v
                    except:
                        print("Could not read line: ", line)

        if 'max' not in self.cfg:
            self.cfg['max'] = None
        else:
            self.cfg['max'] = int(self.cfg['max'])

        if 'saveFiles' not in self.cfg:
            self.cfg['saveFiles'] = True
        else:
            if self.cfg['saveFiles'].lower() in ('true', '1'):
                self.cfg['saveFiles'] = True
            else:
                self.cfg['saveFiles'] = False

        if 'subselection' not in self.cfg:
            self.cfg['subselection'] = ''

        self.cfg['fitmap_args'] = self.cfg['fitmap_args'].replace('listFits false', '')
        self.cfg['fitmap_args'] = self.cfg['fitmap_args'].replace('listFits true', '')
        self.cfg['fitmap_args'] = self.cfg['fitmap_args'].split()
        self.cfg['fitmap_args'].extend(['listFits', 'false'])
        self.cfg['map_threshold'] = float(self.cfg['map_threshold'])

        if len(set(self.cfg.keys()) & set(['symmetry', 'symmetryCenter', 'symmetryAxis'])) > 0:
            if len(set(self.cfg.keys()) & set(['symmetry', 'symmetryCenter', 'symmetryAxis'])) != 3:
                raise SymmetryConfigNotComplete('For symmetry, all three keys: symmetry, symmetryCenter, symmetryAxis must be specified')

            self.cfg['symmetryCenter'] = map(float, self.cfg['symmetryCenter'].split(','))
            self.cfg['symmetryAxis'] = map(float, self.cfg['symmetryAxis'].split(','))

        self.cfg = self.cfg

    def is_query_volume(self):
        return hasattr(self.query, 'surface_level_for_enclosed_volume')

    def open_query(self, model_filename):
        rc('open {0}'.format(model_filename))
        self.query = chimera.openModels.list()[-1]
        self.query_id = self.query.id
        if 'query_threshold' in self.cfg:
            rc('volume #{0} level {1}'.format(self.query_id, self.cfg['query_threshold']))

    def set_query_map_level(self, level):
        volume('#{0}'.format(self.query_id), level=[[level]], step=1)

    def close_model(self):
        rc('close #{0}'.format(self.query_id))
        self.query_id = None

    def open_map(self):
        rc('open {0}'.format(self.cfg['map']))
        self.map_model_id = chimera.openModels.list()[-1].id
        if self.cfg.get('symmetry') and self.cfg.get('symmetryCenter') and self.cfg.get('symmetryAxis'):
            volume('#{0}'.format(self.map_model_id), transparency=0.5, level=[[self.cfg['map_threshold']]], step=1, symmetry=self.cfg.get('symmetry'), center=self.cfg.get('symmetryCenter'), axis=self.cfg.get('symmetryAxis'))
        else:
            volume('#{0}'.format(self.map_model_id), transparency=0.5, level=[[self.cfg['map_threshold']]], step=1)

        # print(chimera.openModels.list()[-1].data.symmetries)

    def open_ref_model(self, ref_model_filename):
        rc('open {0}'.format(ref_model_filename))
        self.ref_model_id = chimera.openModels.list()[-1].id

    def move_to_center(self):
        cmd = 'cofr #{0}'.format(self.map_model_id)
        print(cmd)
        rc(cmd)
        rc('move cofr model #{0}'.format(self.query_id))

    def run(self, model_filename, out_dir):
        if self.ref_model_id is not None:
            rc('mm #{0} #{1}'.format(self.ref_model_id, self.query_id))  # Sup. model to ref model in order to position in the map

        query_str = '#{0}'.format(self.query_id)
        if not self.cfg['subselection'].startswith('&'):
            query_str = query_str + self.cfg['subselection']
        if self.backbone_only:
            query_str = query_str + "@N,CA,C,O,OP1,OP2,OP3,P,O3',O5',C5',C4',C3',O4',C1',C2',O2'"
        if self.cfg['subselection'].startswith('&'):
            query_str = '{0}{1}'.format(query_str, self.cfg['subselection'])

        args = '"{0}" #{1} '.format(query_str, self.map_model_id) + ' '.join(self.cfg['fitmap_args'])
        print(args)
        self.fitmap_command(args, out_dir, number=self.cfg['max'], saveFiles=self.cfg['saveFiles'])

    def save_fits(self, lfits, out_dir, number=None, saveFiles=True):
        mlist = sum([f.fit_molecules() for f in lfits], [])
        if self.query not in mlist:
            mlist.append(self.query)
        # if len(mlist) == 0:
        #     return

        idir = ifile = None
        vlist = [f.volume for f in lfits]

        pmlist = [m for m in mlist + vlist if hasattr(m, 'openedAs')]

        if self.is_query_volume():
            ext = '.mrc'
        else:
            ext = '.pdb'
        if pmlist:
            for m in pmlist:
                dpath, fname = os.path.split(m.openedAs[0])
                base, suf = os.path.splitext(fname)
                if ifile is None:
                    suffix = '_fit%d'+ext if len(lfits) > 1 else '_fit'+ext
                    ifile = base + suffix
                    print(ifile)
                if dpath and idir is None:
                    idir = dpath

        path = os.path.join(out_dir, ifile)
        print(path)
        if len(lfits) > 1 and path.find('%d') == -1:
            base, suf = os.path.splitext(path)
            path = base + '_fit%d' + suf


        out_lines = []

        if self.opt_metric:
            if self.opt_metric == 'correlation':
                sort_key = lambda x: x.correlation()
            elif self.opt_metric == 'cam':
                sort_key = lambda x: x.stats['correlation about mean']
            elif self.opt_metric == 'overlap':
                sort_key = lambda x: x.stats['overlap']
            else:
                sort_key = lambda x: x.correlation()
        else:
            sort_key = lambda x: x.correlation()

        for i, fit in enumerate(reversed(sorted(lfits, key=sort_key))):
            if number is not None and i == number:
                break

            p = path if len(lfits) == 1 else path % (i+1)
            # p = p.replace('^.pdb', '_'+str(round(fit.correlation(), 2))+'.pdb')
            p = re.sub(ext+'$', '_'+str(round(sort_key(fit), 2))+ext, p)
            fit.place_models()

            rot = []
            [rot.extend(x[:3]) for x in fit.transforms[0]]
            trans = []
            [trans.append(x[3]) for x in fit.transforms[0]]

            out_lines.append({
                'solution_id': i,
                'rotation': ' '.join(map(str, rot)),
                'translation': ' '.join(map(str, trans)),
                'cc_score': fit.correlation(),
                'cam_score': fit.stats['correlation about mean'],
                'overlap': fit.stats['overlap'],
                'filename': os.path.basename(p)
              })
            if saveFiles:
                if self.is_query_volume():
                    self.save_fit_volume(relModel=fit.volume, filename=p)
                else:
                    Midas.write(fit.fit_molecules(), relModel=fit.volume, filename=p)

        self.out_lines.extend(out_lines)

    def save_fit_volume(self, relModel, filename):
        rc('vop resample #{0} onGrid #{1} gridStep 1'.format(self.query_id, relModel.id))
        resampled_mdl = chimera.openModels.list()[-1]
        resampled_mdl.write_file(filename, format='mrc')
        rc('close #{0}'.format(resampled_mdl.id))

    def write_solutions(self, solutions_filename):
        if self.opt_metric:
            if self.opt_metric == 'correlation':
                sort_key = lambda x: x['cc_score']
            elif self.opt_metric == 'cam':
                sort_key = lambda x: x['cam_score']
            elif self.opt_metric == 'overlap':
                sort_key = lambda x: x['overlap']
            else:
                sort_key = lambda x: x['cc_score']
        else:
            sort_key = lambda x: x['cc_score']

        sorted_out_lines = list(reversed(sorted(self.out_lines, key=sort_key)))

        for i, line in enumerate(sorted_out_lines):
            line['solution_id'] = i
        out_lines = ['solution_id,filename,rotation,translation,overlap,cc_score,cam_score']

        for line in sorted_out_lines:
            s = '{i},{filename},{rotation},{translation},{overlap:.6f},{cc_score:.6f},{cam_score:.6f}'.format(i=line['solution_id'],
                               filename=line['filename'],
                               rotation=line['rotation'],
                               translation=line['translation'],
                               overlap=line['overlap'],
                               cc_score=line['cc_score'],
                               cam_score=line['cam_score'])
            out_lines.append(s)

        with open(solutions_filename, 'w') as f:
            f.write('\n'.join(out_lines))
            f.write('\n')

    def fitmap_command(self, args, out_dir, number, saveFiles):
        cmdname = 'fitmap'
        from chimera.replyobj import info
        info('\n> %s %s\n' % (cmdname, args))
        from Commands import parse_arguments
        from Commands import specifier_arg, volume_arg, int_arg, float_arg, bool_arg
        from Commands import string_arg

        req_args = (('atomsOrMap', specifier_arg),
                    ('volume', volume_arg),
                    )
        opt_args = ()
        kw_args = (('metric', string_arg),  # overlap, correlation or cam.
                   ('envelope', bool_arg),
                   ('resolution', float_arg),
                   ('shift', bool_arg),
                   ('rotate', bool_arg),
                   ('symmetric', bool_arg),
                   ('moveWholeMolecules', bool_arg),
                   ('search', int_arg),
                   ('placement', string_arg),
                   ('radius', float_arg),
                   ('clusterAngle', float_arg),
                   ('clusterShift', float_arg),
                   ('asymmetricUnit', bool_arg),
                   ('inside', float_arg),
                   ('sequence', int_arg),
                   ('maxSteps', int_arg),
                   ('gridStepMax', float_arg),
                   ('gridStepMin', float_arg),
                   ('listFits', bool_arg),
                   ('eachModel', bool_arg),
                   )
        kw = parse_arguments(cmdname, args, req_args, opt_args, kw_args)
        self.opt_metric = kw.get('metric')
        flist = fitcmd.fitmap(**kw)

        self.save_fits(flist, out_dir, number, saveFiles)
