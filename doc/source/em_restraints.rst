EM restraints
=============

Precomputed EM fits
-------------------

The restraints calculated from :doc:`pre-calculated fit libraries<fit_libraries_intro>`.

They are created automatically if the fit libraries are defined as :doc:`positions<fit_libs>` in the :doc:`json` and named ``discrete_restraints``.

If you want to use them, add ``discrete_restraints`` to your scoring functions.

Parameters:

discrete_restraints_weight
    Weight for restraints derived from the pre-defined positions, e.g. precomputed libraries of fits., default: **1.0**


FitRestraint
------------

The standard cross-correlation-based EM restraints implemented using `FitRestraint from IMP <https://integrativemodeling.org/2.14.0/doc/ref/classIMP_1_1em_1_1FitRestraint.html#a586e135631bd2bf9fea7e22988e5d23e>`_

It can be defined in the :doc:`json` using blocks like this:

.. code-block:: json

    {
        "active": true,
        "type": "em_map",
        "name": "FitRestraint",
        "em_restraint_type": "FitRestraint",
        "filename": "EM_data/emd_4151_binned.mrc",
        "voxel_size": 6.6,
        "resolution": 25,
        "weight": 1000,
        "first_copy_only": false,
        "repr_resolution": 10,
        "optimized_components": [
            {"name": "Elp1", "subunit": "Elp1"},
            {"name": "Elp2", "subunit": "Elp2"},
            {"name": "Elp3", "subunit": "Elp3"}

        ]
    }

Parameters

active
    true or false, whether the restraint should be created, you can set to false to save on loading time if you don't need this restraint
    (even if it's true, you still need to add it to a scoring function to have it an effect)
type
    must be: em_map
name
    whatever name. Use this name to refer to this restraint in :doc:`params` when defining the scoring functions
em_restraint_type
    must be: FitRestraint
filename
    relative or absolute path to the map in the MRC format
voxel_size
    Pixel/voxel size of the map in Angstrom
resolution
    Resolution for the EM map simulated from the model for cross-correlation calculation. Can be e.g. approximately the resolution of your map.
weight
    Weight of this restraint
first_copy_only
    true or false, Apply only to the first molecule copy of each series?
repr_resolution
    The resolution of representation to which this restraint should be applied
optimized_components
    A list of :doc:`selectors` defining molecules or parts to which this restraint should be applied to.

EnvelopePenetrationRestraint
----------------------------

A restraint promoting particles to be covered by an EM map envelope at a defined density threshold, implemented using `EnvelopePenetrationRestraint from IMP <https://integrativemodeling.org/2.14.0/doc/ref/classIMP_1_1em_1_1EnvelopePenetrationRestraint.html#a7e2e5b60bd73875222e250e7a0aefa87>`_

It can be defined in the :doc:`json` using blocks like this:

.. code-block:: json

    {
        "active": true,
        "type": "em_map",
        "name": "Elys_in_difference_density",
        "em_restraint_type": "EnvelopePenetrationRestraint",
        "filename": "EM/map.mrc",
        "threshold": 0.001,
        "voxel_size": 6.9,
        "resolution": 20,
        "weight": 1000,
        "first_copy_only": true,
        "repr_resolution": 10,
        "optimized_components": [
            {
                "name": "Elys",
                "subunit": "Elys"
            }
        ]
    }

Parameters

active
    true or false, whether the restraint should be created, you can set to false to save on loading time if you don't need this restraint
    (even if it's true, you still need to add it to a scoring function to have it an effect)
type
    must be: em_map
name
    whatever name. Use this name to refer to this restraint in :doc:`params` when defining the scoring functions
em_restraint_type
    must be: EnvelopePenetrationRestraint
threshold
    Density threshold to define the EM envelope
filename
    relative or absolute path to the map in the MRC format
voxel_size
    Pixel/voxel size of the map in Angstrom
resolution
    Resolution for the EM map simulated from the model for cross-correlation calculation. Can be e.g. approximately the resolution of your map.
weight
    Weight of this restraint
first_copy_only
    true or false, Apply only to the first molecule copy of each series?
repr_resolution
    The resolution of representation to which this restraint should be applied
optimized_components
    A list of :doc:`selectors` defining molecules or parts to which this restraint should be applied to.


ExcludeMapRestraint1
--------------------

A restraint preventing particle penetration of an EM density. Can used for example to prevent penetration of a segmented membrane density
or density segment known to be occupied by other subunits.

Yes, the name with 1 at the end ;-)

.. code-block:: json

        {
            "active": true,
            "type": "em_map",
            "name": "exclude_membrane",
            "em_restraint_type": "ExcludeMapRestraint1",
            "filename": "../EM/Membrane_final_resampled_no_neg.mrc",
            "threshold": 0.02,
            "voxel_size": 6.9,
            "resolution": 20,
            "weight": 100000,
            "first_copy_only": false,
            "repr_resolution": 10,
            "optimized_components": [
                {
                    "name": "Nup120",
                    "subunit": "Nup120",
                    "copies": [0,1]
                },
                {
                    "name": "Nup189c",
                    "subunit": "Nup189c",
                    "copies": [0,1]
                }
            ]
        }

Parameters

active
    true or false, whether the restraint should be created, you can set to false to save on loading time if you don't need this restraint
    (even if it's true, you still need to add it to a scoring function to have it an effect)
type
    must be: em_map
name
    whatever name. Use this name to refer to this restraint in :doc:`params` when defining the scoring functions
em_restraint_type
    must be: ExcludeMapRestraint1
threshold
    Density threshold to define the EM envelope
filename
    relative or absolute path to the map in the MRC format
voxel_size
    Pixel/voxel size of the map in Angstrom
resolution
    Resolution for the EM map simulated from the model for cross-correlation calculation. Can be e.g. approximately the resolution of your map.
weight
    Weight of this restraint
first_copy_only
    true or false, Apply only to the first molecule copy of each series?
repr_resolution
    The resolution of representation to which this restraint should be applied
optimized_components
    A list of :doc:`selectors` defining molecules or parts to which this restraint should be applied to.


CloseToEnvelopeRestraint
------------------------

A restraint pulling particles towards the envelope defined density. The restraint is 0 if the particle is at the envelope defined at the provided
threshold and increases with the distance from the envelope, either inside or outside of the envelope.

Can be used for example to restrain membrane binding regions to a membrane.

bring particles into the target EM density where they will be fitted by the other EM restraint.

.. code-block:: json

    {
        "active": true,
        "type": "em_map",
        "name": "Nup132_close_to_membrane1",
        "em_restraint_type": "CloseToEnvelopeRestraint",
        "filename": "../EM/Membrane_final_resampled_no_neg.mrc",
        "threshold": 0.02,
        "voxel_size": 6.9,
        "resolution": 20,
        "weight": 2000,
        "max_distance": 200,
        "first_copy_only": false,
        "repr_resolution": 10,
        "optimized_components": [
            {
                "name": "Nup132",
                "subunit": "Nup132",
                "domain": "membrane_binding",
                "copies": [
                    0,1
                ],
                "serie": "NR_1"
            }
        ]
    }

Parameters

active
    true or false, whether the restraint should be created, you can set to false to save on loading time if you don't need this restraint
    (even if it's true, you still need to add it to a scoring function to have it an effect)
type
    must be: em_map
name
    whatever name. Use this name to refer to this restraint in :doc:`params` when defining the scoring functions
em_restraint_type
    must be: CloseToEnvelopeRestraint
threshold
    Density threshold to define the EM envelope
filename
    relative or absolute path to the map in the MRC format
voxel_size
    Pixel/voxel size of the map in Angstrom
resolution
    Resolution for the EM map simulated from the model for cross-correlation calculation. Can be e.g. approximately the resolution of your map.
weight
    Weight of this restraint
max_distance
    Distance of a particle from the envelope at which to start evaluating the restraint, default 100 A. Increasing may decrease computational speed.
first_copy_only
    true or false, Apply only to the first molecule copy of each series?
repr_resolution
    The resolution of representation to which this restraint should be applied
optimized_components
    A list of :doc:`selectors` defining molecules or parts to which this restraint should be applied to.

EnvelopeFitRestraint
--------------------

A restraint from IMP that "fits the particles into the density map by alignment of principal components of the particles with principal components of the map", read more `EnvelopeFitRestraint from IMP <https://integrativemodeling.org/2.14.0/doc/ref/classIMP_1_1em_1_1EnvelopeFitRestraint.html#a53d60213da33d49c6c013e48d1e5b04f>`_

I never use it.

It can be defined in the :doc:`json` using blocks like this:

.. code-block:: json

    {
        "active": true,
        "type": "em_map",
        "name": "restraint_type_I_never_use",
        "em_restraint_type": "EnvelopeFitRestraint",
        "filename": "EM/map.mrc",
        "threshold": 0.001,
        "voxel_size": 6.9,
        "resolution": 20,
        "weight": 1000,
        "first_copy_only": true,
        "repr_resolution": 10,
        "optimized_components": [
            {
                "name": "Elys",
                "subunit": "Elys"
            }
        ]
    }

Parameters as above for EnvelopePenetrationRestraint.
