Custom restraints
=================

.. note:: This is rather for developers!

Any custom restraints can be added through a ``create_custom_restraints`` function in :doc:`params`.

The function accepts ``imp_uttils1.MultiRepresenation`` object as an argument,
and should output a dictionary mapping your chosen restraint names to lists of the restraints.
The restraints must subclass the IMP.Restraint or have same interface.

So you can get or add any native IMP restraints through this function.

.. warning:: Don't forget to add your restraints to the scoring functions in :doc:`params`!

For example, the following example will define restraints that do nothing. It seems
that such restraints have to be added if you use symmetry constraints and assign all your restraints only to the first copy of each series
- in such cases it seems that the copies are not updated properly.

.. code-block:: python

    def create_custom_restraints(r):
        '''
        Added so all copies are updated by constraints:
        when the system contains molecules for which there are no restraints imposed in the scoring function,
        they won't be updated during Optimization, only on model.update().
        '''
        for state_idx, state in enumerate(r.pmi_system.states):
            all_optimized = []
            for serie in r.config.series:
                for i, mol in enumerate(serie.molecules[state_idx]):
                    rbs, beads = IMP.pmi.tools.get_rbs_and_beads([mol.get_hierarchy()])
                    # print mol.get_name(), [x.get_particle().get_name() for x in ref_rbs]
                    for rb in rbs:
                        p = rb.get_particle()
                        if p not in all_optimized:
                            all_optimized.append(p)

        return {'dummy_restraints': [imp_utils1.core.DummyRestraint(r.model, all_optimized)]}