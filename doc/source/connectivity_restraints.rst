Connectivity restraints
=======================

Connectivity restraints are used to restrain distance between:

* consecutive single-residue beads (e.g. when representation resolution of 1 is used)

* two consecutive domains of a single protein separated by a linker of non-modelled/missing residues
  
The restraint is implemented as a harmonic distance restraint with a target distance equal to:

* 3.8 Angstrom for single-residue beads
  
* distance proportional to the number of missing residues between connected consecutive domains
  

The connectivity restraints are defined in the :doc:`params` automatically. If you want to use them, add ``conn_restraints`` to the scoring functions (see :doc:`params`).

Parameters (to be defined in the :doc:`params`):

connectivity_restraint_weight
    Connectivity restraint weight, default: **1.0**
max_conn_gap
    Number of missing residues of the missing atomic region above which the restraint will not be added, default: **None**
connectivity_restraint_k
    Connectivity restraint spring constant k., default: **10.0**
conn_reweight_fn
    A function that accepts the following parameters:
    mol - PMI molecule object
    next_resi_idx - residue of the next rigid body or bead
    prev_resi_idx - residue of the previous rigid body or bead
    connectivity_restraint_weight - default weight passed to add_connectivity_restraints, default: **None**
conn_first_copy_only
    Whether to add the restraints only for the first copy of the molecule (useful for symmetrical assemblies), default: **False**
ca_ca_connectivity_scale
    Scale the average CA-CA distance of 3.8 to account for that
    it is unlikely that the linker is fully stretched, default: 0.526 (count 2A per peptide bond), default: **0.526**
    Use 1.0 for no scaling at all.