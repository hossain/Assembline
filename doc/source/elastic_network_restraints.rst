Elastic network restraints
==========================

Elastic network restraints allow to flexibly restrain input orientations between subunits, domains or residues.

The input orientations are as read from the input PDB files, regardless whether the particles are in the same or different PDB files.

The elastic network can be defined in the :doc:`json` like this:

.. code-block:: json

    {
        "active": true,
        "type": "elastic_network",
        "name": "Nup107_Nup132_elastic_network1",
        "repr_resolution": 10,
        "distance_threshold": 15,
        "random_fraction": 1.0,
        "within_molecule": false,
        "k" : 1,
        "weight": 100,
        "first_copy_only": true,
        "optimized_components": [
            {
                "name": "Nup132", "subunit": "Nup132", "serie": "NR_1",
                "resi_ranges": [
                    [
                        459,
                        1162
                    ]
                ]
            },
            {
                "name": "Nup107", "subunit": "Nup107", "serie": "NR_1",
                "resi_ranges": [
                    [
                        581,
                        813
                    ]
                ]
            }
        ]
    }

This example will create elastic network between the specified residue ranges of Nup132 and Nup107 subunits.

**Parameters**

type
    Must be "elastic_network"
active
    true or false, whether the restraint should be created, you can set to false to save on loading time if you don't need this restraint
    (even if it's true, you still need to add it to a scoring function to have it an effect)
name
    whatever name. Use this name to refer to this restraint in :doc:`params` when defining the scoring functions
comment
    Optional comment, useful for documenting
weight
    Weight of this restraint
first_copy_only
    true or false, Apply only to the first molecule copy of each series?
repr_resolution
    The resolution of representation to which this restraint should be applied
k
    Spring constant for the harmonic potential, redundant to weight, ignore
optimized_components
    A list of :doc:`selectors` defining molecules or parts to which this restraint should be applied to.
distance_threshold
    Residue pairs within this distance threshold will be included in the elastic network 
random_fraction
    A fraction of distances below the thresholds to be randomly selected for creating the elastic network, default: 1.0 (all distances)
within_molecule
    If set to ``true``, only distances within the same subunit will be added to the elastic network
elastic_network_source
    Optional list of :doc:`selectors` PDB files and residue ranges that should serve as a source of distances for the elastic restraints. For example: 

    .. code-block:: json

        {
            "active": false,
            "type": "elastic_network",
            "name": "elastic_network_3-4",
            "repr_resolution": 1,
            "distance_threshold": 5,
            "random_fraction": 1.0,
            "within_molecule": true,
            "k": 1,
            "weight": 1,
            "optimized_components": [
                {
                    "subunit": "ProteinA",
                    "copies": [
                        0
                    ]
                },
                {
                    "subunit": "ProteinA",
                    "copies": [
                        0
                    ]
                }
            ],
            "elastic_network_source": [
                {
                    "filename": "proteinA.pdb",
                    "chain_id": "A",
                    "resi_ranges": [
                        [
                            403,
                            501
                        ]
                    ]
                },
                {
                    "filename": "proteinA.pdb",
                    "chain_id": "A",
                    "resi_ranges": [
                        [
                            509,
                            606
                        ]
                    ]
                }
            ]
        

