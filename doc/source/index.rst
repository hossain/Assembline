.. Assembline_manual documentation master file, created by
   sphinx-quickstart on Sat Dec 19 21:25:49 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Assembline - Assembly line of macromolecular assemblies!


Assembline manual
=================


.. toctree::
    :maxdepth: 2   
    :caption: Installation

    installation

.. toctree::
    :maxdepth: 2   
    :caption: Input preparation

    input_prep
    json
    params

.. toctree::
    :maxdepth: 2   
    :caption: Calculation of fit libraries

    fit_libraries_intro
    efitter_params
    efitter_run
    efitter_analyze
    fit_libs

.. toctree::
    :maxdepth: 2   
    :caption: Input structures

    input_structures

.. toctree::
    :maxdepth: 2   
    :caption: Rigid bodies

    rigid_bodies

.. toctree::
    :maxdepth: 2   
    :caption: Flexibility

    flexibility

.. toctree::
    :maxdepth: 2   
    :caption: Selectors

    selectors

.. toctree::
    :maxdepth: 2   
    :caption: Symmetry

    calc_symmetry
    symmetry_tr3ds
    applying_symmetry
    symmetry_constraints
    symmetry_restraints_intro

.. toctree::
    :maxdepth: 2   
    :caption: Restraints

    excluded_volume
    connectivity_restraints
    em_restraints
    crosslink_restraints
    symmetry_restraints
    interaction_restraints
    distance_restraints
    similar_orientation_restraints
    elastic_network_restraints
    parsimonious_states_restraints
    custom_restraints

.. toctree::
    :maxdepth: 2   
    :caption: Running

    run
    combinations
    recombinations
    refinement
    run_more
    rerunning

.. toctree::
    :maxdepth: 2   
    :caption: Analysis

    check_logs
    extract_scores
    visualize_models
    quick_convergence
    sampling_exhaustiveness

.. toctree::
    :maxdepth: 2   
    :caption: Notes

    glossary
    faq
    troubleshooting
    tips_and_tricks

