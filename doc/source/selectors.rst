Selectors
=========

Selectors are collections of keywords used to "select" parts of the system and act on them. For example,
you can select a part of the system and define as rigid body, add to an EM restraint, or other restraints.

Syntax / list of allowed keywords:

.. code-block:: json

    {
        "subunit": "....",
        "state": "....",
        "states": "....",
        "domain": "....",
        "serie": "....",
        "copies": "....",
        "chain_id": "....",
        "chainIds": "....",
        "filename": "....",
        "resi_ranges": "....",
    }

Additionally, :doc:`input_structures` and :doc:`rigid_bodies` specifications allow using the special ``foreach_serie`` and ``foreach_copie`` modifiers.
They allow to apply selectors to all series in the sytem and/or to all copies within the matching series.

Examples:

**Residue 925 of the Nup120 subunit in the series named NR_1**

.. code-block:: json

    {"subunit": "Nup120", "serie": "NR_1", "resi_ranges": [925]}

**The membrane_binding domain of the Copy 0 of the Nup155 subunit in the IR_cyt_outer ring (one of the four sub-rings of the inner ring of the nuclear pore complex)**

.. code-block:: json

    {
        "name": "Nup155",
        "subunit": "Nup155",
        "domain": "membrane_binding",
        "copies": [
            0
        ],
        "serie": "IR_cyt_outer"
    }

**All copies of Nup107 subunit in the CR series**

.. code-block:: json

    {
        "name": "Nup107",
        "subunit": "Nup107",
        "serie": "CR"
    }

**Define the same PDB file for each copy of Elp1 in all series**

.. code-block:: json

    {
        "foreach_serie": true,
        "foreach_copy": true,
        "components": [
            { "name": "Elp1",
            "subunit": "Elp1",
            "domain": "propeller1",
            "filename": "in_pdbs/Elp1_NTD_1st_propeller.pdb"
            }
        ]
    }


**Define a rigid body for propeller1 domain for each copy of Elp1 in all series**

.. code-block:: json

    {
        "rigid_bodies": [
            {
                "foreach_serie": true,
                "foreach_copy": true,
                "components": [
                    { "name": "Elp1_1", "subunit": "Elp1", "domain": "propeller1"}
                ]
            }
        ]
    }
