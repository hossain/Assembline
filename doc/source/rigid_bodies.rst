Rigid bodies
============

By default, rigid bodies will be defined automatically based on
the specifications in the PDB files :doc:`input_structures`. If you want to define rigid bodies
differently to your PDB files, you can do it in a separate ``rigid_bodies`` block at the top level of the :doc:`json`.

.. warning:: For ``rigid_bodies`` to have an effect, you have to set ``add_rigid_bodies = False`` in the :doc:`params`


The specification is the same as for PDB files:

    .. code-block:: json

        {
            "rigid_bodies": [
                {
                    "components": [
                        "list of selectors"
                    ]
                },
                {
                    "components": [
                        "list of selectors"
                    ]
                },

                "and so on"
            ]
        }

    You can also use the ``foreach_serie`` and ``foreach_copy`` keywords here:

    .. code-block:: json

        {
            "rigid_bodies": [
                {
                    "foreach_serie": true,
                    "foreach_copy": true,
                    "components": [
                        "list of selectors"
                    ]
                },
                {
                    "foreach_serie": true,
                    "foreach_copy": true,
                    "components": [
                        "list of selectors"
                    ]
                },

                "and so on"
            ]
        }

    An example:

    .. code-block:: json

        {
            "rigid_bodies": [
                {
                    "foreach_serie": true,
                    "foreach_copy": true,
                    "components": [
                        { "name": "Elp1_1", "subunit": "Elp1", "domain": "propeller1"}
                    ]
                },
                {
                    "foreach_serie": true,
                    "foreach_copy": true,
                    "components": [
                        { "name": "Elp1_1", "subunit": "Elp1", "domain": "propeller2"}
                    ]
                },
                {
                    "foreach_serie": true,
                    "components": [
                        { "name": "Elp1_1", "subunit": "Elp1", "domain": "CTD", "copies": [0, 1]}
                    ],
                    "freeze": true
                },
                {
                    "foreach_serie": true,
                    "foreach_copy": true,
                    "components": [
                        { "name": "Elp2_1", "subunit": "Elp2", 
                            "resi_ranges": [
                                [1, 239],
                                [257, 788]
                            ]
                        }
                    ]
                },
                {
                    "foreach_serie": true,
                    "foreach_copy": true,
                    "components": [
                        { "name": "Elp3_4", "subunit": "Elp3", "domain": "helix4"}
                    ]
                },
                {
                    "foreach_serie": true,
                    "foreach_copy": true,
                    "components": [
                        { "name": "Elp3_5", "subunit": "Elp3", "domain": "struct"}
                    ]
                },
                {
                    "foreach_serie": true,
                    "foreach_copy": true,
                    "components": [
                        { "name": "Elp3_6", "subunit": "Elp3", "domain": "flex_helix"}
                    ]
                }
            ]
        }