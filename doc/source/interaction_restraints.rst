Interaction restraints
======================

Interaction restraints can be used to define interactions between entire subunits, domains or single residues.

They can be defined in :doc:`json` like this:


* Interaction between two proteins:

    .. code-block:: json

        {
            "type": "connectivity",
            "active": true,
            "name": "Ely5-Nup120_interaction1",
            "comment": "Ely5 - Nup120 interaction",
            "data": [
                [{"subunit": "Nup120", "serie": "NR_1", "copies": [0]}, {"subunit": "Ely5", "serie": "NR_1", "copies": [0]}]
            ],
            "weight": 0.1,
            "k": 1,
            "distance": 7.0,
            "first_copy_only": true,
            "repr_resolution": 10
        }

* Distance between specific residues:

    .. code-block:: json

        {
            "type": "connectivity",
            "active": true,
            "name": "Nup107_connectivity",
            "comment": "Keep the N and C domains of Nup107 close to account for the three missing helices",
            "data": [
                [{"subunit": "Nup107", "serie": "NR_1", "resi_ranges": [494]}, {"subunit": "Nup107", "serie": "NR_1", "resi_ranges": [580]}],
                [{"subunit": "Nup107", "serie": "NR_2", "resi_ranges": [494]}, {"subunit": "Nup107", "serie": "NR_2", "resi_ranges": [580]}],
                [{"subunit": "Nup107", "serie": "NR_1", "resi_ranges": [512]}, {"subunit": "Nup107", "serie": "NR_1", "resi_ranges": [595]}],
                [{"subunit": "Nup107", "serie": "NR_2", "resi_ranges": [512]}, {"subunit": "Nup107", "serie": "NR_2", "resi_ranges": [595]}]
            ],
            "weight": 1000.0,
            "k": 1,
            "distance": 25.0,
            "first_copy_only": true, 
            "repr_resolution": 1
        }

    Here, each pair of residues specified in the ``"data"`` list will be restrained to distance **below** 25A


**Parameters**

type
    Must be "connectivity"
active
    true or false, whether the restraint should be created, you can set to false to save on loading time if you don't need this restraint
    (even if it's true, you still need to add it to a scoring function to have it an effect)
name
    whatever name. Use this name to refer to this restraint in :doc:`params` when defining the scoring functions
comment
    Optional comment, useful for documenting
data
    A list of **pairs** of :doc:`selectors` between which the interaction restraints should be defined
weight
    Weight of this restraint
first_copy_only
    true or false, Apply only to the first molecule copy of each series?
repr_resolution
    The resolution of representation to which this restraint should be applied
k
    Spring constant for the harmonic potential, redundant to weight, ignore
distance
    Distance threshold below which the restraint is satisfied. The score is 0 below this threshold and harmonic above.
