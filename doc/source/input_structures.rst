Input structures
================

    * PDB files are defined by appropriately formatted objects (``{components: ...}``, see below) that define which chain of the given PDB file maps to which subunit
      
    * The PDB file specification is added to ``data`` object in the :doc:`json`

    *  A PDB file can contain parts of different subunits, for example, in the example below, ``yourpath.pdb`` contains two subunits: ``<subunit_name1>`` and ``< subunit_name2>`` with chains ``E`` and ``F``, respectively.

    *  Different parts of subunits can be present in different PDB files and under different chain ids, for example, in the example below, ``<subunit_name2>`` has one part in ``yourpath.pdb`` under chain ``F``, and another part in ``yourpath2.pdb`` under chain ``B``
       

    * by default, the rigid bodies are created based on this PDB file specification. All PDB fragments grouped into the same ``components`` list will be grouped into a rigid body. In the example below, chain ``X`` and ``Y`` of ``yourpath3.pdb`` will be in the same rigid body and all other chains will be seperate rigid bodies.
    
    * Specification:

        .. code-block:: JSON

            {
                "data": [
                    {
                        "type": "pdb_files",
                        "name": "pdb_files",
                        "data": [
                                    {
                                        "components": [
                                            { 
                                            "name": "<subunit_name1>",
                                            "subunit": "<subunit_name1>",
                                            "chain_id": "E",
                                            "filename": "yourpath.pdb"
                                            }
                                        ]
                                    },
                                    {
                                        "components": [
                                            { 
                                            "name": "<subunit_name2>",
                                            "subunit": "<subunit_name2>",
                                            "chain_id": "F",
                                            "filename": "yourpath.pdb"
                                            }
                                        ]
                                    },
                                    {
                                        "components": [
                                            { 
                                            "name": "<subunit_name2>",
                                            "subunit": "<subunit_name2>",
                                            "chain_id": "B",
                                            "filename": "yourpath2.pdb"
                                            }
                                        ]
                                    },
                                    {
                                        "components": [
                                            { 
                                            "name": "<subunit_name3>",
                                            "subunit": "<subunit_name3>",
                                            "chain_id": "X",
                                            "filename": "yourpath3.pdb"
                                            },
                                            { 
                                            "name": "<subunit_name4>",
                                            "subunit": "<subunit_name4>",
                                            "chain_id": "Y",
                                            "filename": "yourpath3.pdb"
                                            }
                                        ]
                                    }
                            ]
                    }
                ]
            }

