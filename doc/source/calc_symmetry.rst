Calculating symmetry
====================

For symmetry restraints and constraints, symmetry axes or transformation matrices need to be specified in :doc:`json`.

If you don't have the symmetry axes or matrices yet, here are some alternative methods for obtaining that information using `UCSF Chimera <https://www.cgl.ucsf.edu/chimera/>`_ based on an EM map. Other EM processing software should be also able to report that information.

Method 1
--------

If you know the symmetry axis goes along x, y or z axis, you still need the "symmetry point", i.e. the point through which the axis passes through.
It might go through (0,0,0) but not necessarily. To detect the symmetry and find the center you can do:

#. Open the EM map in Chimera.
   
#. Open command line: Menu -> General Controls -> Command Line

#. In the command line, execute ``measure symmetry #0`` command.
   
    The software should print sth like "Symmetry map_name.mrc: C2, center 112 112 113".

    The center values are in "grid coordinates" so multiply each value by map voxel size to obtain the coordinates of the center in Angstrom.
    Enter the resulting values in :doc:`symmetry_tr3ds`

Method 2
--------

The manual way. A lot of fun.

#. Open the EM map in Chimera.

#. Open the same map again.

#. Open a IDLE window where the information about transformations will be printed: Menu Tools -> General Controls -> IDLE

#. Open command line: Menu -> General Controls -> Command Line

#. Open `Model Panel <https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/modelpanel.html>`_ (Menu Favorites -> Model Panel).
   
#. In the Model Panel, de-activate the first map from motion using the check box in the A column
   
#. Using a mouse, rotate the second EM map around the symmetry axis for around one rotation step, so the maps roughly overlap.
   
   E.g. for a 2-fold symmetry, rotate 180 degrees, for a 8-fold symmetry rotate 45 degrees.

#. Activate the first model back for motion by clicking the check box in  in the A column

#. Fit the rotated map to the first map using `Fit in Map tool <https://www.cgl.ucsf.edu/chimera/docs/ContributedSoftware/fitmaps/fitmaps.html>`_` (Tools -> Volume Data -> Fit in Map)
   
#. In the command line, execute ``measure rotation #1 #0`` command.
   
#. Switch to the IDLE window. A log that looks like the following should appear:
   
    .. code-block:: 
   
     Position of elp123_150616_cl3dK4nofirstround3class1symC2_reformat.mrc (#0) relative to elp123_150616_cl3dK4nofirstround3class1symC2_reformat.mrc (#1) coordinates:
      Matrix rotation and translation
        -0.99999999  -0.00009462  -0.00011784 492.84569581
         0.00009464  -0.99999999  -0.00014030 492.81640318
        -0.00011782  -0.00014031   0.99999998   0.06655213
      Axis  -0.00005892  -0.00007015   1.00000000
      Axis point 246.41119075 246.41986373   0.00000000
      Rotation angle (degrees) 179.99457796
      Shift along axis   0.00294274

#. Use the values define the rotation matrix in the JSON in :doc:`symmetry_tr3ds`:

    #. Use the values in the "Axis" and "Axis point" parts to define the symmetry in the JSON:

            .. code-block:: json

                {
                    "symmetry": {
                        "sym_tr3ds": [

                            {
                                "name": "2fold",
                                "axis": [0, 0, 1],
                                "center": [246.41119075, 246.41986373, 0.00000000],
                                "type": "C2"   
                                  
                            }

                        ]
                    }
                }


    #. Use the values in the "Matrix rotation and translation" part to define the rotation matrix in the JSON.
       
       The first three columns correspond to rotation and the last column to the translation.
       Copy the two rows of the rotation columns as a single row of 9 elements in the JSON configuration.
       Copy the column of the translation transposing to a row of 3 elements in the JSON configuration.

       For the transformation above, you should end up with sth like this

           .. code-block:: json
           
              {
                "symmetry": {
                    "sym_tr3ds": [
                  
                        {
                            "name": "8-fold",
                            "rot": [-0.99999999, -0.00009462, -0.00011784,0.00009464, -0.99999999, -0.00014030, -0.00011782, -0.00014031, 0.99999998],
                            "trans": [492.84569581, 492.81640318, 0.06655213]
                        }

                    ]
                }
              }

        Insert this code in the JSON file, as specified in :doc:`json`.

