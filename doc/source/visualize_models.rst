Visualize models and trajectories
=================================

Assembline can output models in several formats.

By default, the following formats are produced:

* `RMF format <https://integrativemodeling.org/rmf/format.html>`_  in the ``models_rmf`` directory

    The RMF files can be visualized in UCSF Chimera and UCSF ChimeraX

* Simple TXT format that stores transformations of rigid bodies in the ``models_txt`` directory
    
In addition, optionally, trajectories of the optimization runs can be saved in the RMF format, in ``traj`` directory (using ``--traj`` option of ``assembline.py``)


These two formats can be used to generate PDB or CIF files of the resulting integrative models, using ``rebuild_atomic.py`` atomic script.


Create CIF format file for top models
-------------------------------------

**In the output directory**, run:

.. code-block:: bash

    rebuild_atomic.py --project_dir <full path to the original project directory> --top 1 all_scores_sorted_uniq.csv

.. note:: The ``--project_dir`` option is only necessary if you use relative paths in the :doc:`json`

.. warning:: This will only build the parts corresponding to rigid bodies, if had flexible beads in the system, see below how to rebuild them at the atomic resolution


Create CIF format file for a specific model
-------------------------------------------

.. code-block:: bash

    rebuild_atomic.py --project_dir <full path to the original project directory> --top 1 models_txt/0002321model.txt

Rebuilding flexible beads
-------------------------

If you had single residue flexible beads in your modeling (i.e. at representation resolution of 1), they can be rebuilt by running this script with ``--rmf`` and ``--rmf_auto`` option:

For the top models:

.. code-block:: bash

    rebuild_atomic.py --project_dir <full path to the original project directory> --top 1 --rmf_auto all_scores_sorted_uniq.csv

For a specific model:

.. code-block:: bash

    rebuild_atomic.py --project_dir <full path to the original project directory> --top 1 --rmf models_rmf/0002321model.rmf models_txt/0002321model.txt

The ``-rmf`` options tell the script to extract flexible beads from the RMF files and re-model them at atomic resolution using `Modeller <https://salilab.org/modeller/>`_.

Create PDB for top models
-------------------------



.. code-block:: bash

    rebuild_atomic.py --project_dir <full path to the original project directory> --top 1 --format pdb all_scores_sorted_uniq.csv

.. warning:: The PDB format is not recommended (especially for multi-subunit complexes) - it does not support multi-character chain IDs and has a limit on the number of atoms in the file.

Create separate PDB files for rigid bodies
------------------------------------------

.. code-block:: bash

    rebuild_atomic.py --project_dir <full path to the original project directory> --top 1 --format multi_pdb all_scores_sorted_uniq.csv

This can be useful if you want to use the models for subsequent optimizations with Assembline or other software.

The scripts used for :doc:`refinement` are using the multi_pdb format behind the scenes.

Other options
-------------

See ``rebuild_atomic.py`` usage instructions for other useful options:

.. code-block:: bash

    rebuild_atomic.py -h

For example:

* to rebuild a specific subunit use ``--subunit`` option

* to rebuild only the first copy of each series (often the asymmetric unit of the complex) use ``--first_copy_only`` or ``--copies 0`` option

* to rebuild specific copies use ``--copies`` option, e.g. ``--copies 0,1,7`` option

Visualize the top model(s) in Chimera and Xlink Analyzer
--------------------------------------------------------

Visualizing top models in Chimera and Xlink Analyzer will help to answer:

    * Are crosslinks satisfied?

    * Are there big clashes between subunits?

    * Are models fitting other information you have but not used in the optimization?

Visualize the optimization trajectory of the top model(s) in Chimera and Xlink Analyzer
---------------------------------------------------------------------------------------

* The trajectories in the RMF format are stored in the ``traj/`` folder of the output directory

* UCSF Chimera allows:

    * to open the RMF format and play the trajectories

    * display plots of the total score and individual restraints over the course of the trajectory

* Analyze:

    * are all rigid bodies expected to move during the trajectory?

    * are the total scores and all restraints decreasing?

* For better visualization of crosslinks and to color the subunits in the RMF by subunit color, you can use Xlink Analyzer.

    .. note:: For Xlink Analyzer to work, you need to replace a file ``share/rmf/__init__.py`` in your Chimera installation with this file: https://github.com/crosslinks/XlinkAnalyzer/blob/master/edited__init__.py_for_chimera_rmf

    * The ``share`` directory is located within the Chimera installation folder (https://www.cgl.ucsf.edu/chimera/experimental/install.html):

    .. code-block:: bash
    
        source

        Windows
                       C:\Program Files\Chimera\share
        Linux
                       /usr/local/chimera/share
        Macintosh
                       /Applications/Chimera.app/Contents/Resources/share
