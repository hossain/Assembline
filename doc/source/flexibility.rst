Flexibility
===========

By default:

* all residues that are present in :doc:`input_structures` but are not covered by :doc:`rigid_bodies` are represented as flexible beads. In this case, the representation resolution will be the highest resolution from ``struct_resolutions`` list in ``params.py``.

* all residues that are present in the sequences but are not present in :doc:`input_structures`, will not be added
  
To add residues that are not present in the :doc:`input_structures`, but are present in the sequences, you can use ``add_missing`` parameter in ``params.py``:

``add_missing = True`` will add all missing residues

``add_missing = <list of selectors>`` will add only selected residues. For example: 

    .. code-block:: python

        add_missing = [
            {
                "subunit": "Elp3",
                "resi_ranges": [[87, 92], [375, 390], [403, 406]]
            },
            {
                "subunit": "Elp1",
                "resi_ranges": [[1, 20]]
            }

        ]

If an input PDB structure does not fit as a rigid body, you can treat it somewhat flexibly by breaking it into smaller rigid bodies
and defining :doc:`elastic_network_restraints` to restrain original interfaces.