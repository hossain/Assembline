Tips and tricks
===============

Speeding up calculations
------------------------

* Make your EM maps smaller
  
    Sometimes you may get an EM map which box is much larger than the actual map. All those extra and empty voxels will slow down calculations of cross-correlation function 
    during calculations of :doc:`fit libraries<fit_libs>` and `FitRestraint`. 

    You can remove the extra padding voxels in an EM processing software, for example UCSF Chimera


* Downsample the EM map
  
    Is your map low resolution but has very small voxel size (e.g. EM map at 10 A with voxel size of 1 A)? Such unnecessarily small voxel size will slow down computations.

    Downsample your EM map then in an EM processing software, for example UCSF Chimera or EMAN2.


* Use different system representation resolutions for different restraints
  
    Is it necessary to use atomic representation to calculate all you pre-defined restraints? If your computational running times are relatively high then you should consider
    choosing more coarse grained representations for specific restraints (e.g. Excluded volume restraints).

* Always run single test runs before submitting larger modelling jobs to the cluster (or even to your local multi-core cpu).