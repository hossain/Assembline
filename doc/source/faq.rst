Frequently asked questions
==========================

* How do I cite Assembline?

* Whom should I contact in case of discovered bugs or for general support?

* How many and which types of input data should I have to use Assembline?

* My modelling target is a single protein, can I still use Assembline?

* Can I define custom spatial restraints/constraints?

* When should I stop running Assembline based on sampling exhaustiveness analysis?

* How should I decide (rule of thumb) on weights for scoring terms in my scoring function?

* How is Assembline performing compared to other integrative modelling packages?