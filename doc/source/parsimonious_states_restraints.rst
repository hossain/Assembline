Parsimonious states restraints
==============================

.. note:: This type of restraints is still in experimental stage. In case of urgent use/set-up please contact: ``vasileios.rantos@embl-hamburg.de``, ``jan.kosinski@embl.de``

They are defined in :doc:`params` by setting:

.. code-block:: python
    
    add_parsimonious_states_restraints = True

**parameters**

add_parsimonious_states_restraints
    Add parsimonious states restraints?, default: **False**
parsimonious_states_weight
    Weight, default: **1.0**
parsimonious_states_distance_threshold
    Distance threshold for elastic network restraining the states, default: **0.0**
parsimonious_states_exclude_rbs
    Python function to exclude selected rigid bodies from the restraint.
    Arguments: IMP's rigid body object
    Return: True if the rigid body should be excluded., default: Some default function
parsimonious_states_representation_resolution
    Representation resolution for this restraint, default: **10**
parsimonious_states_restrain_rb_transformations
    Restraint rigid body transformations instead of using elastic network, default: **True**