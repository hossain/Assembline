Defining symmetry
=================

Symmetry can be defined in ``"symmetry"`` blocks at the top level of the :doc:`json` in lists of ``"sym_tr3ds"``.

Multiple symmetries can be defined and used simultaneously.

Using a symmetry axis information
---------------------------------

.. code-block:: json

    {
        "symmetry": {
            "sym_tr3ds": [

                {
                    "name": "2fold",
                    "axis": "vector of the symmetry axis",
                    "center": "symmetry point (through which the above symmetry axis is passing through)",
                    "type": "cyclic symmetry type like C2, C3, C8 and so on"
                      
                }

            ]
        }
    }

For example:

.. code-block:: json

    {
        "symmetry": {
            "sym_tr3ds": [

                {
                    "name": "2fold",
                    "axis": [0, 0, -1],
                    "center": [246.39112398, 246.41114644, 248.600000],
                    "type": "C2"   
                      
                }

            ]
        }
    }


How to get this transformation information? Follow :doc:`calc_symmetry` instruction how to do it based on an EM map.

Using transformation matrix
---------------------------

.. code-block:: json

    {
        "symmetry": {
            "sym_tr3ds": [
          
                {
                    "name": "a unique custom name",
                    "rot": "list of 9 values for rotation, 3 for each row of the symmetry matrix",
                    "trans": "list of 3 values for translation"
                }

            ]
        }
    }

For example:

.. code-block:: json

    {
        "symmetry": {
            "sym_tr3ds": [
          
                {
                    "name": "2-fold",
                    "rot": [0.70712087, 0.70709269, 0.00003928, -0.70709269, 0.70712087, 0.00000394, 0.00003057, -0.00002499, 1.00000000],
                    "trans": [-411.56570152, 993.5285317, 0.09136987]
                }

            ]
        }
    }

How to get this transformation matrices? Follow :doc:`calc_symmetry` instruction how to do it based on an EM map.
