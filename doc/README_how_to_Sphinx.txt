mkdir doc
cd doc/
mkdir tutorials
cd tutorials/
mkdir Elongator
cd Elongator/
sphinx-quickstart 
pip install sphinx-rtd-theme
#add "sphinx_rtd_theme", to extensions in conf.py
#set sphinx_rtd_theme as theme in conf.py
Set html_theme = 'sphinx_rtd_theme' in conf.py

#Add the following lines to conf.py
def setup(app):
    app.add_css_file('custom.css')
and create a file custom.css with 
.wy-nav-content {
    max-width: 70%;
}
to set up the width a bit wider than default (otherwise code blocks are very narrow)

Add to do extension if you wish:
https://stackoverflow.com/questions/50314856/enabling-the-todo-feature-in-sphinx-docs
and use it as here:
https://www.sphinx-doc.org/en/master/usage/extensions/todo.html

make html
ls _build/html/
(open index.html in web browser to see the doc)



The navigation menu on the left will not always update, to update you can do: (https://stackoverflow.com/questions/40032601/why-is-toctree-not-updating-with-rtd-theme)
make clean
make html




Can't do PDF
This doesn't work:
conda install -c conda-forge latexmk
conda install -c conda-forge texlive-core
make latexpdf 

#this also doesn't work
pip install rst2pdf
#add 'sphinx.ext.mathjax', 'rst2pdf.pdfbuilder' to extensions in conf.py
sphinx-build -b pdf ./ _build/pdf


If you use Sublime Text editor:
- set up spell check https://www.sublimetext.com/docs/3/spell_checking.html
- for syntax completion, install Restructured Text (RST) Snippets https://packagecontrol.io/packages/Restructured%20Text%20(RST)%20Snippets
