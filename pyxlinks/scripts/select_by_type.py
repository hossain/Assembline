#!/usr/bin/python
from optparse import OptionParser

import pyxlinks


def main():
    usage = """usage: %prog [options] xquest_filename > outfile
    E.g:
    Generate xlink file with:
    - AbsPos2 for looplinks field (default is n/a)
    - Prot2 for looplinks field (default is -)
    """
    parser = OptionParser(usage=usage)

    parser.add_option("--intra", action="store_true", dest="intra", default=False,
                      help="Select intra-links [default: %default]")

    parser.add_option("--inter", action="store_true", dest="inter", default=False,
                      help="Select inter-links [default: %default]")

    parser.add_option("--mono", action="store_true", dest="mono", default=False,
                      help="Select mono-links [default: %default]")

    (options, args) = parser.parse_args()

    xquest_filename = args[0]

    types = []
    if options.intra:
        types.append('intra')
    if options.inter:
        types.append('inter')
    if options.mono:
        types.append('mono')

    xlink_set = pyxlinks.XlinksSet(xquest_filename)
    xlink_set.filterByType(types)
    xlink_set.print_out()


if __name__ == '__main__':
    main()