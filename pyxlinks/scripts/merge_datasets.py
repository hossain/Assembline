#!/usr/bin/python
import os
import csv
from optparse import OptionParser
from collections import defaultdict

from cogent.parse.fasta import MinimalFastaParser

import pyxlinks

class TooFewSequences(Exception): pass
def seqs_to_cols(aln_seqs):
    if len(aln_seqs) < 2:
        raise TooFewSequences('Provide at least two sequences as input')

    return list(zip(*aln_seqs))


def aln_cols_to_num_map(aln_cols):
    a_i = 1
    b_i = 1
    num_map = {}
    for a, b in aln_cols:
        if '-' not in (a, b):
            num_map[a_i] = b_i
        elif b == '-':
            num_map[a_i] = None
        if a != '-':
            a_i = a_i + 1
        if b != '-':
            b_i = b_i + 1

    return num_map

def read_data(filename):
    with open(filename, 'rU') as csvfile: #'U' is necessary, otherwise sometimes crashes with _csv.Error: new-line character seen in unquoted field - do you need to open the file in universal-newline mode?
        dialect = csv.Sniffer().sniff(csvfile.readline(), ['\t', ','])
        csvfile.seek(0)

        reader = csv.DictReader(csvfile, dialect=dialect)

        # for row in reader:
        #     print row
        out = []
        for row in reader:
            row['alignments'] = row['alignments'].split(',')

            seq_names_to_ref = defaultdict(list)
            with open(row['seq_names_to_ref'], 'rU') as seq_names_to_ref_csvfile:
                seq_names_to_ref_rows = csv.DictReader(seq_names_to_ref_csvfile, dialect=dialect)
                for seq_rec in seq_names_to_ref_rows:
                    seq_names_to_ref[seq_rec['name_from']].append(seq_rec['name_to'])

                row['seq_names_to_ref'] = seq_names_to_ref


            xquest_names_to_seq_names = {}
            with open(row['xquest_names_to_seq_names'], 'rU') as xquest_names_to_seq_names_csvfile:
                xquest_names_to_seq_names_rows = csv.DictReader(xquest_names_to_seq_names_csvfile, dialect=dialect)
                for seq_rec in xquest_names_to_seq_names_rows:
                    xquest_names_to_seq_names[seq_rec['xquest_name']] = seq_rec['seq_name']

                row['xquest_names_to_seq_names'] = xquest_names_to_seq_names

            out.append(row)

        return out

def get_resi_map(seq_name, ref_seq_name, alignments):
    aln_data = {}
    for aln in alignments:
        # names, aln_cols = get_aln_cols(aln)
        with open(aln) as f:
            parser = MinimalFastaParser(f)
            for name, seq in parser:
                if name in (seq_name, ref_seq_name):
                    aln_data[name] = seq
                if len(aln_data) == 2:
                    break

    if len(aln_data) == 2:
        seqs = [
            aln_data[seq_name],
            aln_data[ref_seq_name]
        ]

        return aln_cols_to_num_map(seqs_to_cols(seqs))

def read_out_xquest_names(ref_seq_names_to_xquest_names_filename):
    with open(ref_seq_names_to_xquest_names_filename, 'rU') as csvfile:    
        dialect = csv.Sniffer().sniff(csvfile.readline(), ['\t', ','])
        csvfile.seek(0)

        reader = csv.DictReader(csvfile, dialect=dialect)


        xquest_names_to_seq_names = {}
        for row in reader:
            xquest_names_to_seq_names[row['old_name']] = row['new_name']

        return xquest_names_to_seq_names

def main():
    usage = "usage: %prog [options] cfg_filename outfilename"
    parser = OptionParser(usage=usage)

    parser.add_option("-n", "--names", dest="out_xquest_names",
                      help="optional file with mapping of sequence names to final names. ", metavar="FILE")

    (options, args) = parser.parse_args()


    cfg_filename = args[0]
    outfilename = args[1]


    data = read_data(cfg_filename)

    xlinks_sets = []

    for data_cfg in data:
        xlink_set = pyxlinks.XlinksSet(filename=data_cfg['xquest_file'],
                                xlinker=data_cfg['xlinker'])



        # num_maps = []

        # resi_map = get_resi_map(alignments=data_cfg['alignments'], seq_names_to_ref=data_cfg['seq_names_to_ref'])

        for xquest_name in xlink_set.get_protein_names():
            seq_name = data_cfg['xquest_names_to_seq_names'][xquest_name]

            for ref_seq_name in data_cfg['seq_names_to_ref'][seq_name]:
                resi_map = get_resi_map(seq_name, ref_seq_name, data_cfg['alignments'])

                xlink_set.renumber_resi(xquest_name, resi_map)
            
            xlink_set.rename_protein_names(xquest_name, ref_seq_name)

            for row in xlink_set.data:
                row['source'] = data_cfg['xquest_file']
            
        xlink_set.data[:] = [row for row in xlink_set.data if None not in (row['AbsPos1'], row['AbsPos2'])] #remove xlinks that connected resi without equivalent in ref_seqs


        xlinks_sets.append(xlink_set)

    xlinks_sets_merged = sum(xlinks_sets)

    if options.out_xquest_names:
        rename_dict = read_out_xquest_names(options.out_xquest_names)
        xlinks_sets_merged.rename_protein_names_from_dict(rename_dict)

    xlinks_sets_merged.save_to_file(outfilename)

if __name__ == '__main__':
    main()