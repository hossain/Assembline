#!/usr/bin/python
from optparse import OptionParser
from collections import defaultdict
import os

import pyxlinks
from pyxlinks import _xlink2unique_marker_by_prots_resi

def main():
    usage = """usage: %prog [options] xquest_filename1 xquest_filename2 shorter_threshold
    
    Makes two new files with xlinks shorter in one comparing to second, short more than the threshold

    Files must have distance column, e.g. from Xlink Analyzer
    E.g:
    python get_shorter.py ours_apo.csv ours_DNA.csv 0.8
    """
    parser = OptionParser(usage=usage)

    parser.add_option("--max_len", type="float", dest="max_len", default=None,
                      help="Maximum length of cross-link [default: %default]")

    (options, args) = parser.parse_args()

    xlink_set1 = pyxlinks.XlinksSet(args[0])
    xlink_set2 = pyxlinks.XlinksSet(args[1])
    shorter_threshold = float(args[2])
    if options.max_len is not None:
        max_len = float(options.max_len)
    else:
        max_len = None

    unique_set1 = xlink_set1.get_unique_by_prots_resi()
    unique_set2 = xlink_set2.get_unique_by_prots_resi()

    common = defaultdict(list)

    for xlink in unique_set1.data:
        common[_xlink2unique_marker_by_prots_resi(xlink)].append(xlink)

    for xlink in unique_set2.data:
        common[_xlink2unique_marker_by_prots_resi(xlink)].append(xlink)

    data1 = []
    data2 = []

    for marker, xlink_pair in common.items():
        if len(xlink_pair) == 2:
            xlink1, xlink2 = xlink_pair

            if float(xlink1['distance']) <= shorter_threshold * float(xlink2['distance']):
                if max_len is not None and float(xlink1['distance']) > max_len:
                    continue
                data1.append(xlink1)
            elif float(xlink2['distance']) <= shorter_threshold * float(xlink1['distance']):
                if max_len is not None and float(xlink2['distance']) > max_len:
                    continue
                data2.append(xlink2)

    print(len(data1))
    print(len(data2))


    shorter1 = pyxlinks.XlinksSet(xlink_set_data=data1,
                            fieldnames=xlink_set1.fieldnames[:], description=xlink_set1.description
            )
    filename1 = os.path.splitext(args[0])[0] + '_shorter.csv'
    shorter1.save_to_file(filename1, quoting=None, fieldnames=None)

    shorter2 = pyxlinks.XlinksSet(xlink_set_data=data2,
                            fieldnames=xlink_set2.fieldnames[:], description=xlink_set2.description
            )
    filename2 = os.path.splitext(args[1])[0] + '_shorter.csv'
    shorter2.save_to_file(filename2, quoting=None, fieldnames=None)


    (options, args) = parser.parse_args()

if __name__ == '__main__':
    main()