#!/usr/bin/python
from optparse import OptionParser

from pyxlinks import XlinksSet

def main():
    usage = "usage: %prog [options] xquest_filename dist > some_name.csv"
    parser = OptionParser(usage=usage)

    # parser.add_option("--min", action="store_true", dest="use_min_dist", default=False,
    #                   help="Use minimal rather than maximal distance (distance > dist) [default: %default]")

    (options, args) = parser.parse_args()


    xquest_filename = args[0]
    dist = float(args[1])


    xlink_set = XlinksSet(filename=xquest_filename, add_extra_fields=False)
    xlink_set.calc_scores(dist)


if __name__ == '__main__':
    main()