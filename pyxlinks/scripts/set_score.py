#!/usr/bin/python
from optparse import OptionParser

import pyxlinks


def main():
    usage = """usage: %prog [options] xquest_filename score > outfile

    Set score of all xlinks to specific value
    """
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()

    xquest_filename = args[0]
    score = args[1]

    xlink_set = pyxlinks.XlinksSet(xquest_filename)
    for xlink in xlink_set.data:
        pyxlinks.set_score(xlink, score)
    xlink_set.print_out()


if __name__ == '__main__':
    main()
