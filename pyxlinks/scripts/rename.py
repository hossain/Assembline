#!/usr/bin/python
import os
import csv
from optparse import OptionParser

from pyxlinks import XlinksSet

def read_old_to_new_names(filename):
    with open(filename, 'rU') as csvfile: #'U' is necessary, otherwise sometimes crashes with _csv.Error: new-line character seen in unquoted field - do you need to open the file in universal-newline mode?
        dialect = csv.Sniffer().sniff(csvfile.readline(), ['\t', ','])
        csvfile.seek(0)

        reader = csv.reader(csvfile, dialect=dialect)

        return dict(list(reader))

def main():
    usage = "usage: %prog [options] xquest_filename old_to_new_names > outfilename"
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()


    xquest_filename = args[0]
    old_to_new_names_filename = args[1]

    rename_dict = read_old_to_new_names(old_to_new_names_filename)

    xlink_set = XlinksSet(filename=xquest_filename)
    xlink_set.rename_protein_names_from_dict(rename_dict)

    xlink_set.print_out()

if __name__ == '__main__':
    main()