#!/usr/bin/python
from optparse import OptionParser

import pyxlinks

def main():
    usage = """usage: %prog [options] prot1 resi1 prot2 resi2 xquest_filenames
    E.g:
    python select.py "sp|P04051|RPC1_YEAST" 1242 "sp|P32910|RPC6_YEAST" 65 ours_apo.csv ours_DNA.csv
    """
    parser = OptionParser(usage=usage)

    parser.add_option("--no_fieldnames", action="store_true", dest="no_fieldnames", default=False,
                      help="Do not print fieldnames [default: %default]")

    (options, args) = parser.parse_args()

    prot1 = args[0]
    resi1 = args[1]
    prot2 = args[2]
    resi2 = args[3]
    xquest_filenames = args[4:]

    for filename in xquest_filenames:
        xlink_set = pyxlinks.XlinksSet(filename)
        found = xlink_set.get_by_both_pos(prot1, resi1, prot2, resi2)
        if found:
            for x in found:
                xlink_set.print_xlink_csv(x, no_fieldnames=options.no_fieldnames)

if __name__ == '__main__':
    main()