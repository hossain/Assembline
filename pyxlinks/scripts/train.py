#!/usr/bin/python
'''
To try:
o blending (meta prediction)
    o https://github.com/emanuele/kaggle_pbr/blob/master/blend.py

'''
import csv
import math

import numpy as np
from scipy import interp
# import matplotlib
# matplotlib.use('TkAgg')

import matplotlib.pyplot as plt
from itertools import cycle
from optparse import OptionParser

from sklearn import svm
from sklearn import naive_bayes
from sklearn import cross_validation
from sklearn import preprocessing
from sklearn import metrics
from sklearn.metrics import matthews_corrcoef, confusion_matrix, roc_auc_score, fbeta_score, f1_score, classification_report, roc_curve, auc
from sklearn.decomposition import PCA, ProbabilisticPCA, KernelPCA
from sklearn.grid_search import GridSearchCV
from sklearn.dummy import DummyClassifier
from sklearn.lda import LDA
from sklearn.qda import QDA
from sklearn.utils import check_arrays

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn import linear_model



def is_mono_link(row):
    return row['Type'] == 'monolink'

def is_looplink(row):
    return row['Type'] == 'looplink'

def is_not_mono_or_looplink(row):
    return not (is_mono_link(row) or is_looplink(row))

class ModeNotSupportedError(Exception):
    pass

class Trainer(object):
    def __init__(self, mode, observed_filename, expected_not_observed_filename):
        '''
        mode - choices 'xlinks', 'monolinks'
        '''
        allowed_modes = ('monolinks', 'xlinks') 
        if mode not in allowed_modes:
            raise ModeNotSupportedError('Allowed modes: ' + ','.join(allowed_modes))

        self.fieldnames = None

        observed = self.read_csv(observed_filename)
        # observed = read_csv("test/PolI/out_test_data/Observed_xwalk.csv")
        observed_monolinks = list(filter(is_mono_link, observed))
        observed_xlinks = list(filter(is_not_mono_or_looplink, observed))

        expected_not_observed = self.read_csv(expected_not_observed_filename)
        # expected_not_observed = read_csv("test/PolI/out_test_data/Expected__not_observed_xwalk.csv")
        expected_not_observed_monolinks = list(filter(is_mono_link, expected_not_observed))
        expected_not_observed_xlinks = list(filter(is_not_mono_or_looplink, expected_not_observed))

        if mode == 'monolinks':
            observed_to_use = observed_monolinks
            expected_not_observed_to_use = expected_not_observed_monolinks
        elif mode == 'xlinks':
            observed_to_use = observed_xlinks
            expected_not_observed_to_use = expected_not_observed_xlinks


        self.mode = mode
        self.observed = observed_to_use
        self.expected_not_observed = expected_not_observed_to_use
        self.features = []
        self.target_names = ['expected_not_observed (0)', 'observed (1)']

    def read_csv(self, filename):
        reader = csv.DictReader(open(filename,"rb"),delimiter=',', quoting=csv.QUOTE_NONNUMERIC)
        if not self.fieldnames:
            self.fieldnames = reader.fieldnames

        return list(reader)

    def save_rows(self, rows, filename, quoting=None):
        if quoting is None:
            quoting = csv.QUOTE_NONE


        fieldnames = self.fieldnames

        with open(filename, 'wb') as f:
            wr = csv.DictWriter(f, fieldnames, quoting=quoting, extrasaction='ignore')
            wr.writerow(dict((fn,fn) for fn in fieldnames)) #do not use wr.writeheader() to support also python 2.6
            wr.writerows(rows)

    def create_sample(self, rows):
        '''
        '''
        x = []
        out_rows = []
        y = []
        skip_not_fully_cleaved = True #they may brake training/testing by making peptides from testing overlaping too much with training
        for row in rows:

            if skip_not_fully_cleaved and 'No' in (row['FullyCleaved1'], row['FullyCleaved2']):
                continue

            # features = ['LengthTotal', 'MassTotal', 'hydrophobicTotal', 'HydrophobicityTotal', 'chargedTotal']
            if self.mode == 'monolinks':
                features = ['LengthTotal', 'MassTotal', 'HydrophobicityTotal', 'chargedTotal', 'net_charge', 'rt_Guo1986Total', 'rt_Meek1980Total']
            elif self.mode == 'xlinks':
                features = ['LengthTotal', 'MassTotal', 'HydrophobicityTotal', 'chargedTotal', 'net_charge', 'rt_Guo1986Total', 'rt_Meek1980Total']

            sample = [row[feature] for feature in features]

            # mz = row['MassTotal'] / row['net_charge']
            # features.append('mz')
            # row['mz'] = mz
            # sample.append(mz)



            use_rASA = False
            if use_rASA:
                if self.mode == 'monolinks':                  
                    if row['rASA1'] == 'ND':
                        continue
                    else:
                        features.append('rASA')
                        row['rASA'] = row['rASA1']
                        sample.append(row['rASA'])
                elif self.mode == 'xlinks':
                    rASAs = []
                    if row['rASA1'] != 'ND':
                        rASAs.append(row['rASA1'])
                    if row['rASA2'] != 'ND':
                        rASAs.append(row['rASA2'])

                    if len(rASAs) == 0:
                        continue
                    else:

                        min_rASA = min(rASAs)
                        features.append('rASA')
                        row['rASA'] = min_rASA
                        sample.append(min_rASA)


            use_hub_score = False
            if use_hub_score:
                if row['Hub_score_total'] == 'ND':
                    continue
                else:
                    features.append('Hub_score_total')
                    sample.append(row['Hub_score_total'])

            self.use_monolinkability = False
            if self.use_monolinkability:
                if self.mode == 'xlinks':
                    monolinkabilities = []
                    for monolinkability in ([row['Monolinkability1'], row['Monolinkability2']]):
                        if isinstance(monolinkability, float):
                            monolinkabilities.append(monolinkability)
                    if len(monolinkabilities) > 0:
                        row['Monolinkability'] = min(monolinkabilities)
                        features.append('Monolinkability')
                        sample.append(row['Monolinkability'])
                    else:
                        continue

            x.append(sample)
            out_rows.append(row)

            rename_dict = {
                'No': 0,
                'Yes': 1
            }
            y.append(rename_dict[row['Observed']])

        self.features = features

        return out_rows, y

    def rows_to_x(self, rows):
        x = []

        for row in rows:

            sample = []
            for feature in self.features:
                if feature in row:
                    sample.append(row[feature])

            x.append(sample)

        return x

    def plot_2D(self, data, target):
        colors = cycle('rgbcmykw')
        target_ids = list(range(len(self.target_names)))
        plt.figure()
        for i, c, label in zip(target_ids, colors, self.target_names):
            plt.scatter(data[target == i, 0], data[target == i, 1],
                       c=c, label=label)
        plt.legend()
        # plt.show()
        plt.savefig('pca.png')

    def plot_3D(self, data, target):
        '''
            http://stackoverflow.com/questions/5490288/plotting-3d-scatter-in-matplotlib
        '''
        colors = cycle('rgbcmykw')
        target_ids = list(range(len(self.target_names)))
        fig = plt.figure()

        from mpl_toolkits.mplot3d import Axes3D
        ax = Axes3D(fig)
        for i, c, label in zip(target_ids, colors, self.target_names):
            ax.scatter(data[target == i, 0], data[target == i, 1], data[target == i, 2],
                       c=c, label=label)
            ax.plot([], [], 'o', c = c, label=label)
        ax.legend()
        # fig.show()

        #http://stackoverflow.com/questions/12904912/how-to-set-camera-position-for-3d-plots-using-python-matplotlib
        for ii in range(0,360,10):
            ax.view_init(elev=10., azim=ii)
            fig.savefig("movie"+str(ii)+".png")
        # fig.savefig('pca.png')




    def summarize(self, clf):
        '''
        clf - clf as return from grid_search
        '''
        print("Best parameters set found on development set:")
        print()
        print('=============', str(clf.best_estimator_), '=================')
        print()
        # print("Grid scores on development set:")
        # print
        # for params, mean_score, scores in clf.grid_scores_:
        #     print("%0.3f (+/-%0.03f) for %r"
        #           % (mean_score, scores.std() / 2, params))
        # print


        print("The scores are computed on the full evaluation set.")
        print()
        Y_true, Y_pred = self.Y_test, clf.predict(self.X_test)
        print((classification_report(Y_true, Y_pred, target_names=self.target_names)))
        print()
        print("Matthew's correlation coefficient", matthews_corrcoef(Y_true, Y_pred))
        # print "AUC", roc_auc_score(Y_true, Y_pred)
        print("Confusion matrix:")
        print(confusion_matrix(Y_true, Y_pred))


        print_pepts = False
        if print_pepts:
            i = 0
            print('true pred data', ' '.join(self.features))
            for x, y in zip(Y_true, Y_pred):
                row = self.rows_test[i]
                pepts = row['Id']
                # print x, y, pepts, ' '.join([str(row[fname]) for fname in features])
                print(x, y, pepts, ' '.join(map(str, self.X_test[i])))
                i = i + 1


    def classify(self, scale=True):

        observed_rows, observed_y = self.create_sample(self.observed)
        self.save_rows(observed_rows, 'observed_used.csv')
        expected_not_observed_rows, expected_not_observed_y = self.create_sample(self.expected_not_observed)
        self.save_rows(expected_not_observed_rows, 'expected_used.csv')

        print('Observed', len(observed_rows))
        print('Expected not observed', len(expected_not_observed_rows))
        print(len(observed_y), len(expected_not_observed_y))


        #Split into train and test in a little bit tedious way
        #to have always final X sets and corresponding original rows
        all_rows = observed_rows + expected_not_observed_rows

        observed_rows_freq = float(len(observed_rows))/len(all_rows)
        expected_not_observed_rows = float(len(expected_not_observed_rows))/len(all_rows)
        print(observed_rows_freq, expected_not_observed_rows)


        self.Y = observed_y + expected_not_observed_y

        self.X = self.rows_to_x(all_rows)


        sss = cross_validation.StratifiedShuffleSplit(self.Y, 1, test_size=0.3, random_state=0)

        self.rows_train = []
        self.rows_test = []
        self.Y_train = []
        self.Y_test = []
        for train_index, test_index in sss:
            print(("TRAIN:", len(train_index), "TEST:", len(test_index)))
            for idx in train_index:
                self.rows_train.append(all_rows[idx])
                self.Y_train.append(self.Y[idx])

            for idx in test_index:
                self.rows_test.append(all_rows[idx])
                self.Y_test.append(self.Y[idx])            


        # self.rows_train, self.rows_test, self.Y_train, self.Y_test = cross_validation.train_test_split(all_rows, self.Y, test_size=0.3, random_state=0)
        self.X_train = self.rows_to_x(self.rows_train)
        self.X_test = self.rows_to_x(self.rows_test)


        self.X = np.array(self.X).astype('float')
        self.X_train = np.array(self.X_train).astype('float')
        self.X_test = np.array(self.X_test).astype('float')

        self.Y = np.array(self.Y).astype('int')
        self.Y_train = np.array(self.Y_train).astype('int')
        self.Y_test = np.array(self.Y_test).astype('int')

        X_scaled = preprocessing.scale(self.X)

        if scale:
            self.X_train = preprocessing.scale(self.X_train)
            self.X_test = preprocessing.scale(self.X_test)


        print('train size', len(self.X_train))
        print('test size', len(self.X_test))



        do_pca = False
        if do_pca:
            n_components = 3
            pca = PCA(n_components=n_components).fit(X_scaled)
            print('pca.components_', pca.components_)
            print('pca.explained_variance_ratio_', pca.explained_variance_ratio_)
            print('pca.explained_variance_ratio_.sum()', pca.explained_variance_ratio_.sum())
            X_pca = pca.transform(X_scaled)

            if n_components == 2:
                self.plot_2D(X_pca, self.Y)
            elif n_components == 3:
                self.plot_3D(X_pca, self.Y)


        n_features = len(self.features)



        if self.mode == 'monolinks':
            to_test = [
                    {
                    'clf': svm.SVC,
                    'tuned_parameters': [{'kernel': ['rbf'], 'gamma': [1e-2, 1e-3, 1e-4],
                             'C': [1, 10, 100, 1000, 10000], 'class_weight':['auto', {0: observed_rows_freq, 1: expected_not_observed_rows}]},
                            {'kernel': ['linear'], 'C': [1, 10, 100, 1000, 10000], 'class_weight':['auto']}],
                    'kwargs': {}
                    # 'kwargs': {'class_weight': 'auto'}
                    },
                    {
                        'clf': RandomForestClassifier,
                        'tuned_parameters': [{"max_depth": [3, None],
                          "max_features": [1, 2, 3, n_features],
                          "min_samples_split": [1, 2, 3, n_features],
                          "min_samples_leaf": [1, 2, 3, n_features],
                          "bootstrap": [True, False],
                          "criterion": ["gini", "entropy"]}],
                        'kwargs': {'n_estimators': 20, 'n_jobs': 4}
                    },
                    {
                        'clf': QDA,
                        'tuned_parameters': {},
                        'kwargs': {'priors': [expected_not_observed_rows, observed_rows_freq]}
                    },
                    {
                        'clf': LDA,
                        'tuned_parameters': {},
                        'kwargs': {'priors': [expected_not_observed_rows, observed_rows_freq]}
                    },
                    {
                        'clf': linear_model.LogisticRegression,
                        'tuned_parameters': {'C': [0.001, 0.01, 0.1, 1, 10, 100, 1000], 'class_weight':['auto', {0: observed_rows_freq, 1: expected_not_observed_rows}]},
                        'kwargs': {'intercept_scaling': 1, 'dual': False, 'fit_intercept': True, 'penalty': 'l2', 'tol': 0.0001} #, 'class_weight': 'auto'}                
                    }

                ]

        elif self.mode == 'xlinks':
            to_test = [{
                    'clf': svm.SVC,
                    'tuned_parameters': [{'kernel': ['rbf'], 'gamma': [1, 1e-1, 1e-2, 1e-3, 1e-4, 1e-6],
                             'C': [0.01, 1, 10, 100, 1000, 10000], 'class_weight':['auto', {0: observed_rows_freq, 1: expected_not_observed_rows}]},
                            {'kernel': ['linear'], 'C': [0.01, 1, 10, 100, 1000, 10000], 'class_weight':['auto', {0: observed_rows_freq, 1: expected_not_observed_rows}]}],
                    'kwargs': {}
                    # 'kwargs': {'class_weight': 'auto'}
                    },
                    {
                        'clf': RandomForestClassifier,
                        'tuned_parameters': [{"max_depth": [3, None],
                          "max_features": [1, 2, 3, n_features],
                          "min_samples_split": [1, 2, 3, n_features],
                          "min_samples_leaf": [1, 2, 3, n_features],
                          "bootstrap": [True, False],
                          "criterion": ["gini", "entropy"]}],
                        'kwargs': {'n_estimators': 20, 'n_jobs': 4}
                    },
                    {
                        'clf': QDA,
                        'tuned_parameters': {},
                        'kwargs': {'priors': [expected_not_observed_rows, observed_rows_freq]}
                    },
                    {
                        'clf': LDA,
                        'tuned_parameters': {},
                        'kwargs': {'priors': [expected_not_observed_rows, observed_rows_freq]}
                    },
                    {
                        'clf': linear_model.LogisticRegression,
                        'tuned_parameters': {'C': [0.001, 0.01, 0.1, 1, 10, 100, 1000], 'class_weight':['auto', {0: observed_rows_freq, 1: expected_not_observed_rows}]},
                        'kwargs': {'intercept_scaling': 1, 'dual': False, 'fit_intercept': True, 'penalty': 'l2', 'tol': 0.0001} #, 'class_weight': 'auto'}                
                    }

                ]            

        # scores = [metrics.make_scorer(matthews_corrcoef, greater_is_better=True)]
        # scores = [metrics.make_scorer(metrics.f1_score, average='weighted')]

        if self.mode == 'monolinks':
            scores = ['f1']
            # scores = ['precision']
        elif self.mode == 'xlinks':
            # scores = ['f1']
            scores = ['roc_auc']
            # scores = [metrics.make_scorer(metrics.f1_score, average='weighted')]
            # scores = [metrics.make_scorer(matthews_corrcoef, greater_is_better=True)]


        for score in scores:
            print(("# Tuning hyper-parameters for %s" % score))
            print()

            for clf_to_test in to_test:
                cv = cross_validation.StratifiedKFold(self.Y_train, n_folds=5)
                clf = GridSearchCV(clf_to_test['clf'](**clf_to_test['kwargs']), clf_to_test['tuned_parameters'], cv=cv, scoring=score, n_jobs=4)
                clf.fit(self.X_train, self.Y_train)

                self.summarize(clf)


        for strategy in ('stratified','most_frequent', 'uniform'):
            dummy = DummyClassifier(strategy=strategy)
            dummy.fit(self.X_train, self.Y_train)
            Y_pred = dummy.predict(self.X_test)
            print("Dummy " + strategy)
            print("Matthew's correlation coefficient", matthews_corrcoef(self.Y_test, Y_pred))
            print("AUC", roc_auc_score(self.Y_test, Y_pred))
            print(confusion_matrix(self.Y_test, Y_pred))


        # forest = RandomForestClassifier(bootstrap=True, compute_importances=None,
        #                     criterion='entropy', max_depth=3, max_features=3,
        #                     min_density=None, min_samples_leaf=2, min_samples_split=3,
        #                     n_estimators=20, n_jobs=4, oob_score=False, random_state=None,
        #                     verbose=0)

        # forest.fit(self.X_train, self.Y_train)
        # Y_pred = forest.predict(self.X_test)
        # print "Best forest "
        # print "Matthew's correlation coefficient", matthews_corrcoef(self.Y_test, Y_pred)
        # print "AUC", roc_auc_score(self.Y_test, Y_pred)
        # print confusion_matrix(self.Y_test, Y_pred)

        # importances = forest.feature_importances_

        # std = np.std([tree.feature_importances_ for tree in forest.estimators_],
        #              axis=0)
        # indices = np.argsort(importances)[::-1]

        # Print the feature ranking
        # print("Feature ranking:")

        # for f, f_name in enumerate(self.features):
        #     print("%d. feature %s (%f)" % (f + 1, f_name, importances[indices[f]]))

        self.test_LogisticRegression()

        # Y_pred = ByLength().predict(self.rows_train)
        # print "Matthew's correlation coefficient", matthews_corrcoef(self.Y_train, Y_pred)
        # print(classification_report(self.Y_train, Y_pred, target_names=self.target_names))
        # print confusion_matrix(self.Y_train, Y_pred)

        # if self.use_monolinkability:
        #     Y_pred = ByMonolinkability().predict(self.rows_train)
        #     print "Matthew's correlation coefficient", matthews_corrcoef(self.Y_train, Y_pred)
        #     print(classification_report(self.Y_train, Y_pred, target_names=self.target_names))
        #     print confusion_matrix(self.Y_train, Y_pred)            

    def draw_roc(self, clf):
        #Based on http://scikit-learn.org/stable/auto_examples/plot_roc_crossval.html#example-plot-roc-crossval-py

        cv = cross_validation.StratifiedKFold(self.Y_train, n_folds=5)

        plt.figure()

        mean_tpr = 0.0
        mean_fpr = np.linspace(0, 1, 100)

        for i, (train, test) in enumerate(cv):
            probas_ = clf.fit(self.X_train[train], self.Y_train[train]).predict_proba(self.X_train[test])

            # Compute ROC curve and area the curve
            fpr, tpr, thresholds = roc_curve(self.Y_train[test], probas_[:, 1])
            mean_tpr += interp(mean_fpr, fpr, tpr)
            mean_tpr[0] = 0.0
            roc_auc = auc(fpr, tpr)
            plt.plot(fpr, tpr, lw=1, label='ROC fold %d (area = %0.2f)' % (i, roc_auc))

        plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')


        mean_tpr /= len(cv)
        mean_tpr[-1] = 1.0
        mean_auc = auc(mean_fpr, mean_tpr)
        plt.plot(mean_fpr, mean_tpr, 'k--',
                 label='Mean ROC (area = %0.2f)' % mean_auc, lw=2)

        plt.xlim([-0.05, 1.05])
        plt.ylim([-0.05, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating characteristic example')
        plt.legend(loc="lower right")
        plt.savefig('roc.png')


    def get_optimal_treshold(self, clf, observed_tpr_threshold):
        scores = clf.predict_proba(self.X_train)

        second_col = scores[:,1]

        fpr, tpr, thresholds = roc_curve(self.Y_train, second_col, pos_label=1)

        val = min(tpr, key=lambda x:abs(x-observed_tpr_threshold))
        index = list(tpr).index(val)
        # print fpr
        # print tpr
        # print thresholds

        return thresholds[index]

    def test_LogisticRegression(self):
        '''
        Test manual re-implementation of LogisticRegression based on scikit-optimized coefficients.

        Should give the same results as LogisticRegression above on test set.

        Will print coefficients to use.
        '''


        lr = linear_model.LogisticRegression(C=1000, class_weight='auto', dual=False, fit_intercept=True,
          intercept_scaling=1, penalty='l2', random_state=None, tol=0.0001)
        lr.fit(self.X_train, self.Y_train)

        for key in list(lr.__dict__.keys()):
            print(key, lr.__dict__[key])


        # self.draw_roc(lr)



        Y_pred_manual = []
        X_test_manual = self.rows_to_x(self.rows_test)
        X_test_manual = np.array(X_test_manual).astype('float')
        X_test = scale(X_test_manual)
        Y_true = self.Y_test

       
        # X_test_manual = self.X
        # X_test_manual = np.array(X_test_manual).astype('float')
        # X_test = scale(X_test_manual)
        # Y_true = self.Y

        # X_test_manual = self.X_train
        # X_test_manual = np.array(X_test_manual).astype('float')
        # X_test = scale(X_test_manual)
        # Y_true = self.Y_train

        observed_tpr_threshold = 0.9
        threshold = self.get_optimal_treshold(lr, observed_tpr_threshold)


        print("Use this coefficients for Integraviz:")
        print(lr.coef_.T)
        print("Use this intercept for Integraviz:")
        print(lr.intercept_)
        print("Use proba threshold:")
        print(threshold)

        for x in X_test:
            # print x
            # print lr.coef_
            # print np.dot(x, lr.coef_[0])
            coef = lr.coef_.T
            intercept = lr.intercept_

         #    coef = [[ 0.20674994],                                                                                                                             
         # [-2.32889876],                                                                                                                                       
         # [ 0.60839257],                                                                                                                                                   
         # [-2.00634465],                                                                                                                                                   
         # [ 1.6213341 ],                                                                                                                                                   
         # [-1.09717237],                                                                                                                                                   
         # [ 0.50039194]]

         #    intercept = [-0.01356405]  


            prob = sigmoid(np.dot(x, coef) + intercept)
            if prob < threshold:
                pred = 0
            else:
                pred = 1
            Y_pred_manual.append(pred)

        # for a, b in zip(Y_pred, Y_pred_manual):
        #     print a, b

        for a, b in zip(Y_true, Y_pred_manual):
            print(a, b)

        print("Matthew's correlation coefficient", matthews_corrcoef(Y_true, Y_pred_manual))

        print(confusion_matrix(Y_true, Y_pred_manual))

#Tools to create predictor manually from fitted logistic regression
#http://stackoverflow.com/questions/18993867/scikit-learn-logistic-regression-model-coefficients-clarification?rq=1
def _mean_and_std(X, axis=0, with_mean=True, with_std=True):
    """Compute mean and std deviation for centering, scaling.

    Copied from scikit to avoid dependency.

    Zero valued std components are reset to 1.0 to avoid NaNs when scaling.
    """
    X = np.asarray(X)
    Xr = np.rollaxis(X, axis)

    if with_mean:
        mean_ = Xr.mean(axis=0)
    else:
        mean_ = None

    if with_std:
        std_ = Xr.std(axis=0)
        if isinstance(std_, np.ndarray):
            std_[std_ == 0.0] = 1.0
        elif std_ == 0.:
            std_ = 1.
    else:
        std_ = None

    return mean_, std_


def scale(X):
    '''
    Equivalent of scikit preprocessing.scale

    Modified from scikit to avoid dependency.
    '''
    X = np.asarray(X)
    axis = 0
    with_mean=True
    with_std=True
    mean_, std_ = _mean_and_std(
        X, axis, with_mean=with_mean, with_std=with_std)

    X = X.copy()
    # Xr is a view on the original array that enables easy use of
    # broadcasting on the axis in which we are interested in
    Xr = np.rollaxis(X, axis)
    if with_mean:
        Xr -= mean_
    if with_std:
        Xr /= std_
    
    return X



def sigmoid(x):
    return 1 / (1 + math.exp(-x))

def main():
    usage = "usage: %prog [options] [xlinks|monolinks] observed.csv expected_not_observed.csv"
    parser = OptionParser(usage=usage)

    parser.add_option("--no_scale", action="store_false", dest="scale", default=True,
                      help="skip intra xlinks")

    (options, args) = parser.parse_args()

    mode = args[0]
    observed_filename = args[1]
    expected_not_observed_filename = args[2]

    # observed = read_csv("test/PolI/out_test_data/Observed.csv")
    # # observed = read_csv("test/PolI/out_test_data/Observed_xwalk.csv")
    # observed_monolinks = filter(is_mono_link, observed)
    # observed_xlinks = filter(is_not_mono_or_looplink, observed)

    # expected_not_observed = read_csv("test/PolI/out_test_data/Expected__not_observed.csv")
    # # expected_not_observed = read_csv("test/PolI/out_test_data/Expected__not_observed_xwalk.csv")
    # expected_not_observed_monolinks = filter(is_mono_link, expected_not_observed)
    # expected_not_observed_xlinks = filter(is_not_mono_or_looplink, expected_not_observed)

    # if mode == 'monolinks':
    #     observed_to_use = observed_monolinks
    #     expected_not_observed_to_use = expected_not_observed_monolinks
    # elif mode == 'xlinks':
    #     observed_to_use = observed_xlinks
    #     expected_not_observed_to_use = expected_not_observed_xlinks



    trainer = Trainer(mode=mode, observed_filename=observed_filename, expected_not_observed_filename=expected_not_observed_filename)
    # trainer = Trainer(mode=mode, observed=observed_to_use, expected_not_observed=expected_not_observed_to_use)
    trainer.classify(scale=options.scale)
    # classify(observed, expected_not_observed)

class ByLength(object):
    def predict(self, X):
        out = []
        for x in X:
            if x['MassTotal'] < 4000:
                out.append(1)
            else:
                out.append(0)

        return out

class ByMonolinkability(object):
    def predict(self, X):
        out = []
        for x in X:
            if x['Monolinkability'] > 0.5:
                out.append(1)
            else:
                out.append(0)

        return out

if __name__ == '__main__':
    main()

