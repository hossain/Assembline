#!/usr/bin/python
from optparse import OptionParser

from pyxlinks import XlinksSet

def main():
    usage = "usage: %prog [options] xquest_filename > some_name.csv"
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()


    xquest_filename = args[0]

    xlink_set = XlinksSet(filename=xquest_filename, add_extra_fields=False)
    all_unique = xlink_set.get_unique()

    all_unique.print_out()

if __name__ == '__main__':
    main()