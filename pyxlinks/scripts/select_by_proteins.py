#!/usr/bin/python
from optparse import OptionParser

import pyxlinks


def main():
    usage = """usage: %prog [options] xquest_filename protein1 protein2 ... > outfile
    E.g:
    Generate xlink file with xlinks involving proteins from the list
    """
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()

    xquest_filename = args[0]
    proteins = args[1:]

    xlink_set = pyxlinks.XlinksSet(xquest_filename)
    xlink_set.filterByProteins(proteins)
    xlink_set.print_out()


if __name__ == '__main__':
    main()
