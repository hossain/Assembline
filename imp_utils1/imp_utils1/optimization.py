import os
import sys
import itertools
import subprocess
from collections import defaultdict
import json

import IMP
from IMP import OptimizerState
import IMP.core
import RMF
import IMP.pmi.macros
import IMP.pmi.tools

from . import convergence

class PrintOptimizerState(OptimizerState):
    frame = 0
    prefix = ''

    def update(self):
        if self.frame % self.get_period() == 0:
            print(self.prefix + str(self.frame))
            sys.stdout.flush()
        self.frame += 1

    def reset_frame(self):
        self.frame = 0

class WriteTotalScoreOptimizerState(OptimizerState):
    def __init__(self, sf, fh, m, prefix=''):
        self.sf = sf
        self.fh = fh
        self.prefix = prefix
        IMP.OptimizerState.__init__(self, m, 'WriteTotalScoreOptimizerState')

    def update(self):
        out_str = ','.join([self.prefix,str(self.sf.get_last_score())]) + '\n'
        self.fh.write(out_str)
        self.fh.flush()

class RecordTotalScoreOptimizerState(OptimizerState):
    def __init__(self, sf, m):
        self.sf = sf
        IMP.OptimizerState.__init__(self, m, 'RecordTotalScoreOptimizerState')
        self.scores = []

    def update(self):
        self.scores.append(self.sf.get_last_score())

    def reset(self):
        self.scores = []

def apply_each_mover(model, used_movers):
    for mover in used_movers:
        mover.propose()
        mover.accept()

    model.update()

def print_restraints(r, used_restraints):
    print()
    print('Restraints:')
    printed = []
    for restr in r.xlink_restraints+r.conn_restraints+r.ev_restraints+r.discrete_restraints+r.em_restraints:
        if restr in used_restraints:
            print(restr, restr.get_score(), restr.evaluate(False), restr.get_weight())
            printed.append(restr)
    for restr in used_restraints:
        if restr not in printed:
            print(restr, restr.get_score(), restr.evaluate(False), restr.get_weight())



def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out

def chunkIt_1(it, n, seqlen):
    k, m = divmod(seqlen, n)

    #WTF but works
    extra = k + max(0, m-k)
    kas = []
    for i in range(n-1):
        if extra > 0:
            kas.append(k+1)
            extra = extra - 1
        else:
            kas.append(k)
    kas = kas + [m-extra]

    for k in kas:
        chunk_it = itertools.islice(it, k )
        try:
            first_el = next(chunk_it)
        except StopIteration:
            return
        yield itertools.chain((first_el,), chunk_it)

def optimize_beads_with_cg(all_rbs, cg, n_steps):
    were_optimized = []
    for rb in all_rbs:
        were_optimized.append(rb.get_coordinates_are_optimized())
        rb.set_coordinates_are_optimized(False)
    cg.optimize(n_steps)
    for rb, was_optimized in zip (all_rbs, were_optimized):
        if was_optimized:
            rb.set_coordinates_are_optimized(True)




def ReplicaExchange_protocol(r, params, score_func, all_restraints, score_functions, score_functions_restraints, options):
    raise Exception('not working')

    movers_for_main, all_movers_info = params.get_movers_for_main_opt(r, params.protocol)

    print("Movers for the main optimization:", movers_for_main)

    if not params.do_ini_opt:
        apply_each_mover(discrete_movers, movers_for_main)
        floppy_rbs = []
        hierarchies = []
        for mover_info in all_movers_info:
            if mover_info['mover_type'] in ('RigidBodyMover',):
                floppy_rbs.append(mover_info['object'])
            else:
                hierarchies.append(IMP.atom.Hierarchy(mover_info['object']))
        if len(hierarchies) > 0:
            sphere = r.move_floppy_rbs_close_to_hierarchies(floppy_rbs, hierarchies)
            r.model.update()
        else:
            print('WARNING: Empty hierarchies list for move_floppy_rbs_close_to_hierarchies')
            print('   Will continue without moving')

        # r.write_as_rmf('/tmp/test.rmf', [])

    score = sf.evaluate(False)
    print('score', score)
    print('Restraints for the main optimization:')
    print_restraints(r, score_functions_restraints[params.score_func])

    for restr in score_functions_restraints[params.score_func]:
        IMP.pmi.tools.add_restraint_to_model(r.model, restr) #IMP.pmi.macros.ReplicaExchange0 does not use ScoringFunction

    rex = IMP.pmi.macros.ReplicaExchange0(r.model,
                                        root_hier=r.pmi_system.get_hierarchy(),                          # pass the root hierarchy
                                        # crosslink_restraints=r.xlink_restraints,                     # will display like XLs
                                        monte_carlo_sample_objects=movers_for_main,  # pass MC movers
                                        global_output_directory='{0}/multiscale_output{1}/'.format(self.outdir, self.prefix),
                                        # output_objects=output_objects,
                                        # monte_carlo_steps=1000, #for combs_sols100
                                        monte_carlo_steps=params.rex_monte_carlo_steps,
                                        number_of_best_scoring_models=0,      # set >0 to store best PDB files (but this is slow to do online)
                                        number_of_frames=params.rex_number_of_frames)                   # increase number of frames to get better results!
    rex.execute_macro()

    return sf


class Protocol(object):
    def __init__(self, r, params, prefix, outdir, save_traj=True, schedule_name=''):
        self.r = r
        self.params = params
        self.prefix = prefix
        self.outdir = outdir
        self.save_traj = save_traj
        self.set_return_best = False
        self.schedule_name = schedule_name
        ###########################################################################
        ############          Set trajectory recording.                ############
        ###########################################################################

        if save_traj:
            self.set_traj_output()

        ###########################################################################
        ############          Set progress reporting.                  ############
        ###########################################################################

        self.print_opt_state = PrintOptimizerState(r.model, 'PrintOptimizerState')
        self.print_opt_state.set_period(params.print_frame_period)

    def set_traj_output(self):
        opt_states = []
        rmfs = []
        trajfilenames = []
        print("Setting files with optimization trajectories")
        if self.schedule_name:
            dirname = 'traj_'+self.schedule_name
        else:
            dirname = 'traj'
        os.makedirs(os.path.join(self.outdir, dirname), exist_ok=True)
        trajfilename = self.prefix + 'traj.rmf'
        if self.outdir:
            trajfilename = os.path.join(self.outdir, dirname, trajfilename)

        trajfilenames = []
        if len(self.r.pmi_system.states) > 1:
            for state_i, state in enumerate(self.r.pmi_system.states):
                trajfilenames.append(trajfilename.replace('traj.rmf', 'traj'+str(state_i)+'.rmf'))
        else:
            trajfilenames = [trajfilename]

        for state, trajfilename in zip(self.r.pmi_system.states, trajfilenames):
            rmf = RMF.create_rmf_file(trajfilename)


            rmf.set_description("Optimization\n")

            IMP.rmf.add_hierarchy(rmf, state.get_hierarchy())
            
            opt_state = IMP.rmf.SaveOptimizerState(self.r.model, rmf)
            # opt_state.update_always("initial conformation")

            #set period will save frames each:
            # o update_always() call
            # o optimize() call
            # o every traj_frame_period accepted move
            opt_state.set_period(self.params.traj_frame_period)
            opt_states.append(opt_state)
            rmfs.append(rmf)

        self.opt_states = opt_states
        self.traj_rmfs = rmfs
        self.trajfilenames = trajfilenames

    def set_logs(self):
        if self.schedule_name:
            dirname = 'logs_'+self.schedule_name
        else:
            dirname = 'logs'
        if self.params.print_log_scores_to_files:
            os.makedirs(os.path.join(self.outdir, dirname), exist_ok=True)
            log_scores_filename = os.path.join(self.outdir, dirname, self.prefix + 'scores_log.txt')
            log_scores_opt_state = IMP.core.WriteRestraintScoresOptimizerState(self.restraints, log_scores_filename)
            self.o_main.add_optimizer_state(log_scores_opt_state)
            log_scores_opt_state.set_period(self.params.print_log_scores_to_files_frame_period)

        if self.params.print_total_score_to_files:
            os.makedirs(os.path.join(self.outdir, dirname), exist_ok=True)
            log_scores_filename = os.path.join(self.outdir, dirname, self.prefix + 'total_score_log.txt')
            self.log_scores_fh = open(log_scores_filename, 'w')
            self.log_total_scores_opt_state = WriteTotalScoreOptimizerState(self.sf, self.log_scores_fh, self.r.model)
            self.o_main.add_optimizer_state(self.log_total_scores_opt_state)
            self.log_total_scores_opt_state.set_period(self.params.print_total_score_to_files_frame_period)

        self.total_scores_opt_state = RecordTotalScoreOptimizerState(self.sf, self.r.model)
        self.print_opt_state.set_period(1)

        if self.save_traj:
            for traj_rmf in self.traj_rmfs:
                total_score = IMP.RestraintSet(self.r.model, 1.0, 'total_score')
                restraints = self.restraints
                total_score.add_restraints(restraints)
                IMP.rmf.add_restraints(traj_rmf, restraints+[total_score])
            for opt_state in self.opt_states:
                opt_state.update_always("first conformation")
                self.o_main.add_optimizer_state(opt_state)

    def set_movers(self, get_movers_fn):
        self.movers, self.all_movers_info = get_movers_fn(self.r, protocol=self.params.protocol)
        print("Movers for the optimization:", self.movers)
        print("Total number of movers: ", len(self.movers))

    def create_scoring_function(self, restraints):
        self.sf = IMP.core.RestraintsScoringFunction(restraints, "scoring function")
        self.restraints = restraints

        #TODO: keep for future
        # if hasattr(self.params, 'score_func_preconditioned_mc'):
        #     sf_precond_cfg = self.params.scoring_functions[self.params.score_func_preconditioned_mc]
        #     restraints = []
        #     for restr in sf_precond_cfg['restraints']:
        #         restraints.extend(self.all_restraints[restr])        
        #     self.sf_precond = IMP.core.RestraintsScoringFunction(restraints, "preconditioned scoring function")

        # else:
        #     self.sf_precond = None

    def process_output(self, save_rmf_models):
        print('Output files:')
        if self.save_traj:
            for opt_state in self.opt_states:
                opt_state.update_always("last conformation")
            print(', '.join(self.trajfilenames))

        modelfilename = self.prefix + 'model.rmf'
        if self.outdir:
            modelfilename = os.path.join(self.outdir, 'models_rmf', modelfilename)
            os.makedirs(os.path.join(self.outdir, 'models_rmf'), exist_ok=True)

        if save_rmf_models:
            modelfilenames = []
            if len(self.r.pmi_system.states) > 1:
                for state_i, state in enumerate(self.r.pmi_system.states):
                    modelfilenames.append(modelfilename.replace('model.rmf', 'model'+str(state_i)+'.rmf'))
            else:
                modelfilenames = [modelfilename]

            
            print(', '.join(modelfilenames))

            self.r.write_states_rmf(modelfilenames, self.restraints)

        outfilename = self.prefix + 'model.txt'
        if self.outdir:
            os.makedirs(os.path.join(self.outdir, 'models_txt'), exist_ok=True)
            outfilename = os.path.join(self.outdir, 'models_txt', outfilename)

        score_fn = self.prefix + 'scores.txt'
        if self.outdir:
            os.makedirs(os.path.join(self.outdir, 'scores'), exist_ok=True)
            score_fn = os.path.join(self.outdir, 'scores', score_fn)

        outfilenames = []
        score_fns = []
        if len(self.r.pmi_system.states) > 1:
            for state_i, state in enumerate(self.r.pmi_system.states):
                outfilenames.append(outfilename.replace('model.txt', 'model'+str(state_i)+'.txt'))
                score_fns.append(score_fn.replace('scores.txt', 'scores'+str(state_i)+'.txt'))
        else:
            outfilenames = [outfilename]
            score_fns = [score_fn]
        
        print(', '.join(outfilenames))
        print(', '.join(score_fns))

        self.r.write_solution_temp(outfilenames)

        for score_fn in score_fns:
            with open(score_fn, 'w') as score_f:
                score_f.write('score ' + str(self.sf.evaluate(False)) + '\n')

                for restr in self.restraints:
                    score_f.write(','.join(map(str, (restr, restr.get_score(), restr.evaluate(False), restr.get_weight())))+'\n')

        if hasattr(self, 'log_scores_fh'):
            self.log_scores_fh.close()

    def collect_stats(self):
        self.stats['number_of_proposed_steps'] += self.o_main.get_number_of_proposed_steps() + 1 #add one as every end of optimize() saves a frame
        self.stats['number_of_accepted_steps'] += self.o_main.get_number_of_accepted_steps() + 1
        self.stats['number_of_downward_steps'] += self.o_main.get_number_of_downward_steps() + 1
        self.stats['number_of_upward_steps'] += self.o_main.get_number_of_upward_steps() + 1
        if self.stats['number_of_proposed_steps'] != 0:
            self.stats['fraction of accepted steps'] = self.stats['number_of_accepted_steps'] / float(self.stats['number_of_proposed_steps'])
        else:
            self.stats['fraction of accepted steps'] = 0

    def reset_stats(self):
        self.stats = {
            'number_of_proposed_steps': 0,
            'number_of_accepted_steps': 0,
            'number_of_downward_steps': 0,
            'number_of_upward_steps': 0,
            'fraction of accepted steps': 0.0
        }

    def print_stats(self):
        score = self.sf.evaluate(False)

        print()
        print("Optimization statistics:")
        if self.set_return_best:
            print('Best accepted energy:', self.o_main.get_best_accepted_energy())
        print('Last accepted energy:', self.o_main.get_last_accepted_energy())

        print('Number of proposed steps', self.stats['number_of_proposed_steps'])
        print('Number of accepted_steps', self.stats['number_of_accepted_steps'])
        print('Fraction of accepted steps', self.stats['fraction of accepted steps'])
        print('Number of downward_steps', self.stats['number_of_downward_steps'])
        print('Number of upward_steps', self.stats['number_of_upward_steps'])

        print("Mover, Number of proposed moves, Number of accepted moves, Fraction of accepted moves")
        printed_movers = [] #in case the mover was added more than once, print stats only once
        for mover in self.movers:
            if mover not in printed_movers:
                mv_frac = 0
                if mover.get_number_of_proposed() != 0:
                    mv_frac = mover.get_number_of_accepted() / float(mover.get_number_of_proposed())
                print(mover, mover.get_number_of_proposed(), mover.get_number_of_accepted(), mv_frac)
                printed_movers.append(mover)

        print()
        print('score', score)
        print_restraints(self.r, self.restraints)

class DeNovoMCSAProtocol(Protocol):
    def move_floppy_rbs_close_to_hierarchies(self):
        apply_each_mover(self.r.model, self.movers)

        floppy_rbs = []
        hierarchies = []

        for mover_info in self.all_movers_info:
            if mover_info['mover_type'] in ('RigidBodyMover',):
                floppy_rbs.append(mover_info['object'])
            elif mover_info['mover_type'] not in ('BallMover',):
                hierarchies.append(IMP.atom.Hierarchy(mover_info['object']))

        if len(hierarchies) > 0:
            sphere = self.r.move_floppy_rbs_close_to_hierarchies(floppy_rbs, hierarchies)
            self.r.model.update()
        else:
            print('WARNING: Empty hierarchies list for move_floppy_rbs_close_to_hierarchies')
            print('   Will continue without moving')

    def __init__(self, r, params, prefix, outdir, save_traj=True, schedule_name=''):
        super().__init__(r, params, prefix, outdir, save_traj, schedule_name)

    def run(self, SA_schedule, move_floppy_rbs_close_to_hierarchies=False):
        if move_floppy_rbs_close_to_hierarchies:
            self.move_floppy_rbs_close_to_hierarchies()

        if self.params.before_opt_fn:
            self.params.before_opt_fn(r, self.all_movers_info, self.o_main)

        self.o_main = IMP.core.MonteCarlo(self.r.model)
        if self.movers:
            main_serial_mover = IMP.core.SerialMover(self.movers)
            self.o_main.add_mover(main_serial_mover)
        self.o_main.set_scoring_function(self.sf)
        self.o_main.add_optimizer_state(self.print_opt_state)
        self.o_main.set_return_best(self.set_return_best)

        self.set_logs()

        if self.params.stop_on_convergence:
            self.o_main.add_optimizer_state(self.total_scores_opt_state)

        score = self.sf.evaluate(False)
        print('score', score)
        print_restraints(self.r, self.restraints)
        print("Setting up optimization")

        for kt, steps in SA_schedule:
            self.reset_stats()
            print('kt {0}'.format(kt))
            self.o_main.set_kt(kt)
            if self.params.print_total_score_to_files:
                self.log_total_scores_opt_state.prefix = str(kt)
            
            if self.params.stop_on_convergence and steps > self.params.no_frames_for_convergence:
                stages = [self.params.no_frames_for_convergence] * int(steps // self.params.no_frames_for_convergence)
                for stage in stages:
                    self.o_main.optimize(stage)
                    if len(self.total_scores_opt_state.scores) >= self.params.no_frames_for_convergence and convergence.convergent(self.total_scores_opt_state.scores[-self.params.no_frames_for_convergence:]):
                        print('Converged at kt {0}. Skipping to the next step'.format(kt))
                        self.total_scores_opt_state.reset()
                        break
            else:
                self.o_main.optimize(steps)

            self.collect_stats()
            self.print_stats()

        return self.sf


class DeNovoMCSACGProtocol(DeNovoMCSAProtocol):

    def __init__(self, r, params, prefix, outdir, save_traj=True, schedule_name=''):
        super().__init__(r, params, prefix, outdir, save_traj, schedule_name)

    def create_scoring_function(self, restraints, restraints_for_cg=None):
        super().create_scoring_function(restraints)

        if restraints_for_cg:
            self.score_func_for_CG = IMP.core.RestraintsScoringFunction(restraints_for_cg, "scoring function for CG")
        else:
            self.score_func_for_CG = self.sf


    def run(self, SA_schedule):

        if self.params.before_opt_fn:
            self.params.before_opt_fn(r, self.all_movers_info, self.o_main)

        self.o_main = IMP.core.MonteCarlo(self.r.model)
        if self.movers:
            main_serial_mover = IMP.core.SerialMover(self.movers)
            self.o_main.add_mover(main_serial_mover)
        self.o_main.set_scoring_function(self.sf)
        self.o_main.add_optimizer_state(self.print_opt_state)
        self.o_main.set_return_best(self.set_return_best)

        self.set_logs()

        if self.params.stop_on_convergence:
            self.o_main.add_optimizer_state(self.total_scores_opt_state)

        score = self.sf.evaluate(False)
        print('score', score)
        print_restraints(self.r, self.restraints)
        print("Setting up optimization")

        all_rbs = []
        for serie in self.r.config.series:
            for state_idx, state_mols in enumerate(serie.molecules):
                for mol in state_mols:
                    rbs, beads = IMP.pmi.tools.get_rbs_and_beads(mol.get_hierarchy())
                    all_rbs.extend(rbs)

        for kt, steps in SA_schedule:
            print('kt {0}'.format(kt))
            self.o_main.set_kt(kt)
            if self.params.print_total_score_to_files:
                self.log_total_scores_opt_state.prefix = str(kt)

            self.reset_stats()

            #optimize with CG after every MC mover:
            if len(self.movers) == 0:
                print('No MonteCarlo Movers in the list!')
                sys.exit()
            stages = [len(self.movers)] * int(steps // len(self.movers))

            last_number_of_accepted_steps = 0
            for stage in stages:
                self.o_main.optimize(stage)
                self.r.model.update() #just to be sure all constraints are updated
                cg = IMP.core.ConjugateGradients(self.r.model)
                cg.set_scoring_function(self.score_func_for_CG)
                # if self.save_traj:
                #     for opt_state in opt_states:
                #         cg.add_optimizer_state(opt_state)

                optimize_beads_with_cg(all_rbs, cg, n_steps=self.params.number_of_cg_steps_for_flex_beads)
                # if self.o_main.get_number_of_accepted_steps() > last_number_of_accepted_steps:
                #     #optimize with cg only if anything changed in MC
                    

                #     cg = IMP.core.ConjugateGradients(self.r.model)
                #     cg.set_scoring_function(self.score_func_for_CG)
                #     # if options.save_traj:
                #     #     for opt_state in opt_states:
                #     #         cg.add_optimizer_state(opt_state)
                # optimize_beads_with_cg(all_rbs, cg, n_steps=self.params.number_of_cg_steps_for_flex_beads)
                #     last_number_of_accepted_steps = self.o_main.get_number_of_accepted_steps()
                #     # self.print_stats()
                self.r.model.update() #just to be sure all constraints are updated

                self.collect_stats()
                #further optimize after all movers are done

                # cg = IMP.core.ConjugateGradients(self.r.model)
                # cg.set_scoring_function(self.score_func_for_CG)
                # if self.save_traj:
                #     for opt_state in opt_states:
                #         cg.add_optimizer_state(opt_state)

                # optimize_beads_with_cg(all_rbs, cg, n_steps=self.params.number_of_cg_steps_for_flex_beads)

                if self.params.stop_on_convergence:
                    if len(self.total_scores_opt_state.scores) >= self.params.no_frames_for_convergence and convergence.convergent(self.total_scores_opt_state.scores[-self.params.no_frames_for_convergence:]):
                        print('Converged at kt {0}. Skipping to the next step'.format(kt))
                        self.total_scores_opt_state.reset()
                        break

            self.print_stats()

        return self.sf


class AllCombinationsProtocol(Protocol):

    def __init__(self, r, params, prefix, outdir, save_traj=False, schedule_name=''):
        super().__init__(r, params, prefix, outdir, save_traj, schedule_name)

    def run(self):
        if self.params.ntasks == 1:
            self.all_combinations_protocol_single()
        else:
            self.all_combinations_parallel()

        return self.sf

    def all_combinations_protocol_single(self):
        """
        !!!!!! Dirty code - do not use unles jou know wat are you doink !!!!!!!
        """

        tr3d_lists = []
        counts = 1
        for mover in self.movers:
            tr3ds = []
            if hasattr(mover, 'tr3ds'):
                for tr3d_idx, tr3d in enumerate(mover.tr3ds):
                    tr3ds.append({
                            'mover': mover,
                            'tr3d': tr3d,
                            'tr3d_idx': tr3d_idx
                        })
                print('{0} fits for {1}'.format(len(tr3ds), mover))
                counts = counts * len(tr3ds)
                tr3d_lists.append(tr3ds)

        combinations = itertools.product(*tr3d_lists)
        print('Will do ', counts, ' combinations')


        from IMP.algebra import ReferenceFrame3D
        for i, comb in enumerate(combinations):
            for tr3d_dict in comb:
                mover = tr3d_dict['mover']
                tr3d = tr3d_dict['tr3d']
                rb = IMP.core.RigidBody(mover.get_model(), mover.pi)
                rb.set_reference_frame(ReferenceFrame3D(mover.ini_transformation))
                IMP.core.transform(rb, tr3d)
                mover.tr3d_idx = tr3d_dict['tr3d_idx']
            self.r.model.update()
            modelfilename = self.prefix + '_' + str(i) + '_model.rmf'
            if self.outdir:
                modelfilename = os.path.join(self.outdir, 'models_rmf', modelfilename)
                subprocess.call('mkdir -p {0}/models_rmf'.format(self.outdir), shell=True)

            # if options.save_rmf_models:
            #     modelfilenames = []
            #     if len(self.r.pmi_system.states) > 1:
            #         for state_i, state in enumerate(r.pmi_system.states):
            #             modelfilenames.append(modelfilename.replace('model.rmf', 'model'+str(state_i)+'.rmf'))
            #     else:
            #         modelfilenames = [modelfilename]

                
            #     print(modelfilenames)

                # r.write_states_rmf(modelfilenames, score_functions_restraints[params.score_func])

            outfilename = self.prefix + '_' + str(i) + '_model.txt'
            if self.outdir:
                subprocess.call('mkdir -p {0}/models_txt'.format(self.outdir), shell=True)
                outfilename = os.path.join(self.outdir, 'models_txt', outfilename)

            score_fn = self.prefix + '_' + str(i) + '_scores.txt'
            if self.outdir:
                subprocess.call('mkdir -p {0}/scores'.format(self.outdir), shell=True)
                score_fn = os.path.join(self.outdir, 'scores', score_fn)

            outfilenames = []
            score_fns = []
            if len(self.r.pmi_system.states) > 1:
                for state_i, state in enumerate(r.pmi_system.states):
                    outfilenames.append(outfilename.replace('model.txt', 'model'+str(state_i)+'.txt'))
                    score_fns.append(score_fn.replace('scores.txt', 'scores'+str(state_i)+'.txt'))
            else:
                outfilenames = [outfilename]
                score_fns = [score_fn]
            
            print(outfilenames)
            print(score_fns)

            self.r.write_solution_temp(outfilenames)

            for score_fn in score_fns:
                with open(score_fn, 'w') as score_f:
                    score_f.write('score ' + str(self.sf.evaluate(False)) + '\n')

                    for restr in self.restraints:
                        score_f.write(','.join(map(str, (restr, restr.get_score(), restr.evaluate(False), restr.get_weight())))+'\n')
        return sf

    def all_combinations_parallel(self):
        """
        !!!!!! Dirty code - do not use unles jou know wat are you doink !!!!!!!
        """

        tr3d_lists = []
        counts = 1
        for mover in self.movers:
            tr3ds = []
            if hasattr(mover, 'tr3ds'):
                for tr3d_idx, tr3d in enumerate(mover.tr3ds):
                    tr3ds.append({
                            'mover': mover,
                            'tr3d': tr3d,
                            'tr3d_idx': tr3d_idx
                        })
                print('{0} fits for {1}'.format(len(tr3ds), mover))
                counts = counts * len(tr3ds)
                tr3d_lists.append(tr3ds)


        print('Will do ', counts, ' combinations')

        #inneficient version
        combinations = list(itertools.product(*tr3d_lists))
        chunks = chunkIt(combinations, self.params.ntasks)
        
        #more efficient but also rather experimental
        # combinations = itertools.product(*tr3d_lists)    
        # chunks = chunkIt_1(combinations, self.params.ntasks, counts)


        from IMP.algebra import ReferenceFrame3D
        from multiprocessing import Process, Queue

        def make_combinations(chunk, prefix):
            outfilename = self.prefix + '_' + prefix + '_models.txt'
            if self.outdir:
                subprocess.call('mkdir -p {0}/models_txt'.format(self.outdir), shell=True)
                outfilename = os.path.join(self.outdir, 'models_txt', outfilename)

            score_fn = self.prefix + '_' + prefix + '_scores.txt'
            if self.outdir:
                subprocess.call('mkdir -p {0}/scores'.format(self.outdir), shell=True)
                score_fn = os.path.join(self.outdir, 'scores', score_fn)

            outfilenames = []
            score_fns = []
            if len(self.r.pmi_system.states) > 1:
                for state_i, state in enumerate(r.pmi_system.states):
                    outfilenames.append(outfilename.replace('models.txt', 'models'+str(state_i)+'.txt'))
                    score_fns.append(score_fn.replace('scores.txt', 'scores'+str(state_i)+'.txt'))
            else:
                outfilenames = [outfilename]
                score_fns = [score_fn]
            
            print(outfilenames)
            print(score_fns)
            for i, comb in enumerate(chunk):
                if (i>0 and i%100 == 0):
                    print('Did {0} combinations for {1}'.format(i, prefix))
                for tr3d_dict in comb:
                    mover = tr3d_dict['mover']
                    tr3d = tr3d_dict['tr3d']
                    rb = IMP.core.RigidBody(mover.get_model(), mover.pi)
                    rb.set_reference_frame(ReferenceFrame3D(mover.ini_transformation))
                    IMP.core.transform(rb, tr3d)
                    mover.tr3d_idx = tr3d_dict['tr3d_idx']
                self.r.model.update()
                for state_rbs, outfilename in zip(self.r.rbs_temp, outfilenames):
                    solution = defaultdict(list)
                    for rb in state_rbs:
                        tr3d = rb.get_reference_frame().get_transformation_to() / self.r.rbs_ini_tr3ds[rb]

                        row1 = list(tr3d.get_rotation().get_rotation_matrix_row(0))
                        row2 = list(tr3d.get_rotation().get_rotation_matrix_row(1))
                        row3 = list(tr3d.get_rotation().get_rotation_matrix_row(2))
                        trans = [x for x in tr3d.get_translation()]

                        name = rb.get_name()

                        solution[name].append({
                            'rot': [row1, row2, row3],
                            'trans': trans
                            })

                    with open(outfilename, 'a') as f:
                        f.write('model ' + self.prefix + '_' + prefix + '_' + str(i) + '\n')
                        f.write(json.dumps(solution, indent=None)+'\n')


                for score_fn in score_fns:
                    with open(score_fn, 'a') as score_f:
                        score_f.write('model ' + self.prefix + '_' + prefix + '_' + str(i) + '\n')
                        score_f.write('score ' + str(self.sf.evaluate(False)) + '\n')

                        for restr in self.restraints:
                            score_f.write(','.join(map(str, (restr, restr.get_score(), restr.evaluate(False), restr.get_weight())))+'\n')
        

        processes = []
        for i, chunk in enumerate(chunks):
            print(i)
            chunk = list(chunk) #must pass list to Process, if I pass iterator chunk (which comes from the chunk generator above, I got repetitions of combinations
            proc = Process(target=make_combinations, args=(chunk, 'run'+str(i)))
            processes.append(proc)
            proc.start()
        for p in processes:
            p.join()


class RefineProtocol(Protocol):
    def __init__(self, r, params, prefix, outdir, save_traj=True, schedule_name=''):
        super().__init__(r, params, prefix, outdir, save_traj, schedule_name)

    def create_scoring_function(self, restraints, restraints_for_cg=None):
        super().create_scoring_function(restraints)
        self.restraints_for_cg = restraints_for_cg
        if restraints_for_cg:
            self.score_func_for_CG = IMP.core.RestraintsScoringFunction(restraints_for_cg, "scoring function for CG")
        else:
            self.score_func_for_CG = self.sf

    def run(self, SA_schedule):

        if self.params.before_opt_fn:
            self.params.before_opt_fn(r, self.all_movers_info, self.o_main)

        self.o_main = IMP.core.MonteCarlo(self.r.model)
        if self.movers:
            serial_mover = IMP.core.SerialMover(self.movers)
            self.o_main.add_mover(serial_mover)
        self.o_main.set_scoring_function(self.sf)
        self.o_main.add_optimizer_state(self.print_opt_state)
        self.o_main.set_return_best(self.set_return_best)

        self.set_logs()

        if self.params.stop_on_convergence:
            self.o_main.add_optimizer_state(self.total_scores_opt_state)

        score = self.sf.evaluate(False)
        print('score', score)
        print_restraints(self.r, self.restraints)
        print("Setting up optimization")


        all_rbs = []
        all_beads = []
        for serie in self.r.config.series:
            for state_idx, state_mols in enumerate(serie.molecules):
                for mol in state_mols:
                    rbs, beads = IMP.pmi.tools.get_rbs_and_beads(mol.get_hierarchy())
                    all_rbs.extend(rbs)
                    all_beads.extend(beads)

        # self.r.write_as_rmf('ini.rmf', [])
        # sys.exit()
        
        if all_beads:
            if self.movers:
                cg = IMP.core.ConjugateGradients(self.r.model)
                cg.set_scoring_function(self.score_func_for_CG)
                optimize_beads_with_cg(all_rbs, cg, n_steps=self.params.number_of_cg_steps_for_flex_beads)
                for kt, steps in SA_schedule:
                    self.reset_stats()
                    print('kt {0}'.format(kt))
                    if self.params.print_total_score_to_files:
                        self.log_total_scores_opt_state.prefix = str(kt)
                    stages = [len(self.movers)] * int((steps / len(self.movers)))
                    last_number_of_accepted_steps = 0
                    print(kt, steps, stages)
                    for stage in stages:
                        self.o_main.optimize(stage)
                        cg = IMP.core.ConjugateGradients(self.r.model)
                        cg.set_scoring_function(self.score_func_for_CG)
                        # if self.save_traj:
                        #     for opt_state in opt_states:
                        #         cg.add_optimizer_state(opt_state)

                        optimize_beads_with_cg(all_rbs, cg, n_steps=self.params.number_of_cg_steps_for_flex_beads)
                        # if self.o_main.get_number_of_accepted_steps() > last_number_of_accepted_steps:
                        #     #optimize with cg only if anything changed in MC
                            

                        #     cg = IMP.core.ConjugateGradients(self.r.model)
                        #     cg.set_scoring_function(self.score_func_for_CG)
                        #     # if self.save_traj:
                        #     #     for opt_state in opt_states:
                        #     #         cg.add_optimizer_state(opt_state)
                        # optimize_beads_with_cg(all_rbs, cg, n_steps=cg_steps)
                        #     last_number_of_accepted_steps = self.o_main.get_number_of_accepted_steps()
                        self.r.model.update() #just to be sure all constraints are updated
                        self.collect_stats()

                        if self.params.stop_on_convergence:
                            if len(self.total_scores_opt_state.scores) >= self.params.no_frames_for_convergence and convergence.convergent(self.total_scores_opt_state.scores[-self.params.no_frames_for_convergence:]):
                                print('Converged at kt {0}. Skipping to the next step'.format(kt))
                                self.total_scores_opt_state.reset()
                                break

                    self.r.model.update() #just to be sure all constraints are updated
                    self.print_stats()
                    # for mover in self.movers:
                    #     mover.reset_statistics()
            else: #scenario when only flexible beads are optimized
                print("No movable rigid bodies found. Optimizing flexible beads using Conjugate Gradients")
                print("Optimizing...")
                cg = IMP.core.ConjugateGradients(r.model)
                cg.set_scoring_function(self.score_func_for_CG)
                cg.add_optimizer_state(print_opt_state)
                if self.save_traj:
                    for opt_state in opt_states:
                        cg.add_optimizer_state(opt_state)

                optimize_beads_with_cg(all_rbs, cg, n_steps=self.params.number_of_cg_steps_for_flex_beads)                
                self.r.model.update() #just to be sure all constraints are updated
        else:
            for kt, steps in SA_schedule:
                self.reset_stats()
                print('kt {0}'.format(kt))
                if self.params.print_total_score_to_files:
                    self.log_total_scores_opt_state.prefix = str(kt)

                if self.params.stop_on_convergence and steps > self.params.no_frames_for_convergence:
                    stages = [self.params.no_frames_for_convergence] * int(steps // self.params.no_frames_for_convergence)
                    for stage in stages:
                        self.o_main.optimize(stage)
                        self.r.model.update() #just to be sure all constraints are updated
                        self.collect_stats()
                        if len(self.total_scores_opt_state.scores) >= self.params.no_frames_for_convergence and convergence.convergent(self.total_scores_opt_state.scores[-self.params.no_frames_for_convergence:]):
                            print('Converged at kt {0}. Skipping to the next step'.format(kt))
                            self.total_scores_opt_state.reset()
                            break
                else:
                    self.o_main.optimize(steps)
                    self.r.model.update() #just to be sure all constraints are updated
                    self.collect_stats()

                self.r.model.update() #just to be sure all constraints are updated
                self.print_stats()
                # for mover in self.movers:
                #     mover.reset_statistics()

        return self.sf


#not working, can't override do_step
class PreconditionedMonteCarlo(IMP.core.MonteCarlo):
    def __init__(self, m):
        '''
        tr3ds - trd3s to sample from
        '''
        raise Exception("Not implemented: not working, can't override do_step")
        IMP.core.MonteCarlo.__init__(self, m)

    def do_step(self):
        #not working, can't override do_step
        moved = self.do_move()
        self.cleanup(self)
        energy = self.do_evaluate(moved.get_moved_particles())
        self.do_accept_or_reject_move(energy, moved.get_proposal_ratio())
        cleanup.reset()


def get_movers_for_ini_opt(r, protocol='', precondition_sf=None):
    """
    Define which movers to use for the ini optimization.

    The MultiRepresentation class instantiates various movers for rigid bodies and beads.
    In this function you decide which ones should be used. To change the default, you can override the function by defining it in params.py,
    e.g. by copying the entire function and editing it.

    You could for example get mover except DiscreteConnMovers, to first easier place all subunits in the map,
    avoid stucking in initial positions because of parts moved by non discrete movers

    """
    all_movers = []
    all_movers_info = []
    for state_idx, state in enumerate(r.pmi_system.states):
        movers = []
        added = []

        #add discrete movers:
        movers_selection_order = ('DiscreteMover',)
        # To use DiscreteConnMover or DiscreteSoftConnMover, copy the function to params.py and uncomment the below 
        # movers_selection_order = ('DiscreteWeightedMover', 'DiscreteConnMover', 'DiscreteMover')
        # movers_selection_order = ('DiscreteWeightedMover'. 'DiscreteSoftConnMover', 'DiscreteMover')
        for mover_type in movers_selection_order:
            for mover_info in r.movers_map.map[state_idx]:
                if mover_info['mover_type'] == mover_type and mover_info['object'] not in added and not r.is_sym_constrained(mover_info['object']):

                    movers.append(mover_info['mover'])
                    all_movers_info.append(mover_info)
                    added.append(mover_info['object'])

        #add normal movers for rbs not moving with discrete movers
        #for those symmetry constrained, adding only for reference rbs
        for mover_info in r.movers_map.map[state_idx]:
            if mover_info['mover_type'] in ('RigidBodyMover',) and mover_info['object'] not in added and not r.is_sym_constrained(mover_info['object']):
                if protocol in ('denovo_MC-SA-CG', 'denovo_MC-SA'):
                    for i in range(10): #Move 10 times, to catch up with DiscreteMovers
                        movers.append(mover_info['mover'])
                else:
                    movers.append(mover_info['mover'])
                added.append(mover_info['object'])
                all_movers_info.append(mover_info)

        if protocol != 'denovo_MC-SA-CG':
            no_movers = 1
            if protocol != 'refine':
                no_movers = 20 #Move 20 times, to catch up with DiscreteMovers
            #add bead movers
            for mover_info in r.movers_map.map[state_idx]:
                if mover_info['mover_type'] in ('BallMover', ) and not r.is_sym_constrained(mover_info['object']):
                    for i in range(no_movers): 
                        movers.append(mover_info['mover'])
                    added.append(mover_info['object'])
                    all_movers_info.append(mover_info)

        all_movers.extend(movers)

    return all_movers, all_movers_info

def get_movers_for_main_opt(r, protocol='', precondition_sf=None):
    """
    Define which movers to use for the main optimization.

    The MultiRepresentation class instantiates various movers for rigid bodies and beads.
    In this function you decide which ones should be used. To change the default, you can override the function by defining it in params.py,
    e.g. by copying the entire function and editing it.
    """
    all_movers = []
    all_movers_info = []
    for state_idx, state in enumerate(r.pmi_system.states):
        movers = []
        added = []

        #add discrete movers:
        movers_selection_order = ('DiscreteMover',)
        # To use DiscreteConnMover or DiscreteSoftConnMover, copy the function to params.py and uncomment the below 
        # movers_selection_order = ('DiscreteWeightedMover', 'DiscreteConnMover', 'DiscreteMover')
        # movers_selection_order = ('DiscreteWeightedMover'. 'DiscreteSoftConnMover', 'DiscreteMover')
        for mover_type in movers_selection_order:
            for mover_info in r.movers_map.map[state_idx]:
                if mover_info['mover_type'] == mover_type and mover_info['object'] not in added and not r.is_sym_constrained(mover_info['object']):

                    movers.append(mover_info['mover'])
                    all_movers_info.append(mover_info)
                    added.append(mover_info['object'])

        #add normal movers for rbs not moving with discrete movers
        #for those symmetry constrained, adding only for reference rbs
        for mover_info in r.movers_map.map[state_idx]:
            if mover_info['mover_type'] in ('RigidBodyMover',) and mover_info['object'] not in added and not r.is_sym_constrained(mover_info['object']):
                if protocol in ('denovo_MC-SA-CG', 'denovo_MC-SA'):
                    for i in range(10): #Move 10 times, to catch up with DiscreteMovers
                        movers.append(mover_info['mover'])
                else:
                    movers.append(mover_info['mover'])
                added.append(mover_info['object'])
                all_movers_info.append(mover_info)

        if protocol != 'denovo_MC-SA-CG':
            #add bead movers
            for mover_info in r.movers_map.map[state_idx]:
                if mover_info['mover_type'] in ('BallMover', ) and not r.is_sym_constrained(mover_info['object']):
                    for i in range(20): #Move 20 times, to catch up with DiscreteMovers
                        movers.append(mover_info['mover'])
                    added.append(mover_info['object'])
                    all_movers_info.append(mover_info)

        all_movers.extend(movers)

    return all_movers, all_movers_info

def get_movers_for_refine(r, protocol='', precondition_sf=None):
    all_movers = []
    all_movers_info = []
    for state_idx, state in enumerate(r.pmi_system.states):
        movers = []
        added = []

        #add normal movers for rbs not moving with discrete movers
        #for those symmetry constrained, adding only for reference rbs

        for mover_info in r.movers_map.map[state_idx]:
            if mover_info['mover_type'] in ('RigidBodyMover',) and mover_info['object'] not in added and not r.is_sym_constrained(mover_info['object']):
                if precondition_sf:
                    p_mover = imp_utils1.core.PreconditionedRigidBodyMover(
                        r.model,
                        mover_info['object'],
                        precondition_sf,
                        mover_info['mover'].get_maximum_translation(),
                        mover_info['mover'].get_maximum_rotation()
                        )
                    
                    mover_to_use = p_mover
                else:
                    mover_to_use = mover_info['mover']

                        
                movers.append(mover_to_use)

                added.append(mover_info['object'])
                all_movers_info.append(mover_info)

            if mover_info['mover_type'] in ('BallMover', ) and not r.is_sym_constrained(mover_info['object']):
                for i in range(1): #Move 20 times to make some difference
                    movers.append(mover_info['mover'])
                added.append(mover_info['object'])
                all_movers_info.append(mover_info)

        all_movers.extend(movers)

    return all_movers, all_movers_info

