import sys
import importlib
from string import Template
from collections import OrderedDict
import math

from . import optimization

defaults = OrderedDict([

    ('protocol', 
        ('denovo_MC-SA',
        """Modeling protocol.
\tOptions: 

\t* denovo_MC-SA - Monte Carlo Simulated Annealing global optimization moving rigid bodies according to pre-computed position libraries, if they are defined for the rigid bodies, otherwise moving with random rotations and translations. Flexible beads are moved through Monte Carlo with random rotations and translations.

\t* denovo_MC-SA-CG - as denovo_MC-SA, but flexible beads are moved using Conjugate Gradient optimization

\t* refine - both rigid bodies and flexible beads are moved with random rotations and translations, pre-computed libraries are ignored. Flexible beads are moved using Conjugate Gradient optimization.

\t* all_combinations - generates all combinations of positions from pre-computed position libraries

\t* custom - a function with a custom protocol based on a user-defined function custom_protocol() defined in params.

\t"""
        )),
    ('do_ini_opt', (False, 'Perform initial optimization using the score_func_ini_opt scoring function?')),
    ('SA_schedule', ([(30000,1000), (2000,1000), (1000,1000)], """Simulated Annealing schedule.
\tList of (temperature, number of steps) pairs.""")),
    ('before_opt_fn', (None, 'A Python function with code that should be executed before optimization')),
    ('number_of_cg_steps_for_flex_beads', (100, 'Number of Conjugate Gradient steps per round in denovo_MC-SA-CG and refine protocols')),
    ('stop_on_convergence', (False, 'Whether to stop the current stage of Simulated Annealing when converged, working well only for optimizations with high number of steps.')),
    ('no_frames_for_convergence', (1000, 'Number of frames for evaluating convergence when stop_on_convergence is set to True')),

    ('print_frame_period', (10, 'Print every Nth frame ID in progress reporting')),
    ('traj_frame_period', (10, 'Save every Nth frame to the trajectory output file')),
    ('print_total_score_to_files', (True, 'Save frame total scores to log files?')),
    ('print_total_score_to_files_frame_period', (10, 'Print total score to log files every Nth frame')),
    ('print_log_scores_to_files', (False, 'Save frame individual restraint scores to log files?')),
    ('print_log_scores_to_files_frame_period', (10, 'Print individual restraint scores to log files every Nth frame')),

    ('struct_resolutions', ([1, 10], 'Resolutions of bead representations.')),
    ('add_missing', (False, 'Add missing regions as flexible beads? Either False or a list of selectors.')),
    ('missing_resolution', (1, 'Representation resolution for the missing regions (if add_missing is specified)')),
    ('add_rbs_from_pdbs', (True, 'Define rigid bodies automatically based on pdb_files specification in JSON?')),
    ('ca_only', (True, 'Read only Calpha from PDB structures?')),

    ('add_connectivity_restraints', (True, 'Add domain and bead connectivity restraints?')),
    ('connectivity_restraint_weight', (1.0, 'Connectivity restraint weight')),
    ('max_conn_gap', (None, 'Number of missing residues of the missing atomic region above which the restraint will not be added')),
    ('connectivity_restraint_k', (10.0, 'Connectivity restraint spring constant k.')),
    ('conn_reweight_fn', (None, """A function that accepts the following parameters:
\tmol - PMI molecule object
\tnext_resi_idx - residue of the next rigid body or bead
\tprev_resi_idx - residue of the previous rigid body or bead
\tconnectivity_restraint_weight - default weight passed to add_connectivity_restraints""")),
    ('conn_first_copy_only', (False, 'Whether to add the restraints only for the first copy of the molecule (useful for symmetrical assemblies)')),
    ('ca_ca_connectivity_scale', (0.526, """Scale the average CA-CA distance of 3.8 to account for that
\tit is unlikely that the linker is fully stretched, default: 0.526 (count 2A per peptide bond)""")),

    ('ev_restraints', ([
        {
            'name': 'ev_restraints',
            'weight': 1.0,
            'repr_resolution': 10,
            'copies': None,
            'distance_cutoff': 0.0,
            'slack': 5.0
        }
    ], """Specification of excluded volume (clash score) restraint.
\tParameters:
\t'name': a custom name
\t'weight': weight
\t'repr_resolution': which representation resolution to use for this restraint
\t'copies': which molecule copies are included in this restraint
\t'distance_cutoff': distance cutoff for non-bonded list
\t'slack': slack for non-bonded lists
\tRead more about distance_cutoff and slack: https://integrativemodeling.org/2.14.0/doc/ref/classIMP_1_1container_1_1ClosePairContainer.html#aa7b183795bd28ab268e5e84a5ad0cd99""")),

    ('add_xlink_restraints', (False, 'Add crosslink restraints?')),
    ('x_xlink_score_type', ('HarmonicUpperBound', """A type of crosslink restraint.
\n\tOptions:
\n\t'HarmonicUpperBound' - 0 below x_xlink_distance, harmonic above, distance calculated between centers of the beads
\n\t'HarmonicUpperBoundSphereDistancePairScore' - 0 below x_xlink_distance, harmonic above, distance calculated between surfaces of the beads
\n\t'XlinkScore' - 0 below x_xlink_distance, 1 above
\n\t'LogHarmonic' - A log harmonic potential with the maximum at x_xlink_distance
\n\t'CombinedHarmonic': - harmonic above distance of 35 A and log harmonic with the maximum at x_xlink_distance below 35 A
\n\t""")),
    ('x_min_ld_score', (30.0, 'Only crosslinks with the confidence score above this threshold will be used as restraints. The score must be defined in "score" column of crosslink CSV files (see also Xlink Analyzer documentation)')),
    ('x_weight', (1.0, 'Weight of crosslink restraints')),
    ('x_xlink_distance', (30.0, 'Target or maximual crosslink distance (depending on the implementation)')),
    ('x_k', (1.0, 'Spring constant for the harmonic potential')),
    ('x_inter_xlink_scale', (1.0, 'Multiply the weight of inter-molecule crosslinks by this value')),
    ('x_first_copy_only', (False, 'Apply crosslinks only to the first molecule copy of each series')),
    ('x_skip_pair_fn', (None, """A Python function to skip specific crosslinks.
\tArguments: p1, p2, component1, component2, xlink
\tReturn True to skip the crosslink""")),
    ('x_log_filename', (None, 'Optional log file name for printing more information about added and skipped crosslinks')),
    ('x_score_weighting', (False, 'Scale crosslink weights by their score')),
    ('xlink_reweight_fn', (None, """A custom Python function to scale crosslink weights.
\tArguments: xlink, weight
\tReturn final weight (float)""")),
    ('x_random_sample_fraction', (1.0, 'Take this random fraction of crosslinks for modeling')),

    ('add_symmetry_constraints', (False, 'Add symmetry constraints?')),
    ('add_symmetry_restraints', (False, 'Add symmetry restraints?')),
    ('symmetry_restraints_weight', (1.0, 'Weight of symmetry restraints')),

    ('add_parsimonious_states_restraints', (False, 'Add parsimonious states restraints?')),
    ('parsimonious_states_weight', (1.0, 'Weight')),
    ('parsimonious_states_distance_threshold', (0.0, 'Distance threshold for elastic network restraining the states')),
    ('parsimonious_states_exclude_rbs', (lambda x: False, """Python function to exclude selected rigid bodies from the restraint.
\tArguments: IMP's rigid body object
\tReturn: True if the rigid body should be excluded.""")),
    ('parsimonious_states_representation_resolution', (10, 'Representation resolution for this restraint')),
    ('parsimonious_states_restrain_rb_transformations', (True, 'Restraint rigid body transformations instead of using elastic network')),

    ('create_custom_restraints', (None, """Python function to create custom restraints.
\tArguments: imp_utils1.MultiRepresentation class.
\tReturn dictionary mapping restraint names to list of restraints""")),

    ('discrete_restraints_weight', (1.0, 'Weight for restraints derived from the pre-defined positions, e.g. precomputed libraries of fits.')),
    ('discrete_mover_weight_score_fn', (None, 'help message')),

    ('scoring_functions', ({}, 'A collection of scoring functions')),
    ('score_func', (None, 'Name of the scoring functions in the scoring_functions collection to be used for the main optimization')),
    ('score_func_for_CG', (None, """Name of the scoring functions in the scoring_functions collection to be used for conjugate gradient steps.
\tOnly restraints that have implemented derivative calculation can be used for Conjugate Gradient optimization.""")),
    ('score_func_ini_opt', (None, 'Name of the scoring functions in the scoring_functions collection to be used for the initial optimization')),
    ('score_func_preconditioned_mc', (None, 'Name of the scoring functions in the scoring_functions collection to be used for the preconditioned Monte Carlo')),

    ('add_custom_movers', (None, 'A Python function to add custom Monte Carlo movers')),
    ('custom_preprocessing', (None, """A Python function with code that should be executed before modeling protocols are initiated and after adding and setting all restraints.
\tArguments: imp_utils1.MultiRepresentation class.""")),
    
    ('rb_max_rot', (0.2, 'Maximal rotation of a rigid body in a single Monte Carlo move, in radians')),
    ('rb_max_trans', (2, 'Maximal translation of a rigid body in a single Monte Carlo move, in Angstroms')),
    ('beads_max_trans', (1, 'Maximal translation of a flexible bead in a single Monte Carlo move, in Angstroms')),
    ('randomize_initial_positions', (False, 'Randomize initial positions of all particles?')),
    ('randomize_initial_positions_remove_clashes', (False, 'Randomize initial positions of all particles and run short optimization to try removing steric clashes?')),
    ('randomize_initial_positions_center', ('local', 'A string "local" or a list/tuple of three coordinates for the center of rotation')),
    ('randomize_initial_positions_max_translation', (50, 'Maximal random translation')),
    ('randomize_initial_positions_max_rotation', (2.0*math.pi, 'Maximal random rotation')),
    ('randomize_initial_positions_repeat', (1, 'Repeat randomization procedure these many times')),
    ('randomize_initial_positions_remove_clashes', (False, 'Run optimization to remove clashes that may result from random placement?')),
    ('randomize_initial_positions_remove_clashes_iter', (1000, 'How many steps in optimization for removing clashes')),
    ('move_floppy_beads_close_to_hierarchies', (True, 'Set initial positions of beads smartly?')),
    ('move_floppy_beads_close_to_hierarchies_mode', ('flanks', 'Set initial positions of beads smartly?')),

    ('get_movers_for_main_opt', (optimization.get_movers_for_main_opt, 'Python function defining movers for the main optimization, override for custom implementations')),
    ('get_movers_for_ini_opt', (optimization.get_movers_for_ini_opt, 'Python function defining movers for the initial optimization, override for custom implementations')),
    ('get_movers_for_refine', (optimization.get_movers_for_refine, 'Python function defining movers for the refinement, override for custom implementations')),

    ('debug', (False, 'Print debug messages?')),

    ('ntasks', (1, 'Number of tasks to run for multiprocessor runs')),
    ('cluster_submission_command', (None, 'Command to run the cluster submission script, e.g. sbatch for SLURM')),
    ('run_script_templ', (Template("""
            #!/bin/bash
            #
            #SBATCH --ntasks=$ntasks
            #SBATCH --mem-per-cpu=2000
            #SBATCH --job-name=${prefix}fit
            #SBATCH --time=5-00:00:00
            #SBATCH -e $outdir/logs/${prefix}_err.txt
            #SBATCH -o $outdir/logs/${prefix}_out.txt

            echo "Running on:"
            srun hostname

            $cmd

            wait #necessary when ntasks > 1 and cmd are run in the background, otherwise job may end before the background processes end
    """), """A template for running the jobs on a computer cluster.
\tMake sure your template includes $ntasks, ${prefix}, $outdir, and $cmd"""))


])


def set_default_params(module):

    for key, values in defaults.items():
        value, help_msg = values
        if not hasattr(module, key):
            setattr(module, key, value)

    if module.score_func is None:
        print('You must define score_func')
        sys.exit()

    if module.score_func not in module.scoring_functions:
        print('score_func {0} not defined in scoring_functions'.format(module.score_func))
        sys.exit()

    if not module.score_func_for_CG:
        module.score_func_for_CG = module.score_func

    #Backward compatibility code:
    if hasattr(module, 'add_em_restraints') and module.add_em_restraints is False:
        print('add_em_restraints in params is deprecated.')
        print('Use active attribute of restraints in the JSON file')
        sys.exit()

def read_params(params_file):
    spec = importlib.util.spec_from_file_location('params', params_file)
    params = importlib.util.module_from_spec(spec)
    sys.modules['params'] = params
    spec.loader.exec_module(params)

    set_default_params(params)

    return params

if __name__ == '__main__':
    """
    Run this like this: python -m imp_utils1.params_reader
    """
    for key, values in defaults.items():
        value, help_msg = values
        print(key)
        if isinstance(value, Template):
            default_val = '\n\n\t.. code-block:: python\n\n\t    """\n{0}\n\t    """'.format(value.safe_substitute({}))
        elif callable(value):
            default_val = 'Some default function'
        else:
            default_val = '**{}**'.format(value)
        print('\t{0}, default: {1}'.format(help_msg, default_val))


