import IMP.pmi.topology as topology
import IMP.pmi.topology.system_tools as system_tools
import IMP.pmi.restraints.em
import IMP.atom
import IMP.pmi.dof
import RMF
import IMP.rmf

from itertools import product, groupby, tee
from operator import itemgetter
import pyxlinks

import superconfig
import traceback
import math
from collections import defaultdict
import weakref
import json
import copy
import random
import os
import math
import pprint
import logging

from . import core

logging.basicConfig(format='%(message)s', level=logging.INFO)

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)

def groupby_unsorted(seq, key=lambda x: x):
    """https://code.activestate.com/recipes/580800-groupby-for-unsorted-input/
    """
    indexes = defaultdict(list)
    for i, elem in enumerate(seq):
        indexes[key(elem)].append(i)
    for k, idxs in indexes.items():
        yield k, (seq[i] for i in idxs)

class Config2SystemConverter(object):
    def __init__(self, config):
        self.config = config
        self.added_mols = []
        self.xlink_restraints = []

    def get_first_mol_of_serie(self, query_mol_cfg):
        if query_mol_cfg.serie is not None:
            for mol, mol_cfg in self.added_mols:
                if query_mol_cfg.serie is mol_cfg.serie:
                    return mol, mol_cfg

        return None, None

    def create_system(self, states=1):
        self.system = topology.System()

        for state_i in range(states):
            st = self.system.create_state()

            for serie in self.config.series:
                if (serie.states is not None) and (state_i not in serie.states):
                    serie.molecules.append([])
                    serie.molecules_copy_ids.append([])
                    continue
                try:
                    state_subunit_copies = serie.subunit.copies_per_state[state_i]
                except IndexError:
                    state_subunit_copies = []
                    serie.subunit.copies_per_state.append(state_subunit_copies)
                state_serie_molecules = []
                
                for i, chain_id in enumerate(serie.chainIds):
                    if i == 0:
                        if serie.subunit.name not in st.molecules:
                            mol_seq = self.config.get_seq(serie.subunit.name)
                            if mol_seq:
                                mol = st.create_molecule(serie.subunit.name,
                                                         sequence=mol_seq,
                                                         chain_id=chain_id)

                                state_serie_molecules.append(mol)
                                state_subunit_copies.append(mol)
                            else:
                                print("No mol added because no sequence for", serie.subunit.name)
                        else:
                            if serie.inipos == 'input':
                                old_mol = st.molecules[serie.subunit.name][0]
                            else:
                                ref_serie = self.config.get_serie_by_subunit_and_name(serie.subunit, serie.ref_serie)
                                serie_state_mols = ref_serie.molecules[state_i]
                                old_mol = serie_state_mols[0]

                            if serie.inipos != 'input':
                                old_mol.create_clone(chain_id)
                                mol = st.molecules[old_mol.get_name()][-1]
                            else:
                                mol = old_mol.create_copy(chain_id)

                            state_serie_molecules.append(mol)
                            state_subunit_copies.append(mol)

                            IMP.core.Reference.setup_particle(mol.get_hierarchy(), old_mol.get_hierarchy())

                    else:
                        if serie.inipos == 'input':
                            try:
                                old_mol = state_serie_molecules[0]
                            except IndexError:
                                old_mol = None
                        else:
                            ref_serie = self.config.get_serie_by_subunit_and_name(serie.subunit, serie.ref_serie)
                            serie_state_mols = ref_serie.molecules[state_i]
                            old_mol = serie_state_mols[0]

                        if old_mol is not None:
                            if serie.mode == 'auto':
                                old_mol.create_clone(chain_id)
                                mol = st.molecules[old_mol.get_name()][-1]
                            else:
                                mol = old_mol.create_copy(chain_id)
                            
                            state_serie_molecules.append(mol)
                            state_subunit_copies.append(mol)
                                
                            IMP.core.Reference.setup_particle(mol.get_hierarchy(), old_mol.get_hierarchy())
                        else:
                            print("No mol added for copy of", serie.subunit.name)

                serie.molecules.append(state_serie_molecules)
                serie.molecules_copy_ids.append([IMP.atom.Copy(copy.get_hierarchy()).get_copy_index() for copy in state_serie_molecules])

        #CHECK:
        print("Created PMI system as follows:")
        for serie in self.config.series:
            print(serie.name, serie.subunit.name, serie.molecules)

        for state_idx, state in enumerate(self.system.states):
            print("State {0}".format(state_idx))
            print("\tMolecules:")
            for mol in state.molecules:
                print('\t\t{0} with {1} copies of {2} residues'.format(mol, len(state.molecules[mol]), len(state.molecules[mol][0].residues)))

        return self.system


class MoversMap(object):
    def __init__(self):
        self.map = defaultdict(list)

    def add(self, rb, mover, mover_type='default', state_idx=0):
        self.map[state_idx].append(
                {
                    'object': rb,
                    'mover': mover,
                    'mover_type': mover_type
                }
            )
            
    def get_mover_infos_for(self, obj, state_idx=0):
        out = []
        for mover_info in self.map[state_idx]:
            if obj == mover_info['object']:
                out.append(mover_info)

        return out


class MultiRepresentation(object):
    def __init__(self,
                config,
                pmi_system=None,
                struct_resolutions=None,
                add_missing=False,
                missing_resolution=None,
                add_rbs_from_pdbs=False,
                rb_max_rot=0.2,
                rb_max_trans=1.0,
                beads_max_trans=1.0,
                ca_only=True):

        if struct_resolutions is not None:
            self.STRUCT_RESOLUTIONS = struct_resolutions
        else:
            self.STRUCT_RESOLUTIONS = [0, 1]

        if missing_resolution is not None:
            self.MISSING_RESOLUTION = missing_resolution
        else:
            self.MISSING_RESOLUTION = 1

        self.add_rbs_from_pdbs = add_rbs_from_pdbs


        self.temp_comp_ps = {}
        self.all_restraints = {}
        self.xlink_restraints = []
        self.ev_restraints = []
        self.em_restraints = []
        self.conn_restraints = []
        self.discrete_restraints = []
        self.movers = []
        self.movers_map = MoversMap()
        self.conn_restraints_ps_temp = []
        self.rbs_temp = []
        self.rbs_ini_tr3ds = weakref.WeakKeyDictionary()

        self.sym_restraint_pairs = []
        self.sym_constraint_pairs = []
        self.config = config
        self.read_sym_tr3ds()
        if pmi_system:
            self.pmi_system = pmi_system
            for i, state in enumerate(self.pmi_system.states):
                self.rbs_temp.append([])

            try: 
                self.model = pmi_system.mdl #older PMI 
            except AttributeError:
                self.model = pmi_system.model #newer PMI
            self.universe = self.pmi_system.get_hierarchy()

            if self.add_rbs_from_pdbs and len(self.config.rigid_bodies) == 0:
                self.config.define_rbs_from_pdbs()

            for rb in self.config.rigid_bodies:
                if 'positions' in rb:
                    for pos in rb['positions']:
                        pos['tr3d'] = IMP.algebra.Transformation3D(IMP.algebra.get_rotation_from_matrix(*pos['rotation']),
                                                                IMP.algebra.Vector3D(*pos['translation']))

            self.add_structures(ca_only)
            self.add_representations(add_missing=add_missing)
            self.add_rigid_bodies_and_movers(rb_max_rot=rb_max_rot, rb_max_trans=rb_max_trans, beads_max_trans=beads_max_trans)
            




            # self.resi_map = self.make_resi_map() 

    # def make_resi_map(self):
    #     #list of states, for each state mapping of residue idx to resolutions
    #     '''
    #     [
    #         {
    #         molname: {
    #             residx: {
    #                 0: ...,
    #                 10: ...,
    #             }
    #         }
    #         }
    #     ]
    #     '''

    #     states = self.pmi_system.states
    #     for state_i, state in enumerate(states):
    #         for molname in state.molecules:
    #             for pos in range(1, len(self.config.get_seq(molname))+1):
    #                 ps = self.get_particles_for_resi(molname, pos)
    #                 print ps
    #                 for p in ps:
    #                     if p is not None:
    #                         print IMP.atom.Hierarchy(p).get_parent()

    def add_structures(self, ca_only=True):
        print('Adding structures')
        self.mols_and_resi_pdbs = []

        for cfg in self.config.struct_files:
            for comp_cfg in cfg['components']:
                for state_idx in comp_cfg['states']:
                    mols = self.get_mols_by_comp_cfg(comp_cfg, state_idx)
                    for mol in mols:
                        resi_pdbs = {}
                        for xmol, xresi_pdbs in self.mols_and_resi_pdbs:
                            if mol is xmol:
                                resi_pdbs = xresi_pdbs
                        if not mol.mol_to_clone:
                            chain_id = comp_cfg.get('chain_id')
                            if chain_id is None:
                                subunit = self.config.get_subunit_by_name(comp_cfg['subunit'])
                                if subunit is not None:
                                    chain_id = subunit.chain_ids[0]

                            # resi_ids = sorted(self.get_domain_resi_indexes_struct(mol, comp_cfg['domain']))
                            if comp_cfg.get('resi_ranges'):
                                resi_ranges = comp_cfg.get('resi_ranges')
                            else:
                                resi_ranges = [None]
                            print("Adding", comp_cfg['filename'], chain_id, 'to', mol)
                            for resi_range in resi_ranges: #TODO: this read the PDB file every time again, improve for speed?
                                atomic_res = mol.add_structure(comp_cfg['filename'],
                                                  chain_id=chain_id,
                                                  ca_only=ca_only,
                                                  res_range=resi_range,
                                                  soft_check=True)
                            for resi in atomic_res:
                                resi_pdbs[resi] = comp_cfg['filename']
                            self.mols_and_resi_pdbs.append([mol, resi_pdbs])

        print("Finished adding structures")

    def get_mol_rigid_resi_ranges(self, mol):
        """Get ranges of IDs (starting from 1) of residues that will end up in rigid bodies.
        Can be used before rigid bodies are created.

        :param mol: PMI.Molecule object

        :return: list of lists, each list is a range of residue IDs (int), starting from 1,
                 in the format [start, end]
        """
        resi_lists = []
        state_idx = self.pmi_system.states.index(mol.state)
        for rb_cfg in self.config.rigid_bodies:
            for comp_cfg in rb_cfg['components']:
                subunit = self.config.get_subunit_by_name(comp_cfg['subunit'])
                mols = self.get_mols_by_comp_cfg(comp_cfg, state_idx=state_idx, first_copy_only=False)
                for _mol in mols:
                    if _mol == mol:
                        resi_ids = self.get_resi_ids_from_comp_cfg(mol, comp_cfg, struct_only=True)
                        resi_lists.extend(list(superconfig.ranges_from_list(resi_ids)))

        resi_lists.sort(key=lambda x: x[0])

        return resi_lists

    def get_mol_rigid_resi(self, mol):
        """Get IDs (starting from 1) of residues that will end up in rigid bodies.
        Can be used before rigid bodies are created.

        :param mol: PMI.Molecule object

        :return: list of residue IDs (int), starting from 1
        """
        resi_ranges = self.get_mol_rigid_resi_ranges(mol)
        resi_ids = []
        for r_range in resi_ranges:
            try:
                resi_ids.extend(list(range(r_range[0], r_range[1]+1)))  #when range is (start, end) tuple
            except TypeError:
                resi_ids.append(r_range) #when range is int

        return resi_ids

    def get_mol_extra_breaks(self, mol):
        """Get breaks between residues coming from seperate rigid bodies.
        Otherwise particles that are supposed to go into different rigid bodies,
        may end in the same fragment and crash rigid body creation,
        which would attempt to add the fragment to two rigid bodies.

        :param mol: PMI.Molecule object

        :return: lists of residue IDs (starting from 1)
                                   after which a break between beads should be introduced.
                                   To be passed to PMI.Molecule.add_representation as bead_extra_breaks argument.
        """
        bead_extra_breaks = []
        resi_ranges = self.get_mol_rigid_resi_ranges(mol)
        for _list in resi_ranges[:-1]:
            bead_extra_breaks.append(str(_list[-1])) # pass as String for PMI to understand it as PDB numbering starting from 1

        return bead_extra_breaks

    def add_representations(self, add_missing=False):
        """Create IMP representations for each molecule.

        TODO: currently if self.MISSING_RESOLUTION is higher than 1,
        the flexible beads may join non-consecutive residues. To check and fix if necessary.

        :param add_missing: True/False to define whether to add all residues missing in the input structures OR
                            a specification of which regions to add in the format as the following example:
                            [
                                {
                                    'subunit': 'CEA7_ECOLX',
                                    'resi_ranges': [
                                        (456, 462)
                                    ]
                                },
                                {
                                    'subunit': 'IMM7_ECOLX',
                                    'resi_ranges': [
                                        (57, 63), (80-90)
                                    ]
                                }
                            ]
        """
        print('Adding representations and building the system')
        for state_idx, state in enumerate(self.pmi_system.states):
            for molname, copies in state.molecules.items():
                for mol in copies:
                    if mol.mol_to_clone is None:
                        atomic_res = mol.get_atomic_residues()
                        non_atomic_res = mol.get_non_atomic_residues()

                        rigid_resi_ids = self.get_mol_rigid_resi(mol)
                        atomic_res_rigid = [resi for resi in atomic_res if resi.get_index() in rigid_resi_ids]
                        atomic_res_non_rigid = [resi for resi in atomic_res if resi.get_index() not in rigid_resi_ids]

                        bead_extra_breaks = self.get_mol_extra_breaks(mol)

                        if len(atomic_res_rigid) > 0:
                            mol.add_representation(atomic_res_rigid,
                                                bead_extra_breaks=bead_extra_breaks,
                                                resolutions=self.STRUCT_RESOLUTIONS
                                                )

                        if atomic_res_non_rigid:
                            mol.add_representation(residues=atomic_res_non_rigid, resolutions=[self.MISSING_RESOLUTION])

                        if (add_missing or len(atomic_res) == 0) and len(non_atomic_res) > 0:
                            resi_to_add = []
                            if add_missing is True:
                                resi_to_add = non_atomic_res
                            elif hasattr(add_missing, '__iter__'):
                                for comp_cfg in add_missing:
                                    mols = self.get_mols_by_comp_cfg(comp_cfg, state_idx=state_idx)
                                    if mol in mols:
                                        resi_ids = self.get_resi_ids_from_comp_cfg(mol, comp_cfg)
                                        resi_ids_to_add = sorted( set(resi_ids) & set([r.get_index() for r in non_atomic_res]))
                                        resi_to_add = [r for r in non_atomic_res if r.get_index() in resi_ids_to_add]

                            if resi_to_add:
                                mol.add_representation(residues=resi_to_add, resolutions=[self.MISSING_RESOLUTION])

            state.build()

        print()
        print('Summary of the representations:')
        for state_idx, state in enumerate(self.pmi_system.states):
            print("State {0}".format(state_idx))
            print("\tMolecules:")
            for molname in state.molecules:
                for mol in state.molecules[molname]:
                    print('\t\t{0} has {1} residues including'.format(mol, len(mol.residues)))
                    print('\t\t\t{0} atomic residues read from PDB files'.format(len(mol.get_atomic_residues())))
                    print('\t\t\t{0} non atomic residues absent in PDB files'.format(len(mol.get_non_atomic_residues())))

                    resolutions = set(self.STRUCT_RESOLUTIONS+[self.MISSING_RESOLUTION])
                    for resolution in sorted(resolutions):
                        ps = IMP.atom.Selection(mol.get_hierarchy(),resolution=resolution).get_selected_particles()
                        print('\t\t\t{0} particles at resolution {1}'.format(len(ps), resolution))

    def make_comp_cfgs_for_all_subunits(self):
        out = []
        for sname in [s.name for s in self.config.subunits]:
            out.append({
                    'name': sname,
                    'subunit': sname
                    }
                )

        return out

    def get_resi_ids_from_comp_cfg(self, mol, comp_cfg, struct_only=False):
        if 'filename' in comp_cfg:
            chain_id = comp_cfg.get('chain_id')
            if chain_id is None:
                subunit = self.config.get_subunit_by_name(comp_cfg['subunit'])
                if subunit is not None:
                    chain_id = subunit.chain_ids[0]

            rhs = system_tools.get_structure(self.model,
                                                        comp_cfg['filename'],
                                                        chain_id=chain_id,
                                                        ca_only=True)
            resi_ids = [rh.get_index() for rh in rhs]
            # print "in filename", comp_cfg['filename'], len(resi_ids), chain_id
        elif 'domain' in comp_cfg:
            # domain = self.config.get_domain_by_name(comp_cfg['subunit'], comp_cfg['domain'])
            # resi_ids = []
            # for domrange in domain['ranges']:
            #     try:
            #         resi_ids.extend(range(domrange[0], domrange[1]+1)) #when domrange is start, end tuple
            #     except TypeError:
            #         resi_ids.append(domrange) #when domrange is int
            resi_ids = sorted(self.get_domain_resi_indexes_struct(mol, comp_cfg['domain']))

        else:
            if struct_only:
                residues = mol.get_atomic_residues()
            else:
                residues = mol.get_residues()

            resi_ids = sorted([r.get_index() for r in residues])
            # resi_ids = sorted([r.get_hierarchy().get_index() for r in residues])

        if 'resi_ranges' in comp_cfg:
            resi_ids_from_ranges = []
            for r_range in comp_cfg['resi_ranges']:
                try:
                    resi_ids_from_ranges.extend(list(range(r_range[0], r_range[1]+1)))  #when range is (start, end) tuple
                except TypeError:
                    resi_ids_from_ranges.append(r_range) #when range is int

            if not resi_ids:
                resi_ids = resi_ids_from_ranges
            else:
                resi_ids = [r for r in resi_ids if r in resi_ids_from_ranges]

        return resi_ids

    def get_hs_from_comp_cfg(self, comp_cfg, resolution=None, first_copy_only=False, struct_only=False):
        hierarchies = []
        subunit = self.config.get_subunit_by_name(comp_cfg['subunit'])
        if 'states' in comp_cfg:
            states = comp_cfg['states']
        else:
            if 'state' in comp_cfg:
                states = [comp_cfg['state']]
            else:
                states = list(range(self.config.number_of_states))

        for state_idx in states:
            mols = self.get_mols_by_comp_cfg(comp_cfg, state_idx=state_idx, first_copy_only=first_copy_only)
            for mol in mols:
                resi_ids = self.get_resi_ids_from_comp_cfg(mol, comp_cfg, struct_only=struct_only)
                if resolution is not None:
                    sel = IMP.atom.Selection(
                                             # self.pmi_system.states[-1].get_hierarchy(),
                                             mol.get_hierarchy(),
                                             # molecule=comp_cfg['subunit'],
                                             # state_index=state_idx,
                                             residue_indexes=list(resi_ids),
                                             resolution=resolution)

                    # hierarchies.extend([IMP.atom.Hierarchy(p) for p in sel.get_selected_particles(with_representation=True)])
                    ps = sel.get_selected_particles(with_representation=True)
                else:
                    ps = IMP.pmi.tools.select_at_all_resolutions(mol.get_hierarchy(),
                                                                residue_indexes=list(resi_ids)
                                                                )

                hierarchies.extend([IMP.atom.Hierarchy(p) for p in ps])
                # densities = IMP.atom.Selection(mol.get_hierarchy(),representation_type=IMP.atom.DENSITIES).get_selected_particles()
                # hierarchies.extend([IMP.atom.Hierarchy(p) for p in densities])
        return hierarchies

    def get_mols_by_comp_cfg(self, comp_cfg, state_idx=0, first_copy_only=False):
        subunit = self.config.get_subunit_by_name(comp_cfg['subunit'])
        mols = []

        serie_name = comp_cfg.get('serie')
        if serie_name is not None:
            serie = self.config.get_serie_by_subunit_and_name(subunit, serie_name)
            if serie:
                if not comp_cfg.get('copies'):
                    mols = serie.molecules[state_idx]
                else:
                    mols = []
                    for idx in comp_cfg.get('copies'):
                        if len(serie.molecules[state_idx]) > idx:
                            mols.append(serie.molecules[state_idx][idx])

                    # mols = [serie.molecules[state_idx][idx] for idx in comp_cfg.get('copies') if len(serie.molecules[state_idx]) > idx]

                if first_copy_only:
                    mols = [mols[0]]

        else:
            for serie in subunit.series:
                if serie.states is not None and state_idx not in serie.states:
                    continue
                if not comp_cfg.get('copies'):
                    if first_copy_only:
                        mols.append(serie.molecules[state_idx][0])
                    else:
                        mols.extend(serie.molecules[state_idx])
                    # copy_ids.append(serie.molecules_copy_ids[state_idx][0])
                else:
                    for idx in comp_cfg.get('copies'):
                        if len(serie.molecules[state_idx]) > idx:
                            mols.append(serie.molecules[state_idx][idx])
                            if first_copy_only:
                                break
                    # if first_copy_only:
                    #     mols.append([serie.molecules[state_idx][idx] for idx in comp_cfg.get('copies') if len(serie.molecules[state_idx]) > idx][0])

                    # else:
                        # mols.extend([serie.molecules[state_idx][idx] for idx in comp_cfg.get('copies') if len(serie.molecules[state_idx]) > idx])                    
        return mols

    def get_component_ps(molname, domain=None, state=0, copy_i=0, serie=0, resolution=1):
        # for state_idx, state in enumerate(self.pmi_system.states):

        for serie in series:
            if serie.subunit == molname:
                mol = series[serie].molecules[state][copy_i]

                if domain:
                    resi_ids = self.get_domain_resi_indexes_struct(mol, domain)
                else:
                    resi_ids = []

                sel = IMP.atom.Selection(
                                         # self.pmi_system.states[-1].get_hierarchy(),
                                         mol.get_hierarchy(),
                                         # molecule=comp_cfg['subunit'],
                                         # state_index=state_idx,
                                         residue_indexes=list(resi_ids),
                                         resolution=resolution)
                return sel.get_selected_particles(with_representation=True)

    def get_particles_for_resi(self, molecule_name, pos, first_copy_only=False, resolution=0, state_index=0):
        if first_copy_only:
            pass
            # hierarchies = [s.molecules[0].get_hierarchy() for s in self.config.series] #WRONG: s.molecules[0] is state
            # sel = IMP.atom.Selection(hierarchies, state_index=state_index, molecule=molecule_name, residue_index=pos, resolution=resolution)
        else:
            sel = IMP.atom.Selection(self.universe, state_index=state_index, molecule=molecule_name, residue_index=pos, resolution=resolution)
        
        sel_ps = sel.get_selected_particles(with_representation=False)
        ps = []
        for sel_p in sel_ps:
            if IMP.atom.Residue.get_is_setup(sel_p):
                atom = get_atom_of_resi(sel_p, atom_type=IMP.atom.AT_CA)
                if atom is not None:
                    ps.append(atom)
                else:
                    ps.append(sel_p)
            else:
                ps.append(sel_p)

        return ps

    def get_domain_resi_indexes_struct(self, mol, domain):
        domain = self.config.get_domain_by_name(self.config.get_subunit_by_name(mol.get_name()), domain)
        resi_ids = []
        for domrange in domain['ranges']:
            try:
                resi_ids.extend(list(range(domrange[0], domrange[1]+1))) #when domrange is start, end tuple
            except TypeError:
                resi_ids.append(domrange) #when domrange is int

        return set(resi_ids) - set([r.get_index() for r in mol.get_non_atomic_residues()])

    def _add_rb(self, rb_cfg, rb_max_rot=0.2, rb_max_trans=1, nonrigid_max_trans=3.0):
        # if 'states' in comp_cfg:
        #     states = comp_cfg['states']
        # else:
        #     if 'state' in comp_cfg:
        #         states = [comp_cfg['state']]
        #     else:
        #         states = list(range(self.config.number_of_states))
        state_idx =  rb_cfg['components'][0]['states'][0] #temp solution, to fix
        hierarchies = []
        for comp_cfg in rb_cfg['components']:
            logging.debug('comp_cfg ' + str(comp_cfg))
            hierarchies.extend(self.get_hs_from_comp_cfg(comp_cfg,
                                                        struct_only=True))
        logging.debug('{0} hierarchies for rigid_body'.format(len(hierarchies)))
        # for h in hierarchies:
        #     if IMP.core.RigidMember.get_is_setup(h):
        #         print(h)
        if len(hierarchies) > 0:
            # for h in list(set(hierarchies)):
            #     print h, h.get_parent()
            logging.debug("creating rigid body " + str([(k, v) if k != 'positions' else (k, len(v)) for k, v in rb_cfg.items() ]))

            # print rb_cfg['name'], IMP.core.get_enclosing_sphere(hierarchies).get_radius()
            try:
                rb_movers, rb = self.dof.create_rigid_body(hierarchies,
                    name=rb_cfg['name'],
                    # name=comp_cfg['domain'],
                    max_rot=rb_max_rot,
                    max_trans=rb_max_trans,
                    nonrigid_max_trans=nonrigid_max_trans)
            except IMP._IMP_kernel.UsageException as e:
                    if str(e).startswith("Usage check failure: Can't add attribute that is there"):
                        print(e)
                        print('Rigid body definition for {0} overlaps with the following rigid bodies and their elements:'.format(rb_cfg['name']))
                        for old_rb in self.rbs_temp[state_idx]:
                            old_hs = [IMP.atom.Hierarchy(self.model.get_particle(p_i)) for p_i in old_rb.get_member_indexes()]
                            for h in hierarchies:
                                if h in old_hs:
                                    print(old_rb.get_name(), 'Hierarchy:', h)
                        raise(Exception('dupa'))

            if 'freeze' not in rb_cfg:
                self.movers_map.add(rb, rb_movers[0], 'RigidBodyMover', state_idx)

            self.rbs_temp[state_idx].append(rb)
            self.rbs_ini_tr3ds[rb] = rb.get_reference_frame().get_transformation_to()
            rb_cfg['rb'] = rb

            mols = []
            for h in hierarchies:
                mol = get_parent_mol(h)
                if mol not in mols:
                    mols.append(mol)
            print('Molecules in rigid body {0}: {1}'.format(rb_cfg['name'], mols))

            # rb = IMP.atom.create_rigid_body(list(set(hierarchies)))
            # rb.set_name('_'.join(c['name'] for c in rb_cfg['components']))
            # try:
            #     optimized = rb_cfg['optimized']
            # except KeyError:
            #     optimized = True
            # rb.set_coordinates_are_optimized(optimized)
            # state_rbs.append(rb)

        else:
            raise ValueError("Rigid body definition does not match anything in the system: " + str(rb_cfg))

    def is_rb_frozen(self, rb):
        for state_idx, mover_infos in self.movers_map.map.items():
            for mover_info in mover_infos:
                if rb == mover_info['object']:
                    return False

        return True

    def add_rigid_bodies_and_movers(self, rb_max_rot=0.2, rb_max_trans=1, beads_max_trans=1.0):
        print("Adding rigid bodies and beads")
        if hasattr(self.pmi_system, 'mdl'): #older PMI
            self.dof = IMP.pmi.dof.DegreesOfFreedom(self.pmi_system.mdl)
        else: #newer PMI
            self.dof = IMP.pmi.dof.DegreesOfFreedom(self.pmi_system.model)

        if self.config.rigid_bodies:
            for rb_cfg in self.config.rigid_bodies:
                logging.debug("rb_cfg" + str([(k, v) if k != 'positions' else (k, len(v)) for k, v in rb_cfg.items() ]))
                self._add_rb(rb_cfg, rb_max_rot=rb_max_rot, rb_max_trans=rb_max_trans)

            for state_idx, state in enumerate(self.pmi_system.states):
                for molname, copies in state.molecules.items():
                    for mol in copies:
                        #Get all beads, including mol.get_non_atomic_residues()
                        #and those not added to rbs
                        rbs, beads = IMP.pmi.tools.get_rbs_and_beads(mol.get_hierarchy())
                        logging.info('{2} rbs {0} beads {1} '.format(len(rbs), len(beads), mol))
                        if beads:
                            fb_movers = self.dof.create_flexible_beads(beads,max_trans=beads_max_trans)
                            for bead, mover in zip(self.dof.flexible_beads[-len(fb_movers):], fb_movers):
                                self.movers_map.add(bead, mover, 'BallMover', state_idx)

        else:
            raise Exception('Error: no rigid bodies defined') #TODO: handle subunits with no rigid bodies
            for state in self.pmi_system.states:
                for molname, copies in state.molecules.items():
                    for mol in copies:
                        print("create rigid body")
                        self.dof.create_rigid_body(mol,
                            nonrigid_parts = mol.get_non_atomic_residues(),
                            name=mol.get_name(),
                            nonrigid_max_trans=3.0)
                        print("after create rigid body")
                        # rb = IMP.atom.create_rigid_body(mol.get_hierarchy())
                        # non_atomic_res = mol.get_non_atomic_residues()

                        # residue_indexes = [r.get_index() for r in non_atomic_res]
                        # sel = IMP.atom.Selection(mol.get_hierarchy(), residue_indexes=residue_indexes)
                        # rigid_body.create_non_rigid_members(sel)
                        # state_rbs.append(rb)

                        # self.temp_mol_rbs.append([mol, rb])
        print("Finished adding rigid_bodies")


    def make_discrete_movers(self, restr_weight=1000):
        print('Adding discrete movers')
        d_movers = []
        added_rbs = []

        if self.config.ini_fits: #old_style ini_fits, deprecated, kept for backward compatibility
            for subunit, tr3ds_list in self.config.ini_fits.items():
                for tr3ds in tr3ds_list:
                    for t in tr3ds:
                        t['tr3d'] = IMP.algebra.Transformation3D(IMP.algebra.get_rotation_from_matrix(*t['rotation']),
                                                                IMP.algebra.Vector3D(*t['translation']))
            for serie in self.config.series:
                for state_idx, state_mols in enumerate(serie.molecules):
                    for mol in state_mols:
                        # print '>', mol.get_name()
                        ref_rbs, ref_beads = IMP.pmi.tools.get_rbs_and_beads([mol.get_hierarchy()])
                        # print mol.get_name(), [x.get_particle().get_name() for x in ref_rbs]
                        for rb_i, rb, tr3dsCfgs in zip(list(range(len(ref_rbs))), ref_rbs, self.config.ini_fits[mol.get_name()]):
                            mover = None
                            if not tr3dsCfgs or rb in added_rbs:
                                continue
                            tr3ds = [t['tr3d'] for t in tr3dsCfgs]

                            conn_restr = None
                            for pidx1, pidx2, conn_restr, target_distance in self.conn_restraints_ps_temp:
                                # print dir(conn_restr)
                                # print conn_restr.get_inputs()
                                # ps1, ps2 = list(conn_restr.get_inputs())
                                # print dir(ps2)
                                # print ps2.get_inputs()
                                # print dir(rb.get_particle())
                                ps2 = self.model.get_particle(pidx2)

                                if IMP.core.RigidMember.get_is_setup(ps2) and IMP.core.RigidMember(ps2).get_rigid_body() == rb: # and get_parent_mol(IMP.atom.Hierarchy(ps2)) == mol.get_hierarchy():
                                    break
                                conn_restr = None

                            restr_for_weighted_mover = []
                            #TODO: add code for adding restraints for DiscreteWeightedMovers

                            if len(restr_for_weighted_mover) > 0:
                                weight_score_fn = IMP.core.RestraintsScoringFunction(restr_for_weighted_mover, "scoring function")

                                print('DiscreteWeightedMover:', mol.get_name(), 'tr3ds:', len(tr3ds))
                                mover = core.DiscreteWeightedMover(self.model, rb.get_particle_index(), tr3ds, weight_score_fn, precalc=True)
                                self.movers_map.add(rb, mover, 'DiscreteWeightedMover', state_idx)
                                d_movers.append(mover)

                                restr = core.DiscreteMoverRestraint(self.model, [rb.get_particle()], mover, [t['score'] for t in tr3dsCfgs])
                                restr.set_weight(restr_weight)
                                self.discrete_restraints.append(restr)

                            if conn_restr is not None and rb_i != 0:
                                print('DiscreteConnMover:', mol.get_name(), 'tr3ds:', len(tr3ds))
                                target_distance = 20
                                mover = core.DiscreteConnMover(self.model, rb.get_particle_index(), tr3ds, conn_restr, target_distance, tr3ds_scores=[t['score'] for t in tr3dsCfgs], scaleBetter=False)
                                self.movers_map.add(rb, mover, 'DiscreteConnMover')
                                d_movers.append(mover)

                                restr = core.DiscreteMoverRestraint(self.model, [rb.get_particle()], mover, [t['score'] for t in tr3dsCfgs])
                                restr.set_weight(restr_weight)
                                self.discrete_restraints.append(restr)


                                print('DiscreteSoftConnMover:', mol.get_name(), 'tr3ds:', len(tr3ds))
                                how_many = 20
                                mover = core.DiscreteSoftConnMover(self.model, rb.get_particle_index(), tr3ds, conn_restr, how_many)
                                self.movers_map.add(rb, mover, 'DiscreteSoftConnMover', state_idx)
                                d_movers.append(mover)

                                restr = core.DiscreteMoverRestraint(self.model, [rb.get_particle()], mover, [t['score'] for t in tr3dsCfgs])
                                restr.set_weight(restr_weight)
                                self.discrete_restraints.append(restr)

                                print('DiscreteMover:', mol.get_name(), 'tr3ds:', len(tr3ds))
                                mover = core.DiscreteMover(self.model, rb.get_particle_index(), tr3ds, tr3ds_scores=[t['score'] for t in tr3dsCfgs], scaleBetter=False)
                                self.movers_map.add(rb, mover, 'DiscreteMover', state_idx)
                                d_movers.append(mover)

                                restr = core.DiscreteMoverRestraint(self.model, [rb.get_particle()], mover, [t['score'] for t in tr3dsCfgs])
                                restr.set_weight(restr_weight)
                                self.discrete_restraints.append(restr)


                            else:
                                mover = core.DiscreteMover(self.model, rb.get_particle_index(), tr3ds, tr3ds_scores=[t['score'] for t in tr3dsCfgs], scaleBetter=False)
                                print('DiscreteMover:', mol.get_name(), 'tr3ds:', len(tr3ds))
                                self.movers_map.add(rb, mover, 'DiscreteMover', state_idx)

                                d_movers.append(mover)
                                restr = core.DiscreteMoverRestraint(self.model, [rb.get_particle()], mover, [t['score'] for t in tr3dsCfgs])
                                restr.set_weight(restr_weight)
                                self.discrete_restraints.append(restr)

                            if mover is not None:
                                added_rbs.append(rb)
        else:
            for rb_cfg in self.config.rigid_bodies:
                if 'freeze' in rb_cfg:
                    continue
                # print([(k, v) if k != 'positions' else (k, len(v)) for k, v in rb_cfg.items() ])
                state_idx = rb_cfg['components'][0]['states'][0]
                rb = rb_cfg['rb']
                if 'positions' in rb_cfg:
                    tr3dsCfgs = rb_cfg['positions']

                    mover = None
                    if not tr3dsCfgs or rb in added_rbs:
                        continue
                    tr3ds = [t['tr3d'] for t in tr3dsCfgs]


                    #TODO: revise connectivity restraints mechanism
                    #TODO: add ConnMovers only when requested
                    conn_restr = None
                    for pidx1, pidx2, conn_restr, target_distance in self.conn_restraints_ps_temp:
                        # print dir(conn_restr)
                        # print conn_restr.get_inputs()
                        # ps1, ps2 = list(conn_restr.get_inputs())
                        # print dir(ps2)
                        # print ps2.get_inputs()
                        # print dir(rb.get_particle())
                        ps2 = self.model.get_particle(pidx2)

                        if IMP.core.RigidMember.get_is_setup(ps2) and IMP.core.RigidMember(ps2).get_rigid_body() == rb: # and get_parent_mol(IMP.atom.Hierarchy(ps2)) == mol.get_hierarchy():
                            break
                        conn_restr = None

                    restr_for_weighted_mover = []
                    restr_for_weighted_mover_names = rb_cfg.get('restraints_for_weighted_mover')
                    if restr_for_weighted_mover_names:
                        for name in restr_for_weighted_mover_names:
                            restr_for_weighted_mover.extend(self.all_restraints[name])

                    if len(restr_for_weighted_mover) > 0:
                        weight_score_fn = IMP.core.RestraintsScoringFunction(restr_for_weighted_mover, "scoring function")

                        mover = core.DiscreteWeightedMover(self.model, rb.get_particle_index(), tr3ds, weight_score_fn, precalc=True, top=rb_cfg.get('weighted_mover_top'))
                        logging.debug(' '.join(map(str, ['DiscreteWeightedMover:', mover.get_name(), 'tr3ds:', len(tr3ds)])))
                        self.movers_map.add(rb, mover, 'DiscreteWeightedMover', state_idx)
                        d_movers.append(mover)

                        restr = core.DiscreteMoverRestraint(self.model, [rb.get_particle()], mover, [t['score'] for t in tr3dsCfgs])
                        restr.set_weight(restr_weight)
                        self.discrete_restraints.append(restr)

                    if conn_restr is not None: #TODO: add checking if the conn_restr is not connecting to the first N-terminal rb
                        target_distance = 20
                        mover = core.DiscreteConnMover(self.model, rb.get_particle_index(), tr3ds, conn_restr, target_distance, tr3ds_scores=[t['score'] for t in tr3dsCfgs], scaleBetter=False)
                        logging.debug(' '.join(map(str, ['DiscreteConnMover:', mover.get_name(), 'tr3ds:', len(tr3ds)])))
                        self.movers_map.add(rb, mover, 'DiscreteConnMover')
                        d_movers.append(mover)

                        restr = core.DiscreteMoverRestraint(self.model, [rb.get_particle()], mover, [t['score'] for t in tr3dsCfgs])
                        restr.set_weight(restr_weight)
                        self.discrete_restraints.append(restr)

                        how_many = 20
                        mover = core.DiscreteSoftConnMover(self.model, rb.get_particle_index(), tr3ds, conn_restr, how_many)
                        logging.debug(' '.join(map(str, ['DiscreteSoftConnMover:', mover.get_name(), 'tr3ds:', len(tr3ds)])))
                        self.movers_map.add(rb, mover, 'DiscreteSoftConnMover', state_idx)
                        d_movers.append(mover)

                        restr = core.DiscreteMoverRestraint(self.model, [rb.get_particle()], mover, [t['score'] for t in tr3dsCfgs])
                        restr.set_weight(restr_weight)
                        self.discrete_restraints.append(restr)

                        mover = core.DiscreteMover(self.model, rb.get_particle_index(), tr3ds, tr3ds_scores=[t['score'] for t in tr3dsCfgs], scaleBetter=False)
                        logging.debug(' '.join(map(str, ['DiscreteMover:', mover.get_name(), 'tr3ds:', len(tr3ds)])))
                        self.movers_map.add(rb, mover, 'DiscreteMover', state_idx)
                        d_movers.append(mover)

                        restr = core.DiscreteMoverRestraint(self.model, [rb.get_particle()], mover, [t['score'] for t in tr3dsCfgs])
                        restr.set_weight(restr_weight)
                        self.discrete_restraints.append(restr)


                    else:

                        mover = core.DiscreteMover(self.model, rb.get_particle_index(), tr3ds, tr3ds_scores=[t['score'] for t in tr3dsCfgs], scaleBetter=False)
                        logging.debug(' '.join(map(str, ['DiscreteMover:', rb_cfg['name'], 'tr3ds:', len(tr3ds)])))
                        state_idx = get_parent_state(IMP.atom.Hierarchy(self.model.get_particle(rb.get_member_indexes()[0]))).get_state_index()
                        self.movers_map.add(rb, mover, 'DiscreteMover', state_idx)

                        d_movers.append(mover)
                        restr = core.DiscreteMoverRestraint(self.model, [rb.get_particle()], mover, [t['score'] for t in tr3dsCfgs])
                        restr.set_weight(restr_weight)
                        self.discrete_restraints.append(restr)

                    if mover is not None:
                        added_rbs.append(rb)

        self.dof.movers.extend(d_movers)
        print('Added {0} discrete movers'.format(len(d_movers)))

        return d_movers
    
    def get_tr3d_by_name(self, tr3d_name):
        out_sym_tr3d = None
        for sym_tr3d in self.sym_tr3ds:
            if sym_tr3d['name'] == tr3d_name:
                out_sym_tr3d = sym_tr3d
        if out_sym_tr3d is None:
            raise Exception('No transformation named {0} found'.format(tr3d_name))

        return out_sym_tr3d

    def read_sym_tr3ds(self):
        """Read transformation matrices based on definitions in the self.config.

        Store transformations in self.sym_tr3ds list.

        """
        self.sym_tr3ds = []
        if self.config.symmetry_cfg:
            sym_tr3ds = self.config.symmetry_cfg.get('sym_tr3ds')
            if sym_tr3ds:
                for sym_tr3d in sym_tr3ds:
                    if 'rot' in sym_tr3d and 'trans' in sym_tr3d:
                        rot = IMP.algebra.get_rotation_from_matrix(*sym_tr3d['rot'])
                        trans = IMP.algebra.Vector3D(*sym_tr3d['trans'])

                        self.sym_tr3ds.append({
                            'name': sym_tr3d['name'],
                            'tr3d': IMP.algebra.Transformation3D(rot, trans)}
                        )
                    elif 'axis' in sym_tr3d and 'type' in sym_tr3d:
                        sym_type = sym_tr3d['type']
                        angle = 2 * math.pi / int(sym_tr3d['type'][1:])

                        self.sym_tr3ds.append({
                            'name': sym_tr3d['name'],
                            'tr3d': IMP.algebra.get_rotation_about_point(sym_tr3d['center'], IMP.algebra.get_rotation_about_axis(sym_tr3d['axis'], angle))}
                        )


    def get_rbs_and_beads_for_symmetry(self, ref, clone, mode='constraints'):
        """Define refs_to_use and clones_to_use instead of passing entire molecules to dof.constrain_symmetry
        because parts of molecules can be already in a symmetrical subcomplex as added from PDB
        """
        href    = IMP.pmi.tools.input_adaptor(ref)
        hclones = IMP.pmi.tools.input_adaptor(clone)
        ref_rbs,ref_beads = IMP.pmi.tools.get_rbs_and_beads(href)
        clones_rbs,clones_beads = IMP.pmi.tools.get_rbs_and_beads(hclones)
        # print(ref_mol, mol)
        # print([rb.get_name() for rb in ref_rbs], [rb.get_name() for rb in clones_rbs])
        # dumb check for matching numbers of particles
        if len(ref_rbs)!=len(clones_rbs) or len(ref_beads)!=len(clones_beads):
            rbs_str = ','.join([rb.get_name() for rb in ref_rbs]) + ' ' + ','.join([rb.get_name() for rb in clones_rbs])
            beads_str = ','.join([rb.get_name() for rb in ref_beads]) + ' ' + ','.join([rb.get_name() for rb in clones_beads])
            raise Exception("ERROR: Your references don't match your clones\nRbs: {0}\nBeads: {1}".format(rbs_str, beads_str))

        refs_to_use = []
        clones_to_use = []
        # symmetry RBs
        for ref,clone in zip(ref_rbs,clones_rbs):
            # if not IMP.core.Reference.get_is_setup(clone):
            refh = [IMP.atom.Hierarchy(self.model.get_particle(pi)) for pi in ref.get_member_indexes()][0]
            cloneh = [IMP.atom.Hierarchy(self.model.get_particle(pi)) for pi in clone.get_member_indexes()][0]

            if mode == 'constraints' \
                    and not (IMP.core.RigidMember.get_is_setup(refh) \
                    and IMP.core.RigidMember.get_is_setup(cloneh) \
                    and IMP.core.RigidMember(refh).get_rigid_body() ==  IMP.core.RigidMember(cloneh).get_rigid_body()) \
                or mode == 'restraints':
                    refs_to_use.append(ref)
                    clones_to_use.append(clone)

        for ref,clone in zip(ref_beads,clones_beads):
            # if not IMP.core.Reference.get_is_setup(clone):
            refs_to_use.append(ref)
            clones_to_use.append(clone)

        return refs_to_use, clones_to_use

    def add_symmetry_restraints(self, weight=1, use_constraints=True, use_restraints=False):
        """Add symmetry restraints and/or constraints based on specifications in the config.

        The entire self.config.symmetry_cfg['already_symmed'] code is to let users
        to combine automated addition of symmetry and symmetry_cfg['apply_symmetry'].
        """
        if use_constraints and use_restraints:
            raise UsageException('use_constraints and use_restraints cannot be used in the same time. Run add_symmetry_restraints twice instead.')
        self.config.add_auto_symmetries(use_constraints)
        # print(use_constraints, pprint.pformat(self.config.symmetry_cfg, indent=4))
        logging.debug('Adding symmetry restraints for ' + pprint.pformat(self.config.symmetry_cfg, indent=4))

        if not self.sym_constraint_pairs:
            self.sym_constraint_pairs = []
        if not self.sym_restraint_pairs:
            self.sym_restraint_pairs = []
        restraints = []
        if not 'already_symmed' in self.config.symmetry_cfg:
            self.config.symmetry_cfg['already_symmed'] = []
        for apply_sym_cfg in self.config.symmetry_cfg['apply_symmetry']:
            logging.debug('Adding symmetry restraints for ' + pprint.pformat(apply_sym_cfg, indent=4))
            sym_tr3d = IMP.algebra.get_identity_transformation_3d()
            components = []
            for selector in apply_sym_cfg['selectors']:
                hs = self.get_hs_from_comp_cfg(selector, resolution=max(self.STRUCT_RESOLUTIONS), first_copy_only=False, struct_only=False)
                components.append(hs)
                if len(hs) == 0:
                    logging.warning('No particles selected for ' + pprint.pformat(selector, indent=4))
            ref_component = components[0]
            rb_grouping_key = lambda h: IMP.core.RigidMember(h).get_rigid_body() if IMP.core.RigidMember.get_is_setup(h) else h
            ref_groups = [list(g) for k, g in groupby_unsorted(ref_component, key=rb_grouping_key)]

            for i, component in enumerate(components[1:], 1):
                if apply_sym_cfg['restraint_type'] == 'symmetry_restraint' and use_restraints:
                    ref_name = self.config.get_nice_name_from_comp_cfg(apply_sym_cfg['selectors'][0])
                    name = self.config.get_nice_name_from_comp_cfg(apply_sym_cfg['selectors'][i])
                    rs = IMP.RestraintSet(self.model, f'Symmetry restraint group {ref_name} {name}')
                    sym_tr3d = sym_tr3d * self.get_tr3d_by_name(apply_sym_cfg['sym'])['tr3d']
                    tds = IMP.core.TransformedDistancePairScore(IMP.core.Harmonic(0,1), sym_tr3d)
                    #Group by rigid body
                    groups = [list(g) for k, g in groupby_unsorted(component, key=rb_grouping_key)]
                    for ref, clone in zip(ref_groups, groups):
                        #If a group has more than one particle
                        #it means it is in a rigid body
                        #and we can select a subset of particles just for speed
                        max_number_of_particles = 5
                        if len(ref) > max_number_of_particles:
                            every_nth = int(len(ref)/max_number_of_particles)
                            logging.debug(f'Adding symmetry restraints for every {every_nth}nth particle of the selection (for speed)')
                            selected_ref_hs = ref[::every_nth][:max_number_of_particles]
                            selected_hs = clone[::every_nth][:max_number_of_particles]
                        else:
                            selected_ref_hs = ref
                            selected_hs = clone

                        for ref_h, h in zip(selected_ref_hs, selected_hs):
                            if (ref_h.get_particle(), h.get_particle()) not in self.config.symmetry_cfg['already_symmed'] and (h.get_particle(), ref_h.get_particle()) not in self.config.symmetry_cfg['already_symmed']:
                                if IMP.core.RigidMember.get_is_setup(ref_h) and IMP.core.RigidMember.get_is_setup(h):
                                    ref_rb = IMP.core.RigidMember(ref_h).get_rigid_body()
                                    rb = IMP.core.RigidMember(h).get_rigid_body()
                                    if (ref_rb, rb) in self.config.symmetry_cfg['already_symmed'] or (rb, ref_rb) in self.config.symmetry_cfg['already_symmed']:
                                        continue
                                self.config.symmetry_cfg['already_symmed'].append((ref_h.get_particle(), h.get_particle()))
                                logging.debug(f"Adding symmetry restraint between: {ref_h.get_name()} {h.get_name()}")
                                pr = IMP.core.PairRestraint(self.model, tds, (ref_h, h))

                                pr.set_name('Symmetry restraint: {obj1} - {obj2}'.format(obj1=ref_h.get_name(), obj2=h.get_name()))
                                rs.add_restraint(pr)
                                rs.set_weight(weight)
                                self.sym_restraint_pairs.append((ref_h, h))

                    if rs.get_has_restraints():
                        restraints.append(rs)

                elif apply_sym_cfg['restraint_type'] == 'symmetry_constraint' and use_constraints:
                    sym_tr3d = sym_tr3d * self.get_tr3d_by_name(apply_sym_cfg['sym'])['tr3d']
                    tds = IMP.core.TransformedDistancePairScore(IMP.core.Harmonic(0,1), sym_tr3d)
                    refs, clones = self.get_rbs_and_beads_for_symmetry(ref_component, component, mode='constraints')
                    refs_to_use = []
                    clones_to_use = []
                    for ref,clone in zip(refs,clones):
                        if not IMP.core.Reference.get_is_setup(clone) and (ref, clone) not in self.config.symmetry_cfg['already_symmed'] and (clone, ref) not in self.config.symmetry_cfg['already_symmed']:
                            if IMP.core.RigidBody.get_is_setup(ref) and IMP.core.RigidBody.get_is_setup(clone):
                                
                                if any((ref_member, member) in self.config.symmetry_cfg['already_symmed'] for ref_member, member in zip(IMP.core.RigidBody(ref).get_rigid_members(), IMP.core.RigidBody(clone).get_rigid_members())) or \
                                    any((member, ref_member) in self.config.symmetry_cfg['already_symmed'] for ref_member, member in zip(IMP.core.RigidBody(ref).get_rigid_members(), IMP.core.RigidBody(clone).get_rigid_members())):
                                    continue

                                refs_to_use.extend([self.model.get_particle(pi) for pi in ref.get_member_indexes()])
                                clones_to_use.extend([self.model.get_particle(pi) for pi in clone.get_member_indexes()])
                            else:
                                refs_to_use.append(ref)
                                clones_to_use.append(clone)
                            self.config.symmetry_cfg['already_symmed'].append((ref, clone))
                            self.sym_constraint_pairs.append((ref, clone))
                    
                    if refs_to_use and clones_to_use:
                        self.dof.constrain_symmetry(refs_to_use, clones_to_use, sym_tr3d.get_inverse())

                        # print("Adding symmetry constraint between:", ref.get_name(), clone.get_name())
                        # if IMP.core.RigidBody.get_is_setup(ref) and IMP.core.RigidBody.get_is_setup(clone) and not IMP.core.Reference.get_is_setup(clone):
                        #     ref_ps = [self.model.get_particle(pi) for pi in ref.get_member_indexes()]
                        #     clone_ps = [self.model.get_particle(pi) for pi in clone.get_member_indexes()]
                        #     self.dof.constrain_symmetry(ref_ps, clone_ps, sym_tr3d.get_inverse())
                        # elif not IMP.core.Reference.get_is_setup(clone):
                        #     self.dof.constrain_symmetry(ref, clone, sym_tr3d.get_inverse())
                    
        if use_restraints:
            for p1, p2 in self.sym_restraint_pairs:
                print('Symmetry restraint pair:', p1.get_name(), p2.get_name())
        if use_constraints:
            for p1, p2 in self.sym_constraint_pairs:
                print('Symmetry constrained pair:', p1.get_name(), p2.get_name())

            self.model.update() #apply constraints
            
        return restraints

    def is_sym_constrained(self, obj):
        '''
            o rb or bead from IMP.pmi.tools.get_rbs_and_beads
        '''
        for each_ref, each_clone in self.sym_constraint_pairs:

            if IMP.core.RigidMember.get_is_setup(each_clone):
                each_clone = IMP.core.RigidMember(each_clone).get_rigid_body()

            if obj == each_clone:
                return True

        return False

    def write_as_pdb(self, filename):
        IMP.atom.write_pdb(self.universe, filename)

    def write_as_rmf(self, filename, restraints=None, geometries=None):
        rmf = RMF.create_rmf_file(filename)
        rmf.set_description("Universe.\n")
        IMP.rmf.add_hierarchy(rmf, self.universe)
        if restraints:
            IMP.rmf.add_restraints(rmf, restraints)
        if geometries:
            IMP.rmf.add_geometries(rmf, geometries)
        IMP.rmf.save_frame(rmf)

    def write_states_rmf(self, filenames, restraints=None):
        for state, filename in zip(self.pmi_system.states, filenames):
            rmf = RMF.create_rmf_file(filename)
            rmf.set_description("Universe.\n")
            IMP.rmf.add_hierarchy(rmf, state.get_hierarchy())
            if restraints:
                IMP.rmf.add_restraints(rmf, restraints)
            IMP.rmf.save_frame(rmf)

    def write_solution_temp(self, outfilenames):

        for state_rbs, outfilename in zip(self.rbs_temp, outfilenames):
            solution = defaultdict(list)
            for rb in state_rbs:
                tr3d = rb.get_reference_frame().get_transformation_to() / self.rbs_ini_tr3ds[rb]

                row1 = list(tr3d.get_rotation().get_rotation_matrix_row(0))
                row2 = list(tr3d.get_rotation().get_rotation_matrix_row(1))
                row3 = list(tr3d.get_rotation().get_rotation_matrix_row(2))
                trans = [x for x in tr3d.get_translation()]

                name = rb.get_name()

                solution[name].append({
                    'rot': [row1, row2, row3],
                    'trans': trans
                    })

            with open(outfilename, 'w') as f:
                f.write(json.dumps(solution, indent=None)+'\n')

    def show_hierarchy(self):
        IMP.atom.show_molecular_hierarchy(self.universe)

    def add_ev_restraints(self, weight=1, resolution=0, copies=None, distance_cutoff=0.0, slack=5.0):

        class ConsecutiveFilter(IMP.PairPredicate):

            def __init__(self):
                IMP.PairPredicate.__init__(self, "ConsecutiveFilter%1%")

            def get_value_index(self, m, pp):
                #TODO: add support for full atom
                if (IMP.atom.Residue.get_is_setup(m, pp[0]) or IMP.atom.Atom.get_is_setup(m, pp[0])) and \
                    (IMP.atom.Residue.get_is_setup(m, pp[1]) or IMP.atom.Atom.get_is_setup(m, pp[1])):
                    resi_idx1 = get_end_index(m, pp[0]) - 1
                    resi_idx2 = get_first_index(m, pp[1])
                    diff = resi_idx1 - resi_idx2
                    if diff == -1 or diff == 1:
                        return 1
                return 0

            def do_get_inputs(self, m, pis):
                return [m.get_particle(i) for i in pis]

            def do_show(self, out):
                pass

        if copies is None:
            copies = []

        ev_restraints = []
        for state_idx, state in enumerate(self.pmi_system.states):
            all_optimized = []
            for serie in self.config.series:
                for i, mol in enumerate(serie.molecules[state_idx]):
                    if copies and i not in copies:
                        continue
                    sel = IMP.atom.Selection(mol.get_hierarchy(),
                                                 # molecule=comp_cfg['subunit'],
                                                 # state_index=0,
                                                 residue_indexes=[r.get_index() for r in mol.get_non_atomic_residues()]+[r.get_index() for r in mol.get_atomic_residues()], #WIERD these all residues
                                                 resolution=resolution)

                    all_optimized.extend([p for p in sel.get_selected_particles(with_representation=True)])


            # all_optimized = [rb for self.rbs] + 

            lsc = IMP.container.ListSingletonContainer(self.model)

            lsc.add(IMP.get_indexes(all_optimized))
            rbcpf = IMP.core.RigidClosePairsFinder()
            cpc = IMP.container.ClosePairContainer(lsc, distance_cutoff, rbcpf, slack)
            f = ConsecutiveFilter()
            cpc.add_pair_filter(f)
            ssps = IMP.core.SoftSpherePairScore(1.0)
            ev_r = IMP.container.PairsRestraint(ssps, cpc)
            ev_r.set_name('ev_restraint_resol_'+str(resolution))
            ev_r.set_weight(weight)
            ev_restraints.append(ev_r)

        self.ev_restraints.extend(ev_restraints)

        return ev_restraints

    def add_ev_restraints_only_atomic_temp(self, weight=1, resolution=0, copies=None, distance_cutoff=0.0, slack=5.0):
        if copies is None:
            copies = []

        ev_restraints = []
        for state_idx, state in enumerate(self.pmi_system.states):
            all_optimized = []
            for serie in self.config.series:
                for i, mol in enumerate(serie.molecules[state_idx]):
                    if copies and i not in copies:
                        continue
                    sel = IMP.atom.Selection(mol.get_hierarchy(),
                                                 # molecule=comp_cfg['subunit'],
                                                 # state_index=0,
                                                 residue_indexes=[r.get_index() for r in mol.get_atomic_residues()], #WIERD these all residues
                                                 resolution=resolution)

                    all_optimized.extend([p for p in sel.get_selected_particles(with_representation=True)])


            # all_optimized = [rb for self.rbs] + 

            lsc = IMP.container.ListSingletonContainer(self.model)

            lsc.add(IMP.get_indexes(all_optimized))
            rbcpf = IMP.core.RigidClosePairsFinder()
            cpc = IMP.container.ClosePairContainer(lsc, distance_cutoff, rbcpf, slack)
            ssps = IMP.core.SoftSpherePairScore(1.0)
            ev_r = IMP.container.PairsRestraint(ssps, cpc)
            ev_r.set_name('ev_restraint_resol_only_atomic_'+str(resolution))
            ev_r.set_weight(weight)
            ev_restraints.append(ev_r)

        self.ev_restraints.extend(ev_restraints)

        return ev_restraints

    def add_connectivity_restraints(self, connectivity_restraint_weight=1., k=10.0, first_copy_only=False, max_conn_gap=None, conn_reweight_fn=None, ca_ca_connectivity_scale=0.526):
        """Add connectivity restraints between rigid bodies and beads

        @param connectivity_restraint_weight Weight of the restraint
        @param k Spring constant for the harmonic potential
        @param first_copy_only Whether to add the restraints only for the first copy of the molecule (useful for symmetrical assemblies)
        @param max_conn_gap Number of missing residues of the missing atomic region above which the restraint will not be added
        @param conn_reweight_fn A function that accepts the following parameters:
            mol - PMI molecule object
            next_resi_idx - residue of the next rigid body or bead
            prev_resi_idx - residue of the previous rigid body or bead
            connectivity_restraint_weight - default weight passed to add_connectivity_restraints
        @param ca_ca_connectivity_scale Scale the average CA-CA distance of 3.8 to account for that
             it is unlikely that the linker is fully stretched, default: 0.526 (count 2A per peptide bond)
        """
        print("Adding connectivity restraints")
        self.conn_restraints = []
        self.conn_restraints_ps_temp = []
        for serie in self.config.series:
            for state_idx, state_mols in enumerate(serie.molecules):
                for mol in state_mols:
                    print("Adding connectivity restraints for " + str(mol))
                    resi = sorted(list(mol.get_residues()), key=lambda r: r.get_index())
                    resi_ids = [r.get_index() for r in resi]

                    ps = IMP.atom.Selection(mol.get_hierarchy(), residue_indexes=resi_ids, resolution=self.STRUCT_RESOLUTIONS[0]).get_selected_particles(with_representation=False)
                    ps = sorted(ps, key= lambda p: get_first_index(self.model, p))

                    added = []

                    for first, second in zip(ps, ps[1:]):
                        leaf1 = IMP.atom.get_leaves(IMP.atom.Hierarchy(first))[0]
                        leaf2 = IMP.atom.get_leaves(IMP.atom.Hierarchy(second))[0]
                        if not (IMP.core.RigidMember.get_is_setup(leaf1) \
                            and IMP.core.RigidMember.get_is_setup(leaf2) \
                            and IMP.core.RigidMember(leaf1).get_rigid_body() ==  IMP.core.RigidMember(leaf2).get_rigid_body()):
                            atom1 = None
                            if IMP.atom.Residue.get_is_setup(first):
                                atom1 = get_atom_of_resi(first, atom_type=IMP.atom.AT_CA)
                                if atom1 is None:
                                    atom1 = first
                            else:
                                atom1 = first

                            atom2 = None
                            if IMP.atom.Residue.get_is_setup(second):
                                atom2 = get_atom_of_resi(second, atom_type=IMP.atom.AT_CA)
                                if atom2 is None:
                                    atom2 = second
                            else:
                                atom2 = second

                            if (atom1.get_index(), atom2.get_index()) not in added and (atom1.get_index() != atom2.get_index()):
                                added.append((atom1.get_index(), atom2.get_index()))

                                last_resi_idx = get_end_index(self.model, first)
                                if IMP.atom.Fragment.get_is_setup(first):
                                    last_resi_idx = last_resi_idx - 1
                                resi_idx = get_first_index(self.model, second)
                                gap_len = resi_idx - last_resi_idx

                                if max_conn_gap is None or gap_len < max_conn_gap:
                                    dist = gap_len * 3.8 * ca_ca_connectivity_scale
                                    dist = max([3.8, dist]) # 3.8A is more or less Calpha-Calpha distance https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3892923/
                                    if gap_len == 1:
                                        f = IMP.core.Harmonic(dist, k)
                                    else:
                                        f = IMP.core.HarmonicUpperBound(dist, k)
                                    s = IMP.core.DistancePairScore(f)

                                    if len(self.pmi_system.states) > 1:
                                        state_str = 'State_{0}.'.format(state_idx)
                                    else:
                                        state_str = ''
                                    restr_name = 'conn restraint {first}-{second} of {state_str}{mol}.{copy_idx}'.format(
                                            first=str(first).replace('"',''),
                                            second=str(second).replace('"',''),
                                            state_str=state_str,
                                            mol=mol.get_name(),
                                            copy_idx=IMP.atom.Copy(mol.get_hierarchy()).get_copy_index()
                                        )

                                    r = IMP.core.PairRestraint(self.model, s, (atom1.get_index(), atom2.get_index()), restr_name)
                                    self.conn_restraints_ps_temp.append([atom1.get_index(), atom2.get_index(), r, dist])

                                    # Keep it in case we want to change the implementation
                                    # r = IMP.atom.create_connectivity_restraint([IMP.atom.Selection(IMP.atom.Hierarchy(first_resi)),
                                    #                                            IMP.atom.Selection(IMP.atom.Hierarchy(second_resi))],
                                    #                                            dist,
                                    #                                            k, "conn "+mol.get_name()+'.'+str(i)+' '+first_resi.get_name()+'-'+second_resi.get_name())

                                    if conn_reweight_fn:
                                        final_connectivity_restraint_weight = conn_reweight_fn(mol, resi_idx, last_resi_idx, connectivity_restraint_weight)
                                    else:
                                        final_connectivity_restraint_weight = connectivity_restraint_weight
                                    r.set_weight(final_connectivity_restraint_weight)
                                    self.conn_restraints.append(r)

                    if first_copy_only:
                        break

    def add_xlink_restraints(self,
                             xlink_set,
                             min_ld_score,
                             weight=1,
                             k=1,
                             xlink_distance=30,
                             inter_xlink_scale=1,
                             skip_pair_fn=None,
                             xlink_score_type='HarmonicUpperBound',
                             first_copy_only=False,
                             score_weighting=False,
                             reweight_function=None,
                             log_filename=None,
                             x_random_sample_fraction=1.0):
        '''
        '''
        if log_filename:
            log_file = open(log_filename, 'w')
        else:
            log_file = None
        log_file and log_file.write('adding xlink restraints.\n')

        if xlink_score_type == 'HarmonicUpperBound':
            xlink_score = IMP.core.HarmonicUpperBound(xlink_distance, k)
            xlink_pair_score = IMP.core.DistancePairScore(xlink_score)
        elif xlink_score_type == 'HarmonicUpperBoundSphereDistancePairScore':
            xlink_pair_score = IMP.core.HarmonicUpperBoundSphereDistancePairScore(xlink_distance, k)
        elif xlink_score_type == 'XlinkScore':
            xlink_score = XlinkScore(xlink_distance)
            xlink_pair_score = IMP.core.DistancePairScore(xlink_score)
        elif xlink_score_type == 'LogHarmonic':
            print('WARNING: The LogHarmonic implementation is not very well tested')
            xlink_score = LogHarmonic(xlink_distance, k)
            xlink_pair_score = IMP.core.DistancePairScore(xlink_score)
        elif xlink_score_type == 'CombinedHarmonic':
            print('WARNING: The CombinedHarmonic implementation is not very well tested')
            xlink_score = CombinedHarmonic(xlink_distance, k)
            xlink_pair_score = IMP.core.DistancePairScore(xlink_score)

        log_file and log_file.write('Using {0}, class name {1}\n'.format(xlink_score_type, xlink_pair_score))

        names = xlink_set.get_protein_names()

        log_file and log_file.write('Protein names in xlink input:\n{0}\n'.format('    \n'.join([n for n in names])))

        log_file and log_file.write('No. of all xlinks: {0}\n'.format(len(xlink_set.data)))

        unique_xlinks_set = xlink_set.get_unique_by_prots_resi()

        if x_random_sample_fraction != 1.0:
            unique_xlinks_set = unique_xlinks_set.get_random_subset(x_random_sample_fraction)

        unique_xlinks_set.filterByType(['intra', 'inter'])
        log_file and log_file.write('No. of all unique inter and intra xlinks: {0}'.format(len(unique_xlinks_set.filtered)))
        unique_xlinks_set.clear_filtering()

        log_file and log_file.write('\n\n')
        log_file and log_file.write('Creating the restraints:\n')

        xlink_restraints = []
        self.xlink_restr_map = []
        added = []
        rs = IMP.RestraintSet(self.model, 'xlinks')

        for xlink in unique_xlinks_set.data:
            if min_ld_score is not None and pyxlinks.get_score(xlink) < min_ld_score:
                continue

            if not pyxlinks.is_mono_link(xlink):
                xlink_proper_but_not_added = True
                component1 = xlink['Protein1']
                component2 = xlink['Protein2']
                pos1 = int(xlink['AbsPos1'])
                pos2 = int(xlink['AbsPos2'])

                if (component1 == component2 and pos1 == pos2) or \
                    pyxlinks.is_clearly_dimeric(xlink):
                    is_clearly_dimeric = True
                else:
                    is_clearly_dimeric = False

                final_weight = weight
                if component1 != component2:
                    final_weight = weight * inter_xlink_scale

                if score_weighting:
                    score = pyxlinks.get_score(xlink)
                    if score is not None:
                        score_thresh = 30
                        if score >= score_thresh:
                            scale_factor = 1.0
                        else:
                            scale_factor = (score-0)/(score_thresh-0)
                        final_weight = final_weight * scale_factor

                if reweight_function:
                    final_weight = reweight_function(xlink, final_weight)

                restrained_pairs_across_states = []
                for state_index in range(len(self.pmi_system.states)):
                    ps1 = self.get_particles_for_resi(component1, pos1, state_index=state_index, first_copy_only=first_copy_only)
                    ps2 = self.get_particles_for_resi(component2, pos2, state_index=state_index, first_copy_only=first_copy_only)
                    # print component1, component2, ps1, ps2
                    restrained_pairs = list(product(ps1, ps2))
                    
                    are_overlapping = False
                    distances = []
                    for p1, p2 in restrained_pairs:
                        if (not IMP.atom.Residue.get_is_setup(p1)) and (not IMP.atom.Residue.get_is_setup(p2)):
                            #do not add xlink at all if it is not clearly dimeric and links a bead to itself
                            if component1 == component2:
                                if frags_overlap(IMP.atom.Fragment(p1), IMP.atom.Fragment(p2)):
                                    are_overlapping = True
                                    break


                        distances.append((IMP.core.get_distance(IMP.core.XYZR(p1), IMP.core.XYZR(p2)), p1, p2))

                    if not is_clearly_dimeric and are_overlapping:
                        # If it links to bead itself, means is satisfied
                        # And we need to remove it, otherwise it will be used as inter
                        # (because the subsequent code does not add xlinks of a bead to itself, but does add xlinks to symmetrical copies)
                        print('Not adding xlink to itself: ')
                        pyxlinks.print_xlink_nice(xlink)
                        continue

                    if len(restrained_pairs) == 1:
                        # Add normal restraint
                        p1 = restrained_pairs[0][0]
                        p2 = restrained_pairs[0][1]
                        if skip_pair_fn is not None:
                            if skip_pair_fn(p1, p2, component1, component2, xlink):
                                print("skipping because of skip_pair_fn")
                                xlink_proper_but_not_added = False
                                continue

                        if p1.get_index() == p2.get_index():
                            xlink_proper_but_not_added = False
                            print("skipping because links a bead to itself")
                            continue
                        # print 'mols', pos1, pos2, get_parent_mol(IMP.atom.Hierarchy(self.model, p1)), get_parent_mol(IMP.atom.Hierarchy(self.model, p1))
                        if is_clearly_dimeric:
                            if IMP.atom.Copy(get_parent_mol(IMP.atom.Hierarchy(self.model, p1))).get_copy_index() == IMP.atom.Copy(get_parent_mol(IMP.atom.Hierarchy(self.model, p2))).get_copy_index():
                                xlink_proper_but_not_added = False
                                continue


                        if frozenset([p1.get_index(), p2.get_index()]) not in added:
                            # restr = IMP.core.PairRestraint(xlink_pair_score, (p1, p2))
                            restrained_pairs_across_states.append(restrained_pairs[0])
                            # self.xlink_restraints.append(restr)
                            # rs.add_restraint(self.model, restr, p1.get_name()+' of '+component1+'-x-'+p2.get_name()+' of '+component2, weight=final_weight, max_score=False)
                            # log_file and log_file.write('Created non-ambiguous restraint: {0}'.format(restr))
                            # added.append(frozenset([p1.get_index(), p2.get_index()]))
                        xlink_proper_but_not_added = False

                    else:
                        # Add OR restraint
                        # The below conditions check if the two beads have been already restrained.
                        # They are ugly, assume the first pair comes the original symmetry copies
                        if len(restrained_pairs) > 0:
                            first_p1, first_p2 = restrained_pairs[0]
                            if frozenset([first_p1.get_index(), first_p2.get_index()]) not in added:
                                # added.append(frozenset([first_p1.get_index(), first_p2.get_index()]))
                                restrained_pairs_valid = []
                                for p1, p2 in restrained_pairs:
                                    if skip_pair_fn is not None:
                                        if skip_pair_fn(p1, p2, component1, component2, xlink):
                                            xlink_proper_but_not_added = False
                                            continue

                                    if p1.get_index() == p2.get_index():
                                        xlink_proper_but_not_added = False
                                        continue

                                    if is_clearly_dimeric:
                                        if IMP.atom.Copy(get_parent_mol(IMP.atom.Hierarchy(self.model, p1))).get_copy_index() == IMP.atom.Copy(get_parent_mol(IMP.atom.Hierarchy(self.model, p2))).get_copy_index():
                                            xlink_proper_but_not_added = False
                                            continue

                                    # print '>', p1, get_parent_chain(IMP.atom.Hierarchy(self.model, p1)).get_as_chain().get_id(), p2, get_parent_chain(IMP.atom.Hierarchy(self.model, p2)).get_as_chain().get_id()

                                    restrained_pairs_valid.append((p1, p2))

                                if len(restrained_pairs_valid):
                                    restrained_pairs_across_states.extend(restrained_pairs_valid)
                                    #TODO: use Close Pairs Container?
                                    # log_file and log_file.write('Adding clearly dimeric MinimumPairRestraint ambiguous restraint for: {0} {1}-{2} {3}:\n'.format(component1, pos1, component2, pos2))
                                    # pl = IMP.container.ListPairContainer(self.model)
                                    # for p1, p2 in restrained_pairs_valid:
                                    #     pl.add((p1, p2))

                                    # r = IMP.container.MinimumPairRestraint(xlink_pair_score, pl, 1, p1.get_name()+' of '+component1+'-x-'+p2.get_name()+' of '+component2)

                                    # log_file and log_file.write('Added clearly dimeric MinimumPairRestraint ambiguous restraint: {0}\n'.format(r))

                                    # if r is not None:
                                    #     r.set_weight(final_weight)
                                    #     log_file and log_file.write('    Set final weight to: {0}\n'.format(final_weight))
                                    #     self.xlink_restraints.append(r)
                                    #     xlink_proper_but_not_added = False

                            else:
                                print("Is redundant xlink:")
                                pyxlinks.print_xlink_nice(xlink)
                                xlink_proper_but_not_added = False

                if len(restrained_pairs_across_states) > 0:
                    first_p1, first_p2 = restrained_pairs_across_states[0]
                    if frozenset([first_p1.get_index(), first_p2.get_index()]) not in added:
                        added.append(frozenset([first_p1.get_index(), first_p2.get_index()]))
                        pl = IMP.container.ListPairContainer(self.model)
                        for p1, p2 in restrained_pairs_across_states:
                            pl.add((p1, p2))

                            added.append(frozenset([p1.get_index(), p2.get_index()]))

                        r_name = '{p1_name} of {component1}-x-{p2_name} of {component2}'.format(
                                p1_name=p1.get_name(),
                                component1=component1,
                                p2_name=p2.get_name(),
                                component2=component2
                            )
                        r = IMP.container.MinimumPairRestraint(xlink_pair_score, pl, 1, r_name)
                        if r is not None:
                            r.set_weight(final_weight)
                            log_file and log_file.write('    Set final weight to: {0}\n'.format(final_weight))
                            xlink_restraints.append(r)
                            xlink_proper_but_not_added = False
                            self.xlink_restr_map.append([xlink, r])

                if xlink_proper_but_not_added:
                    logging.debug('Could not add: ')
                    if logging.getLogger().level == logging.DEBUG:
                        pyxlinks.print_xlink_nice(xlink)

        self.xlink_restraints.extend(xlink_restraints)
        if log_filename:
            log_file.close()

        return xlink_restraints

    def create_interaction_restraints(self, multistate=False):
        restraints = []
        for data_item in self.config.data:
            if data_item['type'] == superconfig.CONNECTIVITY_DATA_TYPE and data_item.get('active') in (True, None):
                print('Creating interaction restraint')
                weight = data_item['weight']
                distance = data_item['distance']
                k = data_item['k']
                first_copy_only = data_item.get('first_copy_only') or False
                for comp_cfg1, comp_cfg2 in data_item['data']:
                    hiers1 = self.get_hs_from_comp_cfg(comp_cfg1, first_copy_only=first_copy_only, resolution=data_item['repr_resolution'])
                    hiers2 = self.get_hs_from_comp_cfg(comp_cfg2, first_copy_only=first_copy_only, resolution=data_item['repr_resolution'])
                    #TODO: pass selections of rigid body particles when possible for efficiency?
                    restr = IMP.atom.create_connectivity_restraint([IMP.atom.Selection(hiers1),
                                                               IMP.atom.Selection(hiers2)],
                                                               distance,
                                                               k, data_item['name'])
                    restr.set_weight(weight)
                    restraints.append(restr)
                    logging.debug(' '.join(map(str, (hiers1, hiers2, IMP.core.get_distance(IMP.core.XYZ(hiers1[0]), IMP.core.XYZ(hiers2[0])), IMP.core.get_distance(IMP.core.XYZR(hiers1[0]), IMP.core.XYZR(hiers2[0])), restr.get_score()))))

        return restraints

    def move_floppy_rbs_close_to_map(self, rbs):
        for data_item in self.config.data:
            if data_item['type'] == superconfig.EM_DATA_TYPE and data_item.get('active') in (True, None):
                dmap = data_item['em_map']
        bb =  IMP.em.get_bounding_box(dmap)
        for rb in rbs:
            old_coord=IMP.core.XYZ(rb).get_coordinates()
            translation =  IMP.algebra.get_random_vector_on(bb)
            rotation = IMP.algebra.get_random_rotation_3d()
            transformation = IMP.algebra.Transformation3D(
                rotation,
                translation-old_coord)
            IMP.core.transform(rb, transformation)

    def move_floppy_rbs_close_to_hierarchies(self, rbs, hierarchies):
        bb =  IMP.core.get_enclosing_sphere(hierarchies)
        for rb in rbs:
            #Move to the enclosing sphere:
            old_coord=IMP.core.XYZ(rb).get_coordinates()
            translation =  IMP.algebra.get_random_vector_on(bb)
            rotation = IMP.algebra.get_identity_rotation_3d()
            transformation = IMP.algebra.Transformation3D(
                rotation,
                translation-old_coord)
            if not IMP.core.RigidBody.get_is_setup(rb):
                rb = IMP.core.XYZ(rb)
            IMP.core.transform(rb, transformation)

            #rotate locally in the new position:
            transformation = IMP.algebra.get_random_local_transformation(IMP.core.XYZ(rb).get_coordinates(),
                                                                        0.0001, #The sphere must have positive radius
                                                                        6.28319) #360 degrees
            IMP.core.transform(rb, transformation)            

        return bb


    def move_floppy_beads_close_to_hierarchies(self, mode='flanks'):
        for serie in self.config.series:
            for state_mols in serie.molecules:
                for mol in state_mols:
                    hiers = IMP.pmi.tools.input_adaptor(mol)
                    rbs, beads = IMP.pmi.tools.get_rbs_and_beads(hiers)
                    atomic_res = mol.get_atomic_residues()
                    non_atomic_res = mol.get_non_atomic_residues()

                    rigid_resi_ids = self.get_mol_rigid_resi(mol)
                    atomic_res_rigid = [resi for resi in atomic_res if resi.get_index() in rigid_resi_ids]
                    atomic_res_non_rigid = [resi for resi in atomic_res if resi.get_index() not in rigid_resi_ids]

                    resi = sorted(list(mol.get_residues()), key=lambda r: r.get_index())
                    resi_ids = [r.get_index() for r in resi]

                    ps = IMP.atom.Selection(mol.get_hierarchy(), residue_indexes=resi_ids, resolution=self.STRUCT_RESOLUTIONS[0]).get_selected_particles(with_representation=True)
                    ps = sorted(ps, key= lambda p: get_first_index(self.model, p))
                    # print(mol, atomic_res_non_rigid)
                    if beads:
                        # for atom in ps:
                        #     print(atom, atom in beads)
                        def grouper(atom):
                            if IMP.core.RigidMember.get_is_setup(atom):
                                return 'rb'
                            elif IMP.atom.Residue.get_is_setup(atom) and IMP.atom.Residue(atom).get_index() in [x.get_index() for x in atomic_res_non_rigid]:
                                return 'beads_from_input_structure'
                            elif atom in beads:
                                return 'beads_missing_regions'
                            else:
                                raise Exception("Should not get here!")
                        grouped = groupby(ps, grouper)
                        groups = [(k, list(v)) for k, v in grouped]

                        if len(groups) > 1:
                            #first, position beads_from_input_structure
                            i = 0
                            for key, group in groups:
                                if key == 'beads_from_input_structure':
                                    rb = None

                                    if i == 0:
                                        if IMP.core.RigidMember.get_is_setup(groups[i+1][1][0]):
                                            rb = IMP.core.RigidMember(groups[i+1][1][0]).get_rigid_body()
                                    elif i == len(groups)-1:
                                        if IMP.core.RigidMember.get_is_setup(groups[i-1][1][0]):
                                            rb = IMP.core.RigidMember(groups[i-1][1][0]).get_rigid_body()
                                    else:
                                        if IMP.core.RigidMember.get_is_setup(groups[i+1][1][0]):
                                            rb = IMP.core.RigidMember(groups[i+1][1][0]).get_rigid_body()
                                        elif IMP.core.RigidMember.get_is_setup(groups[i-1][1][0]):
                                            rb = IMP.core.RigidMember(groups[i-1][1][0]).get_rigid_body()

                                    if rb:
                                        for bead in group:
                                            if not self.is_sym_constrained(bead):
                                                tr3d = rb.get_reference_frame().get_transformation_to() / self.rbs_ini_tr3ds[rb]
                                                IMP.core.transform(IMP.core.XYZ(bead), tr3d)

                                i = i + 1

                            #second, position beads_missing_regions
                            i = 0
                            for key, group in groups:
                                if key == 'beads_missing_regions':
                                    group = list(group)
                                    if i == 0:
                                        for bead in group:
                                            if not self.is_sym_constrained(bead):
                                                IMP.core.XYZ(bead).set_coordinates(IMP.core.XYZ(groups[i+1][1][0]).get_coordinates())
                                    elif i == len(groups)-1: 
                                        for bead in group:
                                            if not self.is_sym_constrained(bead):
                                                IMP.core.XYZ(bead).set_coordinates(IMP.core.XYZ(groups[i-1][1][-1]).get_coordinates())
                                    else:
                                        if mode == 'flanks':
                                            half1 = group[:int(len(group)/2)]
                                            half2 = group[int(len(group)/2):]
                                            for bead in half1:
                                                if not self.is_sym_constrained(bead):
                                                    IMP.core.XYZ(bead).set_coordinates(IMP.core.XYZ(groups[i-1][1][-1]).get_coordinates())
                                            for bead in half2:
                                                if not self.is_sym_constrained(bead):
                                                    IMP.core.XYZ(bead).set_coordinates(IMP.core.XYZ(groups[i+1][1][0]).get_coordinates())
                                        elif mode == 'line':
                                            start = IMP.core.XYZ(groups[i-1][1][-1]).get_coordinates()
                                            end = IMP.core.XYZ(groups[i+1][1][0]).get_coordinates()
                                            line = start - end
                                            n = len(group)
                                            inc = 0 #random.uniform(0,5) 
                                            offset = 0
                                            for j, bead in enumerate(group):
                                                if not self.is_sym_constrained(bead):
                                                    a = float(j+1) / n             # rescale 0 < i < n --> 0 < a < 1

                                                    x = (1 - a) * start[0] + a * end[0] + offset #+ random.uniform(-2,2)   # interpolate x coordinate
                                                    y = (1 - a) * start[1] + a * end[1] + offset #+ random.uniform(-2,2)   # interpolate y coordinate
                                                    z = (1 - a) * start[2] + a * end[2] + offset #+ random.uniform(-2,2)   # interpolate z coordinate

                                                    if j == int(n/2):
                                                        inc = -inc
                                                    offset = offset + inc

                                                    IMP.core.XYZ(bead).set_coordinates((x, y, z))




                                i = i + 1


        self.model.update()  # necessary to update symmetrical constraints
        # self.write_as_rmf('ini.rmf', [])

    def iter_consecutive_beads(self, mol):
        resi = sorted(list(mol.get_residues()), key=lambda r: r.get_index())
        resi_ids = [r.get_index() for r in resi]

        ps = IMP.atom.Selection(mol.get_hierarchy(), residue_indexes=resi_ids, resolution=self.STRUCT_RESOLUTIONS[0]).get_selected_particles(with_representation=False)
        ps = sorted(ps, key= lambda p: get_first_index(self.model, p))

        for p in ps:
            atom = None
            if IMP.atom.Residue.get_is_setup(p):
                atom = get_atom_of_resi(p, atom_type=IMP.atom.AT_CA)
                if atom is None:
                    atom = p
            else:
                atom = p

            yield atom

    def get_equivalent_ps_of_rbs(self, rb1, rb2, nrandom=None):
        '''
        TODO: improve by sorting etc.
        '''
        rb1_ps = [self.model.get_particle(pi) for pi in rb1.get_member_indexes()]
        rb2_ps = [self.model.get_particle(pi) for pi in rb2.get_member_indexes()]
        if len(rb1_ps) != len(rb2_ps):
            raise Exception('len(rb1_ps) != len(rb2_ps)')
        all_pairs = list(zip(rb1_ps, rb2_ps))
        
        for p1, p2 in all_pairs:
            if str(p1) != str(p2):
                raise Exception('Failed to get equivalent ps')

        if nrandom is not None:
            #get n random particle pairs from the rbs
            #try to get pairs as distant to each other by testing 10 pair sets
            max_tot_distance = 0.0
            for i in range(10):
                selected = random.sample(all_pairs, nrandom)
                tot_distance = 0.0
                # best_pairs = selected

                for n in range(1, nrandom):
                    tot_distance += IMP.core.get_distance(IMP.core.XYZ(selected[n-1][0]), IMP.core.XYZ(selected[n][0]))

                    if tot_distance > max_tot_distance:
                        best_pairs = selected
                        max_tot_distance = tot_distance

            return best_pairs
        else:
            return all_pairs

    def add_parsimonious_states_restraints(self, weight=1.0, distance_threshold=0.0, k=1.0, random_fraction=1.0, resolution=0, restrain_rb_transformations=True, exclude_rbs_fn=lambda x: False):
        '''
        Restrain orientations of corresponding rigid bodies in all states,
        so they are approximately in similar orientations in all states,
        under a parsimonious assumption that the conformational change would not be too big.

        Warning: assumes mols in all states are the same and in the same order 
        For now restraints all states to be similar to the first.

        for now it's only working on rigid bodies
        '''
        if len(self.pmi_system.states) == 1:
            raise Exception('Only one state found. Parsimonious states restraints are only useful for more than one state.')

        print("Adding parsimonious states restraints for resolution {0} and distance threshold {1} and weight {2}".format(resolution, distance_threshold, weight))
        rs = IMP.RestraintSet(self.model)
        rs.set_name('parsimonious_states_restraints')

        no_of_molecules = len(self.pmi_system.states[0].molecules)
        ps_lists = [] #all ps for each state
        for state_idx, state in enumerate(self.pmi_system.states):
            if len(state.molecules) != no_of_molecules:
                raise Exception('State 0 has {0} and state {1} has {2} molecules. For parsimonious states restraints all states must have equal number of molecules'.format(no_of_molecules, state_idx, len(state.molecules)))
            ps_lists.append([])

            for mol_copies in state.molecules.values():
                for mol in mol_copies:
                    ps_lists[-1].extend(IMP.atom.Selection(mol.get_hierarchy(),resolution=resolution).get_selected_particles())

        no_of_ps = len(ps_lists[0])
        for state_idx, ps_list in enumerate(ps_lists):
            if len(ps_list) != no_of_ps:
                raise Exception('State 0 has {0} and state {1} has {2} particles for parsimonious states restraints, but all states must have equal number of particles'.format(no_of_ps, state_idx, len(ps_list)))

        ps_sets = zip(*ps_lists)

        ps_map = {ps_set[0].get_index(): ps_set for ps_set in ps_sets} #map index of 1st state p to to equvialent ps from all states
        lsc = IMP.container.ListSingletonContainer(self.model)
        lsc.add(IMP.get_indexes(ps_lists[0]))
        # rbcpf = IMP.core.RigidClosePairsFinder()
        # cpc = IMP.container.ClosePairContainer(lsc, 0.0, rbcpf, 5.0)
        cpc = IMP.container.ClosePairContainer(lsc, distance_threshold, 0.0)
        self.model.update() #to update the ClosePairContainer
        # cpc = IMP.container.ClosePairContainer(ps_lists[0], 5.0, 3.0)
        all_close_pairs = [(self.model.get_particle(i), self.model.get_particle(j)) for i,j in cpc.get_contents()]
        close_pairs = []
        print(len(all_close_pairs))
        for p1, p2 in all_close_pairs:
            if IMP.core.RigidMember.get_is_setup(p1) and IMP.core.RigidMember.get_is_setup(p2): # for now it's only working on rigid bodies
                rb1 = IMP.core.RigidMember(p1).get_rigid_body()
                rb2 = IMP.core.RigidMember(p2).get_rigid_body()
                if (rb1 != rb2 and                                                              # restraining within the same rigid body of course does not make sense
                    not all([self.is_sym_constrained(rb1), self.is_sym_constrained(rb2)]) and   # do not restrain between symmetry constrained rbs, but do restrain between reference and their symmetrical copies
                    # not any([self.is_sym_constrained(rb1), self.is_sym_constrained(rb2)]) and   # do not restrain symmetry constrained rbs                
                    not exclude_rbs_fn(rb1) and                         # exclude based on user-defined conditions
                    not exclude_rbs_fn(rb2) and                       # exclude based on user-defined conditions
                    not restrain_rb_transformations):
                        close_pairs.append((p1, p2))

            else: #flexible beads 
                close_pairs.append((p1, p2)) #TODO: test how it works for flexible beads

        print(len(close_pairs))
        close_sets = []
        for p1, p2 in close_pairs:
            dist = IMP.core.get_distance(IMP.core.XYZ(p1), IMP.core.XYZ(p2))
            print([get_parent_mol(IMP.atom.Hierarchy(p)) for p in (p1, p2)], [IMP.atom.get_state_index(IMP.atom.Hierarchy(p)) for p in (p1, p2)], p1, p2, dist)
            idx1 = ps_map[p1.get_index()]
            idx2 = ps_map[p2.get_index()]
            close_sets.append((idx1, idx2))

        added_rb_pairs = []
        for ps_set1, ps_set2 in close_sets:
            pairs = list(zip(ps_set1, ps_set2))
            ref_pair = pairs[0]

            for other_pair in pairs[1:]:
                restr = core.SimilarTwoDistancesRestraint(self.model, ref_pair, other_pair, k)
                rs.add_restraint(restr)

        if restrain_rb_transformations:
            for rb_set in zip(*self.rbs_temp):
                ref_rb = rb_set[0]
                for other_rb in rb_set[1:]:
                    print(ref_rb.get_name(), other_rb.get_name())
                    restr = core.TransformedRotationPairRestraint(self.model, ref_rb.get_particle(), other_rb.get_particle(), IMP.algebra.get_identity_transformation_3d(), resolution=max(self.STRUCT_RESOLUTIONS))
                    rs.add_restraint(restr)

        rs.set_weight(weight)

        return [rs]


    def get_representative_ps_of_rb(self, rb):
        rb_ps = [self.model.get_particle(pi) for pi in rb.get_member_indexes()]
        #for now define orientation of rb with two particles
        #TODO: calculate sth sophisticated, using PCA?
        return (rb_ps[0], rb_ps[-1])

    def get_representative_ps_of_hiers(self, hierarchies, max_number_of_particles=5):
        '''
        Return:
        o a subset of hierarchies that define their structure shape if they are within rigid body
            WARNING: CURRENTLY SIMPLY THE FIRST AND THE LAST PARTICLE WITH RB IS TAKEN
        o all beads within these hierarchies
        '''
        beads = []
        rbs = []
        grouped_by_rbs = []
        for h in hierarchies:
            p = h.get_particle()
            if IMP.core.RigidMember.get_is_setup(p):
                rb = IMP.core.RigidMember(p).get_rigid_body()
                if rb not in rbs:
                    rbs.append(rb)
                    grouped_by_rbs.append([p])
                else:
                    grouped_by_rbs[rbs.index(rb)].append(p)

            else:
                beads.append(p)

        all_ps = []
        for ps in grouped_by_rbs:
            if len(ps) > max_number_of_particles:
                every_nth = int(len(ps)/max_number_of_particles)
                all_ps.extend(ps[::every_nth])
            else:
                all_ps.extend(ps)

            # all_ps.append(ps[0])
            # all_ps.append(ps[-1])

        all_ps.extend(beads)
        
        return all_ps

    def add_similar_orientation_restraints(self):
        restraints = []
        for data_item in self.config.data:
            if data_item['type'] == "similar_orientation" and data_item.get('active') in (True, None):
                print("Creating similar_orientation restraint {0}".format(data_item['name']))

                if data_item.get('k'):
                    k = data_item.get('k')
                else:
                    k = 1

                if data_item.get('weight'):
                    weight = data_item.get('weight')
                else:
                    weight = 1

                first_copy_only = data_item.get('first_copy_only') or False
                if data_item.get('repr_resolution'):
                    repr_resolution = data_item.get('repr_resolution')
                else:
                    repr_resolution = 1

                rs = IMP.RestraintSet(self.model)
                rs.set_name(data_item['name'])

                hierarchies = []
                for data_row in data_item['data']:
                    if len(data_row) > 2:
                        raise NotImplementedError("similar_orientation restraint currently supported only for pairs")

                    ps_pairs = []
                    for comp_cfg_pair in data_row:
                        hiers_pair = [self.get_hs_from_comp_cfg(comp_cfg, resolution=repr_resolution, first_copy_only=first_copy_only) for comp_cfg in comp_cfg_pair]
                        ps_pair = [self.get_representative_ps_of_hiers(hiers) for hiers in hiers_pair]
                        ps_pairs.append(product(*ps_pair))

                    for pair1, pair2 in zip(*ps_pairs):
                        logging.debug(' '.join(map(str, [pair1, [get_parent_mol(IMP.atom.Hierarchy(p)) for p in pair1], pair2, [get_parent_mol(IMP.atom.Hierarchy(p)) for p in pair2]])))
                        restr = core.SimilarTwoDistancesRestraint(self.model, pair1, pair2, k)
                        rs.add_restraint(restr)

                rs.set_weight(weight)
                restraints.append(rs)

        return restraints

    def add_angle_restraints(self):
        restraints = []

        for data_item in self.config.data:
            if data_item['type'] == "angle_restraints" and data_item.get('active') in (True, None):
                print("Creating angle restraint {0}".format(data_item['name']))

                if data_item.get('k'):
                    k = data_item.get('k')
                else:
                    k = 1

                if data_item.get('weight'):
                    weight = data_item.get('weight')
                else:
                    weight = 1

                first_copy_only = data_item.get('first_copy_only') or False

                if data_item.get('repr_resolution'):
                    repr_resolution = data_item.get('repr_resolution')
                else:
                    repr_resolution = 1

                if 'angle' not in data_item:
                    raise AttributeError('Angle restraints must have an "angle" attribute')
                angle = data_item.get('angle')

                rs = IMP.RestraintSet(self.model)
                rs.set_name(data_item['name'])

                all_rbs = []
                for data_row in data_item['data']:
                    hiers = []
                    for comp_cfg in data_row['components']:
                        hiers.extend(self.get_hs_from_comp_cfg(comp_cfg, resolution=repr_resolution, first_copy_only=first_copy_only))
                    rbs, beads = IMP.pmi.tools.get_rbs_and_beads(hiers)
                    if len(rbs) != 1:
                        raise ValueError('Each item in angle_restraints data must define at most one rigid body')
                    all_rbs.extend(rbs)

                for first, second in zip(all_rbs, all_rbs[1:]):
                    restr = core.AngleRestraint(self.model, first, second, angle, k, repr_resolution)
                    rs.add_restraint(restr)

                rs.set_weight(weight)
                restraints.append(rs)

        return restraints

    def add_collinear_restraints(self):
        restraints = []

        for data_item in self.config.data:
            if data_item['type'] == "collinear_restraints" and data_item.get('active') in (True, None):
                print("Creating linear restraint {0}".format(data_item['name']))

                if data_item.get('k'):
                    k = data_item.get('k')
                else:
                    k = 1

                if data_item.get('weight'):
                    weight = data_item.get('weight')
                else:
                    weight = 1

                first_copy_only = data_item.get('first_copy_only') or False

                if data_item.get('repr_resolution'):
                    repr_resolution = data_item.get('repr_resolution')
                else:
                    repr_resolution = 1

                rs = IMP.RestraintSet(self.model)
                rs.set_name(data_item['name'])

                all_rbs = []
                for data_row in data_item['data']:
                    hiers = []
                    for comp_cfg in data_row['components']:
                        hiers.extend(self.get_hs_from_comp_cfg(comp_cfg, resolution=repr_resolution, first_copy_only=first_copy_only))
                    rbs, beads = IMP.pmi.tools.get_rbs_and_beads(hiers)
                    if len(rbs) != 1:
                        raise ValueError('Each item in angle_restraints data must define at most one rigid body')
                    all_rbs.extend(rbs)

                for first, second in zip(all_rbs, all_rbs[1:]):
                    restr = core.CollinearRestraint(self.model, first, second, k, repr_resolution)
                    rs.add_restraint(restr)

                rs.set_weight(weight)
                restraints.append(rs)

        return restraints

    def create_elastic_network(self):
        """Create elastic network restraints on interfaces between rigid bodies.
        """
        restraints = []
        for data_item in self.config.data:
            if data_item['type'] == "elastic_network" and data_item.get('active') in (True, None):
                print("Creating elastic network restraint {0}".format(data_item['name']))

                if data_item.get('distance_threshold'):
                    distance_threshold = data_item.get('distance_threshold')
                else:
                    distance_threshold = 10

                optimized_components = data_item.get('optimized_components')
                if not optimized_components:
                    optimized_components = self.make_comp_cfgs_for_all_subunits()

                first_copy_only = data_item.get('first_copy_only') or False
                if 'repr_resolution' in data_item:
                    repr_resolution = data_item.get('repr_resolution')
                else:
                    repr_resolution = 1

                if 'k' in data_item:
                    k = data_item.get('k')
                else:
                    k = 1

                if 'weight' in data_item:
                    weight = data_item.get('weight')
                else:
                    weight = 1

                if 'random_fraction' in data_item:
                    random_fraction = data_item.get('random_fraction')
                else:
                    random_fraction = 1.0

                rs = IMP.RestraintSet(self.model)
                rs.set_name(data_item['name'])
                logging.debug('Elastic network: Calculating distances')

                if not data_item.get('elastic_network_source'):
                    #find distances between rigid bodies with hierarchies
                    #selected by all optimized_components

                    hierarchies = []
                    for comp_cfg in optimized_components:
                        for leaves in [IMP.core.get_leaves(h) for h in self.get_hs_from_comp_cfg(comp_cfg, resolution=repr_resolution, first_copy_only=first_copy_only)]:
                            hierarchies.extend(leaves)

                    ps = [h.get_particle() for h in hierarchies]
                    ps = [p for p in ps if IMP.core.RigidMember.get_is_setup(p)]

                    lsc = IMP.container.ListSingletonContainer(self.model)
                    lsc.add(IMP.get_indexes(ps))
                    rbcpf = IMP.core.RigidClosePairsFinder() #TODO: shall we use distances between points rather than spheres?
                    slack = 0
                    cpc = IMP.container.ClosePairContainer(lsc, distance_threshold, rbcpf, slack)
                    self.model.update() #to update the container
                    distances = []
                    for p1, p2 in [(self.model.get_particle(i), self.model.get_particle(j)) for i,j in cpc.get_contents()]:
                        if data_item.get('within_molecule') and get_parent_mol(IMP.atom.Hierarchy(p1)) != get_parent_mol(IMP.atom.Hierarchy(p2)):
                            continue #TODO: check for 'within_molecule' in the finder to avoid unnecessary calculations?
                        distances.append((p1, p2, IMP.core.get_distance(IMP.core.XYZR(p1), IMP.core.XYZR(p2))))
                else:

                    if repr_resolution != 1 or 1 not in self.STRUCT_RESOLUTIONS:
                        raise Exception('Currently only repr_resolution=1 supported when using elastic_network_source in elastic network restraints (repr_resolution == {0}, struct_resolutions == {1}'.format(repr_resolution, self.STRUCT_RESOLUTIONS))

                    model = IMP.Model()
                    source_hierarchy_lists = []
                    for comp_cfg in data_item.get('elastic_network_source'):
                        #TODO: support other than ca_only?
                        if comp_cfg.get('resi_ranges'):
                            resi_ranges = comp_cfg.get('resi_ranges')
                        else:
                            resi_ranges = [None]

                        residues = []
                        for resi_range in resi_ranges: #TODO: this read the PDB file every time again, improve for speed?
                            residues.extend(system_tools.get_structure(model,
                                                self.config.find_path(comp_cfg['filename']),
                                                comp_cfg['chain_id'],
                                                res_range=resi_range,
                                                offset=0,
                                                model_num=None,
                                                ca_only=True))
                        source_hierarchy_lists.append(residues)

                    list_containers = []
                    for source_hierarchies in source_hierarchy_lists:
                        ps = [h.get_children()[0].get_particle() for h in source_hierarchies]
                        lsc = IMP.container.ListSingletonContainer(model)
                        lsc.add(IMP.get_indexes(ps))
                        list_containers.append(lsc)

                    #TODO: make for all combinations for len(list_containers) > 2
                    slack = 0
                    model.update()
                    pair_container = IMP.container.CloseBipartitePairContainer(list_containers[0], list_containers[1], distance_threshold, slack)
                    model.update() #to update the container
                    close_pairs = pair_container.get_indexes() #TODO: shall we use distances between points rather than spheres?
                    
                    distances = []
                    target_hierarchies = []
                    for comp_cfg in optimized_components:
                        out = []
                        for leaves in [IMP.core.get_leaves(h) for h in self.get_hs_from_comp_cfg(comp_cfg, resolution=repr_resolution, first_copy_only=first_copy_only)]:
                            out.extend(leaves)
                        target_hierarchies.append(out)

                    #TODO: select all matching pairs, not just the first (get_selected_particles()[0])
                    added = []
                    for p1, p2 in close_pairs:
                        atom1 = IMP.atom.Atom(model.get_particle(p1))
                        resi1 = IMP.atom.Residue(atom1.get_parent())
                        sel_particles1 = IMP.atom.Selection(target_hierarchies[0], residue_index=resi1.get_index(), resolution=repr_resolution).get_selected_particles()
                        atom2 = IMP.atom.Atom(model.get_particle(p2))
                        resi2 = IMP.atom.Residue(atom2.get_parent())
                        sel_particles2 = IMP.atom.Selection(target_hierarchies[1], residue_index=resi2.get_index(), resolution=repr_resolution).get_selected_particles()
                        if sel_particles1 and sel_particles2:
                            target_particle1 = sel_particles1[0]
                            target_particle2 = sel_particles2[0]
                            if data_item.get('within_molecule') and get_parent_mol(IMP.atom.Hierarchy(target_particle1)) != get_parent_mol(IMP.atom.Hierarchy(target_particle2)):
                                continue #TODO: check for 'within_molecule' in the finder to avoid unnecessary calculations?
                            if (target_particle1, target_particle2) in added:
                                continue

                            dist = IMP.core.get_distance(IMP.core.XYZR(atom1), IMP.core.XYZR(atom2))
                            distances.append((target_particle1, target_particle2, dist))
                            added.append((target_particle1, target_particle2))

                distances = random.sample(distances, int(random_fraction*len(distances)))
                logging.debug('Elastic network: Creating restraints')
                for p1, p2, dist in distances:
                    score = IMP.core.HarmonicSphereDistancePairScore(dist, k)
                    restr = IMP.core.PairRestraint(self.model, score, (p1, p2))
                    rs.add_restraint(restr)

                rs.set_weight(weight)
                print('{0} distances used for the elastic network restraint {1}'.format(len(distances), data_item['name']))

                restraints.append(rs)
                
        return restraints

    def add_em_restraints(self,
                        em_restraint='FitRestraint',
                        weight=100,
                        first_copy_only=False,
                        resolution=0):
        em_map_cfg = None
        em_restraints = []
        for data_item in self.config.data:
            if data_item['type'] == superconfig.EM_DATA_TYPE and data_item.get('active') in (True, None):
                if not hasattr(data_item, 'em_map'):
                    data_item['em_map'] = load_em_density_map(self.config.find_path(data_item['filename']), data_item['voxel_size'], data_item['resolution'])

                if data_item.get('em_restraint_type'):
                    em_restraint_type = data_item.get('em_restraint_type')
                else:
                    em_restraint_type = em_restraint

                if data_item.get('weight'):
                    weight = data_item.get('weight')
                else:
                    weight = 1.

                if data_item.get('first_copy_only'):
                    first_copy_only = data_item.get('first_copy_only')
                else:
                    first_copy_only = False

                if data_item.get('repr_resolution'):
                    repr_resolution = data_item.get('repr_resolution')
                else:
                    repr_resolution = 0

                if data_item.get('max_distance'):
                    max_distance = data_item.get('max_distance')
                else:
                    max_distance = 100

                if data_item.get('threshold'):
                    threshold = data_item.get('threshold')
                else:
                    threshold = 0

                if data_item.get('offset'):
                    offset = data_item.get('offset')
                else:
                    offset = 0

                hierarchies = []
                optimized_components = data_item.get('optimized_components')

                if not optimized_components:
                    optimized_components = self.make_comp_cfgs_for_all_subunits()

                for comp_cfg in optimized_components:
                    for leaves in [IMP.core.get_leaves(h) for h in self.get_hs_from_comp_cfg(comp_cfg, resolution=repr_resolution, first_copy_only=first_copy_only)]:
                        hierarchies.extend(leaves)

                if em_restraint_type == 'EnvelopeFitRestraint':
                    penetration_threshold = 30
                    em_r = IMP.em.EnvelopeFitRestraint(hierarchies, data_item['em_map'], threshold, penetration_threshold)

                elif em_restraint_type == 'EnvelopePenetrationRestraint':
                    ps = [h.get_particle() for h in hierarchies]
                    em_r = IMP.em.EnvelopePenetrationRestraint(ps, data_item['em_map'], threshold)
                elif em_restraint_type == 'EnvelopePenetrationRestraint1':
                    ps = [h.get_particle() for h in hierarchies]
                    em_r = core.EnvelopePenetrationRestraint1(
                        self.model,
                        ps,
                        data_item['em_map'],
                        threshold,
                        max_distance=max_distance)

                elif em_restraint_type == 'FitRestraint':
                    ps = [h.get_particle() for h in hierarchies]

                    scale = 100
                    em_r = IMP.em.FitRestraint(
                        ps,
                        data_item['em_map'],
                        [0., 0.],
                        IMP.atom.Mass.get_mass_key(),
                        scale,
                        False)
                        # IMP.em.BINARIZED_SPHERE))
                    # print(data_item['em_map'], em_restraint_type)

                elif em_restraint_type == 'ExcludeMapRestraint':
                    ps = [h.get_particle() for h in hierarchies]
                    em_r = core.ExcludeMapRestraint(
                        self.model,
                        ps,
                        data_item['em_map'],
                        threshold)
                    # print(data_item['em_map'], em_restraint_type)
                elif em_restraint_type == 'ExcludeMapRestraint1':
                    ps = [h.get_particle() for h in hierarchies]
                    em_r = core.ExcludeMapRestraint1(
                        self.model,
                        ps,
                        data_item['em_map'],
                        threshold,
                        max_distance=max_distance,
                        offset=offset)
                    # print(data_item['em_map'], em_restraint_type)
                elif em_restraint_type == 'CloseToEnvelopeRestraint':
                    ps = [h.get_particle() for h in hierarchies]
                    em_r = core.CloseToEnvelopeRestraint(
                        self.model,
                        ps,
                        data_item['em_map'],
                        threshold,
                        max_distance=max_distance)
                    # print(data_item['em_map'], em_restraint_type)
                elif em_restraint_type == 'GaussianEMRestraint':
                    sys.exit()
                    #TODO: add as single restraint for all states?
                    for state in self.pmi_system.states:
                        for molname, copies in state.molecules.items():
                            for mol in copies:
                                densities = IMP.atom.Selection(mol.get_hierarchy(),representation_type=IMP.atom.DENSITIES).get_selected_particles()
                                print(densities)
                                middle_mass=sum((IMP.atom.Mass(p).get_mass() for h in densities for p in IMP.atom.get_leaves(h)))
                                emr = IMP.pmi.restraints.em.GaussianEMRestraint(
                                    densities,
                                    # target_is_rigid_body=True,
                                    target_fn='/struct/cmueller/kosinski/devel/SuperConfig/test/data/1ujz/1ujz.10A.gmm150.txt',
                                    # target_fn='/g/scb/cmueller/kosinski/elongator/elongator_protocol/elongator_modeling/data/EM_maps_new/refined_class2_cl3dround2cl3.mirror.gmm50.txt',  #HACK: add to input created by user, see top of file
                                    slope=0.0000000000001,                           # a small number, helps drag bits into map
                                    # scale_target_to_mass=True,           # if the model is the same size as map, usually set to True
                                    target_mass_scale=middle_mass,             # manually set the mass of the target map (remove if you set above to True)
                                    weight=10000.0)                         # the data weight
                                emr.add_to_model()
                                # emr.add_target_density_to_hierarchy(state)
                                # em_r = emr.get_restraint_set()
                                # em_rs.extend(emr.get_restraint_set().get_restraints())
                else:
                    raise Exception('Unrecognized EM restraint type: '+em_restraint_type)
                em_restraints.append(em_r)
                em_r.set_weight(weight)
                if data_item.get('weight'):
                    em_r.set_name(data_item.get('name'))

        self.em_restraints.extend(em_restraints)

        return em_restraints

    def set_initial_positions(self):
        for rb_cfg in self.config.rigid_bodies:
            # print([(k, v) if k != 'positions' else (k, len(v)) for k, v in rb_cfg.items() ])
            state_idx = rb_cfg['components'][0]['states'][0]
            rb = rb_cfg['rb']
            if 'initial_position' in rb_cfg:
                tr3d = IMP.algebra.Transformation3D(IMP.algebra.get_rotation_from_matrix(*rb_cfg['initial_position']['rotation']),
                                                        IMP.algebra.Vector3D(*rb_cfg['initial_position']['translation']))
                IMP.atom.transform(rb, tr3d)

        self.model.update()  # necessary to update symmetrical constraints

    def randomize(self, center='local',
                 max_translation=50.0,
                 max_rotation=2.0*math.pi,
                 remove_clashes=False,
                 remove_clashes_iter=1000,
                 repeat=1):
        '''
        @param minimize_overlap Whether to perform MC optimization to remove clashes
        '''

        things = []

        for serie in self.config.series:
            for state_mols in serie.molecules:
                for mol in state_mols:
                    hiers = IMP.pmi.tools.input_adaptor(mol)
                    rbs, beads = IMP.pmi.tools.get_rbs_and_beads(hiers)
                    things.extend(rbs+beads)

        if hasattr(center, '__iter__') and len(center) == 3:
            center = IMP.algebra.Vector3D(center)

        if center == 'global':
            center = IMP.core.get_centroid(things)

        for thing in things:
            if IMP.core.RigidBody.get_is_setup(thing) and self.is_rb_frozen(thing):
                continue
            for i in range(repeat):
                if isinstance(center, str) and center == 'local':
                    random_tr3d = IMP.algebra.get_random_local_transformation(IMP.core.get_centroid(IMP.atom.get_leaves(IMP.atom.Hierarchy(thing))), max_translation, max_rotation)
                    IMP.atom.transform(thing, random_tr3d)
                elif isinstance(center, IMP.algebra.Vector3D):
                    #Code based on PMI.tools.shuffle, can we avoid moving to origin first?
                    sphere = IMP.algebra.Sphere3D(center, max_translation)
                    transformation_orig = IMP.algebra.Transformation3D(
                        IMP.algebra.get_identity_rotation_3d(),
                        -IMP.core.XYZ(thing).get_coordinates())
                    IMP.atom.transform(thing, transformation_orig)
                    rotation = IMP.algebra.get_random_rotation_3d()
                    translation = IMP.algebra.get_random_vector_in(sphere)
                    random_tr3d = IMP.algebra.Transformation3D(rotation,
                                                               translation)

                    IMP.atom.transform(thing, random_tr3d)
                else:
                    raise NotImplementedError

        self.model.update()  # necessary to update symmetrical constraints

        if remove_clashes:
            #TODO: select appropriate movers
            #TODO: do not optimize frozen rbs
            sf = IMP.core.RestraintsScoringFunction(self.ev_restraints+self.conn_restraints, "ev scoring function")
            o = IMP.core.MonteCarlo(self.model)
            serial_mover = IMP.core.SerialMover(self.dof.movers)
            o.add_mover(serial_mover)
            # for mover in self.dof.movers:
            #     o.add_mover(mover)
            o.set_scoring_function(sf)
            o.set_kt(10)
            o.optimize(remove_clashes_iter)

            # o = IMP.core.ConjugateGradients(self.model)
            # o.set_scoring_function(sf)
            # o.optimize(remove_clashes_iter)

            all_rbs = []
            for serie in self.config.series:
                for state_idx, state_mols in enumerate(serie.molecules):
                    for mol in state_mols:
                        rbs, beads = IMP.pmi.tools.get_rbs_and_beads(mol.get_hierarchy())
                        all_rbs.extend(rbs)

            cg = IMP.core.ConjugateGradients(self.model)
            cg.set_scoring_function(sf)

            for rb in all_rbs:
                rb.set_coordinates_are_optimized(False)
            cg.optimize(remove_clashes_iter)
            for rb in all_rbs:
                rb.set_coordinates_are_optimized(True)


def get_atom_of_resi(resi_p, atom_type=None):
    if IMP.atom.Residue.get_is_setup(resi_p):
        for c in IMP.atom.Residue(resi_p).get_children():
            if IMP.atom.Atom(c).get_atom_type() == atom_type:
                return c.get_particle()


def frags_overlap(frag1, frag2):
    frag1_resi = set(frag1.get_residue_indexes())
    frag2_resi = set(frag2.get_residue_indexes())

    if len(frag1_resi & frag2_resi) > 0:
        return True

    return False

def overlaps_with(sth, resi):
    if IMP.atom.Fragment.get_is_setup(sth):
        return IMP.atom.Fragment(sth).get_contains_residue(resi)
    elif IMP.atom.Residue.get_is_setup(sth):
        return sth.get_index() == resi

def get_parent_residue(h):
    p = h
    while p.get_parent():
        p = p.get_parent()
        if IMP.atom.Residue.get_is_setup(p):
            return p

def get_parent_mol(h):
    p = h
    while p.get_parent():
        p = p.get_parent()
        if IMP.atom.Molecule.get_is_setup(p):
            return p

def get_parent_state(h):
    p = h
    while p.get_parent():
        p = p.get_parent()
        if IMP.atom.State.get_is_setup(p):
            return IMP.atom.State(p)


def get_parent_chain(h):
    p = h
    while p.get_parent():
        p = p.get_parent()
        if IMP.atom.Chain.get_is_setup(p):
            return p


class XlinkScore(IMP.UnaryFunction):
    def __init__(self, d):
        IMP.UnaryFunction.__init__(self)
        self.d = d

    def evaluate(self, feature):
        out = 1.
        if feature < self.d:
            out = 0.

        return out

class LogHarmonic(IMP.UnaryFunction):
    '''
    TODO: test it better
    Allow for d = 0
    '''
    def __init__(self, d, k, base=math.e):
        IMP.UnaryFunction.__init__(self)

        if d == 0:
            raise ValueError('Target distance d cannot be 0')
        self.d = d
        self.k = k
        self.base = base

    def evaluate(self, feature):
        if feature == 0:
            feature += 0.0000001

        return self.k * (math.log( feature / self.d, self.base) ** 2)

    def evaluate_with_derivative(self, feature):
        score = self.evaluate(feature)
        if feature == 0:
            feature += 0.0000001
        
        derivative = self.k * 2 * math.log(feature / self.d, self.base) / feature

        return score, derivative

class CombinedHarmonic(IMP.UnaryFunction):
    '''
    TODO: test it better
    Use harmonic potential above the threshold
    and log harmonic below the threshold
    '''
    def __init__(self, d, k, threshold=35):
        IMP.UnaryFunction.__init__(self)
        self.d = d
        self.k = k
        self.threshold = threshold

    def evaluate(self, feature):
        if feature == 0:
            feature += 0.00001

        if feature <= self.threshold:
            # return self.k * (math.log10( feature / self.d) ** 2)
            return self.k * (math.log( feature / self.d) ** 2) # log harmonic
        else:
            return self.k * ((feature - self.d) ** 2) # harmonic

    def evaluate_with_derivative(self, feature):
        if feature == 0:
            feature += 0.00001

        score = self.evaluate(feature)
        if feature <= self.threshold:
            # derivative = self.k * 2 * math.log10(feature / self.d) / (feature * math.log(10))
            derivative = self.k * 2 * math.log(feature / self.d) / feature
            return score, derivative
        else:
            derivative = self.k * 2 * (feature - self.d)
            return score, derivative

def get_resolutions_hierarchies_for_resi(molecule, resi):
    sel = IMP.atom.Selection(molecule.get_hierarchy(), state_index=0, residue_index=resi)
    ps = sel.get_selected_particles(with_representation=False)
    if len(ps) > 0:
        raise Exception("sth went wrong")

    frag = IMP.atom.Hierarchy(ps[0]).get_parent()

    # if IMP.atom.Representation.get_is_setup(node):
    #     repr = IMP.atom.Representation(node)
    #     resolutions = repr.get_resolutions()
    #     for r in resolutions:
    #         print('---- resolution %i ----' %r)
    #         IMP.atom.show_molecular_hierarchy(repr.get_representation(r))


class ModelDensity(object):
    '''Simplified ModelDensity class based on PMI equivalent'''
    def __init__(self, voxel_size=5.0):
        self.voxel_size = voxel_size
        self.dmap = None
        self.count_models = 0.0

    def add(self, ps,
          resolution=1,
          kernel_type='GAUSSIAN'):
        '''Internal function for adding to densities.
        pass XYZR particles with mass and create a density from them.
        kernel type options are GAUSSIAN, BINARIZED_SPHERE, and SPHERE.'''
        kd = {
            'GAUSSIAN': IMP.em.GAUSSIAN,
            'BINARIZED_SPHERE': IMP.em.BINARIZED_SPHERE,
            'SPHERE': IMP.em.SPHERE}

        self.count_models += 1.0

        dmap = IMP.em.SampledDensityMap(ps, resolution, self.voxel_size)
        dmap.calcRMS()
        dmap.set_was_used(True)
        if not self.dmap:
            self.dmap = dmap
        else:
            bbox1 = IMP.em.get_bounding_box(self.dmap)
            bbox2 = IMP.em.get_bounding_box(dmap)
            bbox1 += bbox2
            dmap3 = IMP.em.create_density_map(bbox1,self.voxel_size)
            dmap3.add(dmap)
            dmap3.add(self.dmap)
            self.dmap = dmap3
            dmap3.set_was_used(True)

    def write_mrc(self, filename_prefix):
        self.dmap.multiply(1. / self.count_models)
        IMP.em.write_map(
            self.dmap,
            filename_prefix + ".mrc",
            IMP.em.MRCReaderWriter())

def get_hiers_and_restraints_from_rmf(model, frame_number, rmf_file):
    # I have to deprecate this function
    print(("getting coordinates for frame %i rmf file %s" % (frame_number, rmf_file)))

    # load the frame
    rh = RMF.open_rmf_file_read_only(rmf_file)

    try:
        prots = IMP.rmf.create_hierarchies(rh, model)
        rs = IMP.rmf.create_restraints(rh, model)
    except:
        print(("Unable to open rmf file %s" % (rmf_file)))
        prot = None
        rs = None
        return prots,rs
    try:
        IMP.rmf.load_frame(rh, RMF.FrameID(frame_number))
    except:
        print(("Unable to open frame %i of file %s" % (frame_number, rmf_file)))
        prots = None
        rs = None
        return prots,rs
    model.update()
    del rh
    return prots,rs

def get_residue_index_ranges(frag):
    '''
    Was introduced after Fragment.get_residue_index_ranges have been deprecated
    '''
    indexes = frag.get_residue_indexes()

    ranges = []
    for key, group in groupby(enumerate(indexes), lambda index_item: index_item[0] - index_item[1]):
        group = list(map(itemgetter(1), group))

        if len(group) > 1:
            ranges.append((group[0], group[-1]+1))
        else:
            ranges.append((group[0], group[0]+1))

    return ranges

def get_first_index(m, sth):
    '''
    For frags return beginning index
    For residues return index
    For atoms return index of the parent residue
    '''
    if IMP.atom.Fragment.get_is_setup(m, sth):
        return get_residue_index_ranges(IMP.atom.Fragment(sth))[0][0]
    elif IMP.atom.Residue.get_is_setup(m, sth):
        return IMP.atom.Residue(m, sth).get_index()
    elif IMP.atom.Atom.get_is_setup(m, sth):
        return IMP.atom.Residue(m, get_parent_residue(IMP.atom.Hierarchy(m, sth))).get_index()

def get_end_index(m, sth):
    '''
    For frags return end index
    (end index is EXCLUSIVE!!!, DOES NOT INCLUDE THAT RESI)
    For residues return index
    For atoms return index of the parent residue
    '''
    if IMP.atom.Fragment.get_is_setup(m, sth):
        return get_residue_index_ranges(IMP.atom.Fragment(sth))[0][1]
    elif IMP.atom.Residue.get_is_setup(m, sth):
        return IMP.atom.Residue(m, sth).get_index()
    elif IMP.atom.Atom.get_is_setup(m, sth):
        return IMP.atom.Residue(m, get_parent_residue(IMP.atom.Hierarchy(m, sth))).get_index()

def load_em_density_map(filename, voxel_size, resolution):
    """Create IMP EM map object from a file in mrc
    """
    dmap = IMP.em.read_map(filename, IMP.em.MRCReaderWriter())
    dmap.update_voxel_size(voxel_size)
    dmap.get_header().set_resolution(resolution)

    return dmap
