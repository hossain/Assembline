import os, subprocess
from distutils.core import setup
from distutils.command.install import install



setup(
    name='imp_utils1',
    version='0.0.2dev',
    author='Jan Kosinski',
    author_email='jan.kosinski@embl.de',
    packages=['imp_utils1'
              ],
    scripts=[
        # 'scripts/make_loc_densities.py',
        # 'scripts/replace_fits.py',
        # 'scripts/score_em2d.py',
        # 'scripts/sort_by_score.py',
        # 'scripts/draw_score_histogram.py',
        # 'scripts/make_dist_matrix.py',
        # 'scripts/make_dist_matrix_rmsd_from_rmfs.py',
        # 'scripts/print_mover_stats.py',
        # 'scripts/test_run_many2.py',
        # 'scripts/extract_scores_from_RMF_traj.py',
        'scripts/setup_recombination.py',
        # 'scripts/compare_models.py',
        'scripts/gen_refinement_template.py',
        'scripts/setup_analysis.py',
        'scripts/setup_refine.py',
        'scripts/extract_scores.py',
        'scripts/rebuild_atomic.py',
        'scripts/optimize.py',
        'scripts/assembline.py',
        'scripts/run_many.py',
        'scripts/merge_jsons.py',
        'scripts/create_density_file.py',
        'scripts/extract_cluster_models.py',
        'scripts/create_symm_groups_file.py',
        'scripts/copy_sampcon_gnuplot_scripts.py',
        'scripts/rmf_cat.py',

        # 'scripts/analyze.R',
        'scripts/plot_scores.R',
        'scripts/plot_convergence.R',

             ],
    url='none',
    license='LICENSE.txt',
    description='Interface to IMP.',
    # long_description=open('README').read(),
)
