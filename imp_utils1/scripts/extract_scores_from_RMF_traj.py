import sys
from collections import defaultdict
import csv

import RMF
import IMP
import IMP.rmf

rmf_file = sys.argv[1]
rh = RMF.open_rmf_file_read_only(rmf_file)

scoreFactory = RMF.ScoreConstFactory(rh)

model = IMP.Model()


prots = IMP.rmf.create_hierarchies(rh, model)
rs = IMP.rmf.create_restraints(rh, model)



root_node = rh.get_root_node()
features = []
IMP.rmf.load_frame(rh, RMF.FrameID(1))
for c in root_node.get_children():
    if c.get_type() == RMF.FEATURE:
        features.append(c)

scores = defaultdict(list)
frameCount = rh.get_number_of_frames()
for frame in range(frameCount):
    IMP.rmf.load_frame(rh, RMF.FrameID(frame))

    for i, f in enumerate(features):
        if f is None:
            sys.exit()
        # if not scoreFactory.get_is(f):
        #         continue
        score = scoreFactory.get(f)
        try:
            s = score.get_frame_score()
        except IOError:
            s = 0.0

        scores[f.get_name()].append(s)

header = ','.join(scores.keys())
header = header.replace('"','').replace(' ','_')
lines = [header]
for line in list(zip(*list(scores.values()))):
    lines.append(','.join(map(str, line)))

print('\n'.join(lines))