#!usr/bin/python

import os
import sys
import re
import string
import subprocess
from optparse import OptionParser
import random
from multiprocessing import Process, Queue, Manager
import csv
import glob

def get_scores(fn):
    scores = {}
    with open(fn) as f:
        csv_reader = csv.DictReader(f)
        #store in a list all lines of score file

        #change keys from model ids to paths
        for row in csv_reader:
            if not os.path.split(row['model'])[0]: #is not path but model id (for refinement multi it is a path)
                key = os.path.join('models_txt',row['model'] + 'model.txt')
            else:
                key = row['model']
            scores[key] = float(row['total_score'])

    return scores

def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out


def main():
    usage = "usage: %prog [options] identities_file1 identities_file2 cluster_all_file json_file"
    parser = OptionParser(usage=usage)

    parser.add_option("--nrand", dest="n_random",
                      help="Number of random models from the cluster", type="int", default=0)

    parser.add_option("--ntop", dest="n_top",
                      help="Number of top scoring models from the cluster", type="int", default=0)

    parser.add_option("-o", "--outdir", dest="outdir",
                      help="Outdir for models", default='./')

    parser.add_option("--scores", dest="scores_file",
                      help="Score file", default=None)

    parser.add_option("--subunit", dest="selected_subunit",
                      help="Only extract a selected subunit")

    parser.add_option("--copies", dest="copies", type="string", default=None, 
                      help="Which copies of each series to save, comma separated, no space, e.g. 0,1,7")

    parser.add_option("--resi_range", dest="resi_range",
                      help="Only extract a selected subunit:", default=None)

    parser.add_option("--n_procs", dest="n_procs",
                      help="Number of parallel processes", default=None)

    parser.add_option("--project_dir", dest="project_dir",
                      help="Replace basename dir for data with this dir", metavar="DIR")

    parser.add_option("--rebuild_loops", dest="rebuild_loops", action="store_true",
                      help="Rebuild full atomic loops (to use only when modeling was run including resolution 1 or Calpha). For many long loops it can take a long time!", default=False)

    (options, args) = parser.parse_args()

    if options.n_top:
        if not options.scores_file:
            print('You must provide --scores option if you use --ntop')
            sys.exit()

        scores = get_scores(options.scores_file)

    master_dir = os.path.realpath(args[0])

    identities = {}
    for fn in [args[0], args[1]]:
        with open(fn) as f:
            for line in f:
                fn, idx = line.strip().split()
                identities[int(idx)] = fn

    with open(args[2]) as f:
        cluster_ids = map(int,f.read().splitlines())

    models = []
    if options.n_random:
        sample = random.sample(list(cluster_ids), options.n_random)
        for idx in sample:
            models.append(identities[idx])
    elif options.n_top:
        all_models = [identities[idx] for idx in cluster_ids]

        model_ids = {}
        for m in all_models:
            model_txt_path = os.path.realpath(os.path.join(os.path.dirname(master_dir), m)).replace('model.rmf3', '').replace('model.rmf', '').replace('models_rmf', 'models_txt')+'model.txt'
            prefix = os.path.commonpath([model_txt_path, os.path.realpath(options.scores_file)])
            prefix = os.path.join(prefix, '') # add trailing slash if not yet there, cross-platform
            model_ids[m] = model_txt_path.replace(prefix, '')

        all_models_sorted = sorted(all_models, key=lambda x: scores[model_ids[x]])
        models = all_models_sorted[:options.n_top]

    else:
        models = [identities[idx] for idx in cluster_ids]

    guess_jsons = False
    if len(args) == 4:
        json_file = os.path.abspath(args[3])
    else:
        guess_jsons = True #will be guessing configs based on model paths
        json_file = None

    cmds = []
    for model in models:
        outdir, model = os.path.realpath(os.path.join(os.path.dirname(master_dir), model)).split('/models_rmf/')
        model_idx = model.replace('model.rmf','')
        extra = ''
        if options.selected_subunit:
            extra += '--subunit {0} '.format(options.selected_subunit)
        if options.copies:
            extra += '--copies {0} '.format(options.copies)
        if options.resi_range:
            extra += '--resi_range {0} '.format(options.resi_range)
        if options.project_dir:
            extra += '--project_dir {0} '.format(os.path.realpath(options.project_dir))
        if options.rebuild_loops:
            extra += '--rmf_auto '

        scripts_path = os.path.dirname(os.path.abspath(__file__))
        if guess_jsons:
            #get paths to all jsons in case of multistate modeling
            path_str = outdir + '/*.json'
            json_file = glob.glob(path_str)[0]

        cmd = 'python {4}/rebuild_atomic.py {3} -o {2}/{1}.cif {0} models_txt/{1}model.txt'.format(json_file, model_idx, os.path.realpath(options.outdir), extra, scripts_path)
        
        cmds.append((cmd, outdir))

    if not options.n_procs:
        run_cmds(cmds)
    else:
        chunks = chunkIt(cmds, options.n_procs)
        processes = []
        for i, chunk in enumerate(chunks):
            proc = Process(target=run_cmds, args=(chunk,))
            processes.append(proc)
            proc.start()

        for p in processes:
            p.join() 

def run_cmds(cmds):
    for cmd, cwd in cmds:
        print(cmd, cwd)
        subprocess.call(cmd,shell=True, cwd=cwd)

if __name__ == '__main__':
    main()