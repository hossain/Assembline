import subprocess
import glob
import os
import ntpath
import sys
from optparse import OptionParser
import time
from subprocess import Popen, PIPE
from subprocess import check_output
import re
from string import Template
import imp

usage = "usage: %prog [options] project.json params.py outdir start max_no_runs"
parser = OptionParser(usage=usage)

parser.add_option("--prefix", type="str", dest="prefix", default='',
                  help="prefix, ({0}), [default: %default]")

parser.add_option("--daemon", action="store_true", dest="daemon", default=False,
                  help="start a daemon that re-submits jobs? [default: %default]")

parser.add_option("--max_concurrent_jobs", type="int", dest="max_concurrent_jobs", default=10000,
                  help="Maximum number of jobs to run concurrently, ({0}), [default: %default]")

parser.add_option("--min_concurrent_jobs", type="int", dest="min_concurrent_jobs", default=0,
                  help="Minimum number of jobs to run concurrently, ({0}), [default: %default]")

(options, args) = parser.parse_args()

def get_free():
    output = check_output(['/etc/profile.d/clusterstatus.sh'])

    m = re.search('used: (?P<used>\d+) out of (?P<available>\d+) available cores', str(output))
    if m:
        free = int(m.group('available')) - int(m.group('used'))
        return free
    else:
        return 0

def get_my_jobs_number():
    output = check_output(['squeue', '-u', 'kosinski'])
    return len(output.splitlines()) - 1

def get_no_jobs_to_run():
    free = get_free()
    my_jobs_number = get_my_jobs_number()
    my_free = max(0, options.max_concurrent_jobs - my_jobs_number)
    to_run = max(free, options.min_concurrent_jobs - my_jobs_number)

    return min(to_run, my_free)


print('get_free()', get_free())
print('get_my_jobs_number()', get_my_jobs_number())
print('get_no_jobs_to_run()', get_no_jobs_to_run())
