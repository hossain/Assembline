#!/usr/bin/python
from optparse import OptionParser
import os
from collections import defaultdict

import superconfig

def main():
    usage = "usage: %prog [options] cfg_filename solution_filename"
    parser = OptionParser(usage=usage)

    def list_split_callback(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))

    parser.add_option("--pdb_dir", dest="pdb_dir",
                      help="Replace basename dir for pdb files with this pdb_dir", metavar="DIR")

    parser.add_option("--project_dir", dest="project_dir",
                      help="Replace basename dir for data with this dir", metavar="DIR")

    parser.add_option("-o", dest="outfilename", default='density.txt',
                      help="Custom outfilename, density.txt")

    parser.add_option('--other_states',
                      type='string',
                      action='callback',
                      callback=list_split_callback,
                      default=[])

    # parser.add_option("--subunit", dest="selected_subunit",
    #                   help="Only extract a selected subunit", default=False)

    # parser.add_option("--serie", dest="selected_serie",
    #                   help="Only extract a selected serie", default=False)

    # parser.add_option("--resi_range", dest="resi_range",
    #                   help="Only extract a selected subunit:", default=None)

    # parser.add_option("--add_series", dest="add_series", action="store_true",
    #                   help="Add series to names in multi_pdb and multi_cif formats", default=False)

    parser.add_option("--by_subunit", dest="by_subunit", action="store_true",
                      help="Densities by subunits", default=True)

    parser.add_option("--by_rigid_body", dest="by_rigid_body", action="store_true",
                      help="Densities by by rigid body", default=False)

    (options, args) = parser.parse_args()


    cfg_filenames = [args[0]] + options.other_states
    config = superconfig.Config(cfg_filenames, project_dir=options.project_dir, do_read_data=True)

    if options.pdb_dir:
        config.change_pdb_dir(options.pdb_dir)

    if len(config.rigid_bodies) == 0:
        config.define_rbs_from_pdbs()

    densities_for_each_state = defaultdict(dict)

    # densities = {}

    if options.by_rigid_body:
        for rb_cfg in config.rigid_bodies:
            print('1', [(k, v) if k != 'positions' else (k, len(v)) for k, v in rb_cfg.items() ])
            print(rb_cfg['name'])
            for comp in rb_cfg['components']:
                resi_ranges = superconfig.ranges_from_list(config.get_resi_ids_from_comp_cfg(comp))
                # densities[rb_cfg['name']] = [(start, end, comp['subunit']) for start, end in resi_ranges]
                if 'state' in comp:
                    state = comp['state']
                else:
                    state = 0

                if 'copies' in comp:
                    if rb_cfg['name'] not in densities_for_each_state[state]:
                        densities_for_each_state[state][rb_cfg['name']] = []
                    for copy_idx in comp['copies']:
                        densities_for_each_state[state][rb_cfg['name']].extend([(start, end, f"{comp['subunit']}.{copy_idx}") for start, end in resi_ranges])
                else:
                    densities_for_each_state[state][rb_cfg['name']] = [(start, end, comp['subunit']) for start, end in resi_ranges]
    else:
        for subunit in config.subunits:
            sequence = config.get_seq(subunit.name)
            start = 1
            end = len(sequence) + 1
            # densities[subunit.name] = [(start, end, subunit.name)]
            for state in range(0, len(cfg_filenames)):
                densities_for_each_state[state][subunit.name] = [(start, end, subunit.name)]

    if len(densities_for_each_state) == 1:
        out_str = 'density_custom_ranges=' + str(list(densities_for_each_state.values())[0])

        with open(options.outfilename, 'w') as f:
            f.write(out_str)

    else:
        for state_id, densities in densities_for_each_state.items():
            out_str = 'density_custom_ranges=' + str(densities)

            with open(options.outfilename.replace('.txt', '_state'+str(state_id)+'.txt'), 'w') as f:
                f.write(out_str)


if __name__ == '__main__':
    main()