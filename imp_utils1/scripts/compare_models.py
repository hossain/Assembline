from optparse import OptionParser
import os
import json
import subprocess
from collections import defaultdict
from itertools import product
import numpy
from operator import itemgetter
import math

import superconfig
from pdb_utils import transform_pdb
from pdb_utils import transformations

def convert(input):
    if isinstance(input, dict):
        return dict([(convert(key), convert(value)) for key, value in input.items()])
    elif isinstance(input, list):
        return [convert(element) for element in input]
    elif isinstance(input, str):
        return input.encode('utf-8')
    else:
        return input

def iter_solutions(solution_filename):
    with open(solution_filename, 'rU') as f:
        for line in f:
            try:
                yield json.loads(line.strip())
            except ValueError:
                print('Bad combination line:')
                print(line.strip())

def transpose(rot):
    return list(zip(*rot))

def get_tr3ds_sets(solution_filename):
    sol_i = 0
    for sol in iter_solutions(solution_filename):
        print('sol', sol)
        tr3ds_sets = defaultdict(list)

        for rb_name, fit in sol.items():
            print('rb_name, fit', rb_name, fit)
            fit = fit[0]
            name = rb_name.rsplit('_',1)[0]
            # print name
            tr3d = []
            # print fit['rot'], fit['trans']
            # fit['trans'] = [t/100 for t in fit['trans']]
            for i, row in enumerate(fit['rot']):
                tr3d.append(row)
                tr3d[-1].append(fit['trans'][i])

            tr3ds_sets[name].append(tr3d)

    sol_i = sol_i + 1

    return tr3ds_sets

def get_mean(deviations):
    rb_devs = []
    for key, rb_dev in deviations.items():
        rb_devs.extend(rb_dev)

    return numpy.mean(rb_devs)

def get_median(deviations):
    rb_devs = []
    for key, rb_dev in deviations.items():
        rb_devs.extend(rb_dev)

    return numpy.median(rb_devs)

def get_rmsd(deviations):
    rb_devs = []
    for key, rb_dev in deviations.items():
        rb_devs.extend(rb_dev)

    sqr_devs = [dev*dev for dev in rb_devs]
    return math.sqrt(sum(sqr_devs) / len(sqr_devs))


def compare(solution_filename1, solution_filename2):
    tr3ds_sets1 = get_tr3ds_sets(solution_filename1)
    tr3ds_sets2 = get_tr3ds_sets(solution_filename2)

    deviations = {}
    for k in tr3ds_sets1:
        # print k
        set1 = tr3ds_sets1[k]
        set2 = tr3ds_sets2[k]

        # print(len(set1))
        # print(len(set1))

        pairs = product(set1, set2)
        with_scores = []
        for pair in pairs:
            # dist = transformations.vector_norm(numpy.array(pair[0])-numpy.array(pair[1]))[0]
            dist = numpy.linalg.norm(numpy.array(pair[0])-numpy.array(pair[1]))
            with_scores.append([pair[0], pair[1], dist])


        used = []
        this_deviations = []
        with_scores.sort(key=itemgetter(2))
        for match in with_scores:
            m1 = match[0]
            m2 = match[1]

            m1_used = False
            m2_used = False
            for prev in used:
                if m1 is prev:
                    m1_used = True
                if m2 is prev:
                    m2_used = True

            if not m1_used and not m2_used:
                this_deviations.append(match[2])
                # print match[2]
                used.append(m1)
                used.append(m2)


        deviations[k] = this_deviations

    return get_mean(deviations)
    # return get_median(deviations)
    # return get_rmsd(deviations)

def main():
    usage = "usage: %prog [options] cfg_filename solution_filename1 solution_filename2"
    parser = OptionParser(usage=usage)
    
    parser.add_option("-o", "--outfile", dest="outfile", default=None,
                      help="optional FILE with output", metavar="FILE")

    parser.add_option("--pdb_dir", dest="pdb_dir",
                      help="Replace basename dir for pdb files with this pdb_dir", metavar="DIR")

    parser.add_option("--list", action="store_true", dest="against_list", default=False,
                      help="second argument is filelist? [default: %default]")

    (options, args) = parser.parse_args()

    cfg_filename = args[0]
    solution_filename1 = args[1]
    solution_filename2 = args[2]

    config = superconfig.Config(cfg_filename)

    if options.pdb_dir:
        config.change_pdb_dir(options.pdb_dir)

    out = [['dist','model']]
    if not options.against_list:
        dist = compare(solution_filename1, solution_filename2)
        if not options.outfile:
            print(dist, solution_filename2)
        else:
            out.append([dist, solution_filename2])
    else:
        with open(solution_filename2) as f:
            for line in f:
                solution_filename2_file = line.strip()
                dist = compare(solution_filename1, solution_filename2_file)
                if not options.outfile:
                    print(dist, solution_filename2_file)
                else:
                    out.append([dist, solution_filename2_file])               

    if options.outfile:
        with open(options.outfile, 'w') as f:
            f.write('\n'.join([','.join(map(str,fields)) for fields in out]))
    else:
        print('\n'.join([','.join(map(str,fields)) for fields in out]))

if __name__ == '__main__':
    main()