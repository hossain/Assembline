#!/usr/bin/python
import sys
import os
import subprocess
import json
import shutil
from itertools import groupby
from optparse import OptionParser
import tempfile
import copy
import glob
import re
from pprint import pprint
import csv
from multiprocessing import Process, Queue, Manager

from superconfig import minify_json, read_json
import imp_utils1

TEST = False

def get_model_filename(model_id):
    fn = os.path.join('models_txt', model_id+'model.txt')

    return fn

def gen_model(model_id, previous_outdir, json_project_fn, outdir, add_series=False, copies=None):
    model_txt_fn = os.path.join(previous_outdir, get_model_filename(model_id))
    if TEST:
        return outdir

    scripts_path = os.path.dirname(os.path.abspath(__file__))
    options = ''
    if add_series:
        options = '--add_series'
    if copies is not None:
        options += ' --copies '+copies
    cmd = 'python {3}/rebuild_atomic.py {4} --format multi_pdb --outdir {2} {0} {1}'.format(json_project_fn, model_txt_fn, outdir, scripts_path, options)
    print(cmd)
    subprocess.call(cmd, shell=True)
    if not os.path.exists(outdir):
        raise FileNotFoundError

    return outdir

def create_refine_json(json_project_fn, outfilename, new_pdb_dir=None):
    new_pdb_dir = os.path.abspath(new_pdb_dir)
    in_cfg = read_json(json_project_fn)
    for key, item in in_cfg.items():
        if key == 'data':
            for data_item in item:
                if data_item['type'] == 'pdb_files':
                    for pdb_spec in data_item['data']:
                        for c in pdb_spec['components']:
                            if new_pdb_dir is not None:
                                # c['filename'] = c['filename'].replace('{INPUT_STRUCT_PATH}', new_pdb_dir+'/')
                                head, filename = os.path.split(c['filename']) #just in case, this allows to use existing refine jsons as templates
                                c['filename'] = os.path.join(new_pdb_dir, filename)
                                print(c['filename'])
                                if not os.path.exists(c['filename']):
                                    print('WARNING: {0} does not exist'.format(c['filename']))

    with open(outfilename, 'w') as json_file:
        json.dump(in_cfg, json_file, indent=4)

    return outfilename

def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out



if __name__ == '__main__':
    usage = '''Usage: python %prog [options]
        '''.format(sys.argv[0])
    parser = OptionParser(usage=usage)

    parser.add_option("--dir", dest="dir", default='./',
                      help="directory with the output with models to refine [default: %default]")

    parser.add_option("--top", dest="top", default=None, type="int",
                      help="How many models from the provided list [default: all]")

    parser.add_option('--score_thresh', dest="score_thresh", help='Refine models only with score better than this score threshold', default=None, type=float)

    parser.add_option('--score', dest="select_score", help='Specify score name for score selection of models to extract and analyze as displayed in all_scores_uniq.csv column header', default="total_score")

    parser.add_option("--model", dest="model_id", default=None,
                      help="ID of the model to refine [default: %default]")

    parser.add_option("--scores", dest="scores_file", default=None,
                      help="The CSV file with model scores as returned from extract_scores.py [default: %default]")

    parser.add_option("--previous_json", dest="previous_json", default=None,
                      help="JSON file from the previous run, e.g. of the combinations stage [default: %default]")

    parser.add_option("--refine_json_template", dest="refine_json_template", default=None,
                      help="Template for the refinement JSON [default: %default]")

    parser.add_option("--refine_json_outname", dest="refine_json_outname", default=None,
                      help="Name for the refinement JSON files that will be created based on the template [default: %default]")

    parser.add_option("--previous_outdir", dest="previous_outdir", default=None,
                      help="Directory of the previous run [default: %default]")

    parser.add_option("--refine_outdir", dest="refine_outdir", default=None,
                      help="Directory of the refinement run [default: %default]")

    parser.add_option("--add_series", dest="add_series", action="store_true",
                      help="Add series to names in multi_pdb and multi_cif formats", default=False)

    parser.add_option("--copies", dest="copies", type="string", default=None, 
                      help="Which copies of each series to save, comma separated, no space, e.g. 0,1,7")

    parser.add_option("--n_procs", dest="n_procs", default=1, type="int",
                      help="How many processors [default: %default]")

    (options, args) = parser.parse_args()

    if len(args) > 0:
        print('This script usage has changed!')
        print()
        parser.print_usage()
        exit()

    if options.scores_file:
        with open(options.scores_file) as f:
            csv_reader = csv.DictReader(f)
            score_file_data = list(csv_reader)

            if options.top:
                score_file_data = sorted(score_file_data, key=lambda d: float(d['total_score']))[:options.top]

            if options.score_thresh is not None:
                score_file_data.sort(key=lambda x: float(x[options.select_score]))
                score_file_data = list(filter(lambda x: float(x[options.select_score]) <= options.score_thresh, score_file_data))

                for x in score_file_data[:10]:
                    print(x['model'], x[options.select_score])

                print('\nNumber of models below the specified score threshold of {0}: {1}\n'.format(options.score_thresh, len(score_file_data)))

            model_ids = [row['model'] for row in score_file_data]

    elif options.model:
        model_ids = [options.model]

    json_project_fn = options.previous_json
    json_template_for_refine_fn = options.refine_json_template
    refine_json_outname = options.refine_json_outname
    previous_outdir = options.previous_outdir
    refine_outdir = options.refine_outdir

    in_cfg = read_json(json_template_for_refine_fn)

    os.makedirs(refine_outdir, exist_ok=True)

    def run_cmds(model_ids):
        for model_id in model_ids:
            refine_modelid_dir = os.path.join(refine_outdir, model_id)
            refine_in_pdbs_dir = os.path.join(refine_modelid_dir, 'in_pdbs')
            os.makedirs(refine_modelid_dir, exist_ok=True)

            gen_model(model_id, previous_outdir, json_project_fn, refine_in_pdbs_dir, options.add_series, options.copies)

            refine_json_fn = create_refine_json(json_template_for_refine_fn, 

                                                os.path.join(refine_modelid_dir, os.path.basename(refine_json_outname)),
                                                refine_in_pdbs_dir)

    if  options.n_procs == 1:


        # for model_id in model_ids:
        #     refine_modelid_dir = os.path.join(refine_outdir, model_id)
        #     refine_in_pdbs_dir = os.path.join(refine_modelid_dir, 'in_pdbs')
        #     os.makedirs(refine_modelid_dir, exist_ok=True)

        #     gen_model(model_id, previous_outdir, json_project_fn, refine_in_pdbs_dir, options.add_series, options.copies)

        #     refine_json_fn = create_refine_json(json_template_for_refine_fn, 

        #                                         os.path.join(refine_modelid_dir, os.path.basename(refine_json_outname)),
        #                                         refine_in_pdbs_dir)

        run_cmds(model_ids)
    else:
        chunks = chunkIt(model_ids, options.n_procs)
        processes = []
        for i, chunk in enumerate(chunks):
            proc = Process(target=run_cmds, args=(chunk,))
            processes.append(proc)
            proc.start()

        for p in processes:
            p.join() 




