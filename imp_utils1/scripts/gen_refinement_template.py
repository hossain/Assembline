#!/usr/bin/python
from optparse import OptionParser
import os
import json
import subprocess
from collections import defaultdict
import itertools
import io
import copy
import tempfile
import shutil
import sys
from pprint import pprint


from imp_utils1 import params_reader

import superconfig

def main():
    usage = "usage: %prog [options] cfg_filename"
    parser = OptionParser(usage=usage)

    def list_split_callback(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))

    parser.add_option("-o", dest="outfilename",
                      help="Custom outfilename, otherwise $MODEL.cif")


    parser.add_option('--other_states',
                      type='string',
                      action='callback',
                      callback=list_split_callback,
                      default=[])

    parser.add_option("--out_json", dest="out_json",
                      help="Output json, e.g. for refinement")

    parser.add_option("--params", dest="params_file",
                      help="Params file")

    parser.add_option("--add_series", dest="add_series", action="store_true",
                      help="Add series to names in multi_pdb and multi_cif formats", default=False)

    (options, args) = parser.parse_args()

    cfg_filenames = [args[0]] + options.other_states

    config = superconfig.Config(cfg_filenames)

    out_jsons = []
    for cfg_filename in cfg_filenames:
        out_jsons.append(superconfig.read_json(cfg_filename))
        for data_item in out_jsons[-1]['data']:
            if data_item['type'] == superconfig.PDB_FILES_DATA_TYPE:
                data_item['data'] == []

    # out_json_filenames = options.out_json.split(',')

    params = params_reader.read_params(options.params_file)
    if params.add_symmetry_constraints:
        config.add_auto_symmetries(use_constraints=True)
    if params.add_symmetry_restraints:
        config.add_auto_symmetries(use_constraints=False)

    #this should be done within config based on 
    #a parameter in the config itself?
    if len(config.rigid_bodies) == 0:
        config.define_rbs_from_pdbs()

    out_pdb_files_rb_config_constrained_groups = []

    for rb_cfg in config.rigid_bodies:
        # print('1', [(k, v) if k != 'positions' else (k, len(v)) for k, v in rb_cfg.items() ])

        if options.add_series:
            series = []
            for rb_comp in rb_cfg['components']:
                if rb_comp['serie'] not in series:
                    series.append(rb_comp['serie'])
            split_items = rb_cfg['name'].rsplit('_', maxsplit=1)
            name = '{base}_{series}_{idx}.pdb'.format(base=split_items[0], series='_'.join(series), idx=split_items[1])
        else:
            name = rb_cfg['name'] + '.pdb'

        temp_rb_cfg = copy.deepcopy(rb_cfg)

        # if params.add_rbs_from_pdbs:
        #     for comp in temp_rb_cfg['components']:
        #         comp.pop('chain_id', None) #because, confusion! there chain_id is used to specify chain id in the

        # print(config.is_symmetry_constrained(temp_rb_cfg))
        # pprint([(k, v) if k not in ('positions', 'pdblines') else (k, len(v)) for k, v in temp_rb_cfg.items() ])
        # print('1', [(k, v) if k not in ('positions', 'pdblines') else (k, len(v)) for k, v in rb_cfg.items() ])
        out_rb_cfg = copy.deepcopy(rb_cfg)
        remove_keys = ['positions', 'positions_type', 'max_positions', 'positions_score', 'pdblines', 'name']
        for key in remove_keys:
            out_rb_cfg.pop(key, None)
        for comp in out_rb_cfg['components']:
            # comp['filename'] = outpdb_path
            comp['new_filename'] = name

        added = False
        if config.is_symmetry_constrained(rb_cfg):
            for prev_rb_cfg_group in out_pdb_files_rb_config_constrained_groups:
                for prev_rb_cfg in prev_rb_cfg_group:
                    if len(prev_rb_cfg['components']) == len(out_rb_cfg['components']):
                        are_components_equal = []
                        for comp, prev_comp in zip(out_rb_cfg['components'], prev_rb_cfg['components']):
                            # are_components_equal.append(superconfig.are_selectors_equal(comp, prev_comp, fields=('subunit', 'state', 'states', 'domain', 'serie', 'resi_ranges')))
                            are_components_equal.append(superconfig.are_selectors_equal(comp, prev_comp, fields=('subunit', 'state', 'states', 'domain', 'filename', 'serie', 'resi_ranges')))
                            # are_components_equal.append(config.is_selector_subset_of_another(comp, prev_comp))
                        if all(are_components_equal):
                            # pprint(prev_rb_cfg)
                            # pprint(out_rb_cfg)
                            # print('-----------')
                            prev_rb_cfg_group.append(out_rb_cfg)
                            added = True
                            break
        if not added:
            out_pdb_files_rb_config_constrained_groups.append([out_rb_cfg])

    for group in out_pdb_files_rb_config_constrained_groups:
        for rb_cfg in group:
            for comp in rb_cfg['components']:
                comp['filename'] = comp['new_filename']
                comp.pop('new_filename', None)

    out_pdb_files_rb_config = []
    for group in out_pdb_files_rb_config_constrained_groups:
        if len(group) > 1:
            group = sorted(group, key=lambda x: sum( [ sum(c['copies']) for c in x['components'] ] ))
            #nice usual case
            copies = [c['copies'] for c in group[0]['components']]
            if copies.count(copies[0]) == len(copies): # i.e. all copies are equal
                group[0]['foreach_copy'] = True
                for comp in group[0]['components']:
                    comp.pop('copies')
                out_pdb_files_rb_config.append(group[0])
            #all other weird cases
            else:
                out_pdb_files_rb_config.extend(group)
        else:
            rb_cfg = group[0]
            #check if monomeric series and copies can be removed
            for rb_comp in rb_cfg['components']:
                serie = config.get_serie_by_subunit_and_name(rb_comp['subunit'], rb_comp.get('serie')) or config.get_series_by_subunit(config.get_subunit_by_name(rb_comp['subunit']))[0]
                if serie.cell_count == 1:
                    rb_comp.pop('copies')
            out_pdb_files_rb_config.append(rb_cfg)

    state_idx = group[0]['components'][0]['states'][0]
    out_cfg = superconfig.read_json(cfg_filenames[state_idx])
    out_cfg.pop('rigid_bodies', None)

    for key, item in out_cfg.items():
        if key == 'data':
            for data_item in item:
                if data_item['type'] == superconfig.PDB_FILES_DATA_TYPE:
                    data_item['data'] = out_pdb_files_rb_config

    with open(options.out_json, 'w') as json_file:
        json.dump(out_cfg, json_file, indent=4)


if __name__ == '__main__':
    main()