import sys

import IMP
import IMP.atom
import IMP.pmi.restraints.em2d

pdb_filename = sys.argv[1]

model = IMP.Model()
hier = IMP.atom.read_pdb(pdb_filename, model, IMP.atom.CAlphaPDBSelector())

simpl_hier =  IMP.atom.create_simplified_along_backbone(hier, 10)
# images = ['/g/scb/cmueller/kosinski/elongator/elongator_protocol/elongator_modeling/data/2D/hexamer/holocomplex182elp2gfp_22A224.spi']
images = ['/g/scb/cmueller/kosinski/elongator/elongator_protocol/elongator_modeling/data/2D/hexamer/holocomplex182elp2gfp_22A224_clip_fc.spi']
# images = ['/g/scb/cmueller/kosinski/elongator/elongator_protocol/elongator_modeling/data/2D/hexamer/holocomplex182elp2gfp_22A224_clip_fcLow.spi']
restr = IMP.pmi.restraints.em2d.ElectronMicroscopy2D_FFT(
    hier=simpl_hier,
    images=images,
     # pixel_size=2.2, #original
    # pixel_size=4.4, #fc_clip
    pixel_size=2.83, #clip_fc
    # pixel_size=3.85, #clip_fcLow
    image_resolution=20,
    projection_number=20,
    resolution=10
    ).get_restraint()

print(restr.evaluate(False))