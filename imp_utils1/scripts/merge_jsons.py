#!/usr/bin/python

import sys
import json

import superconfig

fns = sys.argv[1:]

out = superconfig.merge_jsons(fns, multistate=False)


print(json.dumps(out, indent=4))
