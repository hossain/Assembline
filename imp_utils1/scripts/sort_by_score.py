import operator
import glob

files = glob.glob('scores/*scores.txt')


scores = {}
for fn in files:
    with open(fn) as f:

        f_x_scores = []
        for line in f:
            if line.startswith('score'):
                total_score = float(line.split()[1])
            if '-x-' in line:
                items = line.split(',')
                f_x_scores.append(float(items[1]))
        scores[fn] = (total_score, sum(f_x_scores))

sorted_by_total = sorted(list(scores.items()), key=operator.itemgetter(0))


sorted_scores = sorted(sorted_by_total, key=lambda s: s[1][0])
for fn, x_score in sorted_by_total:
    print(fn, x_score)




