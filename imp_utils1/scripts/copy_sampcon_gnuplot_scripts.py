#!/usr/bin/python
import os
import glob
import shutil

import IMP.sampcon

gnuplotdir = IMP.sampcon.get_data_path("gnuplot_scripts")

for filename in sorted(glob.glob(os.path.join(gnuplotdir, "*.plt"))):
    shutil.copy(filename, os.getcwd())
