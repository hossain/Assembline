import os, subprocess
from distutils.core import setup
from distutils.command.install import install



setup(
    name='SuperConfig',
    version='0.0.1dev',
    author='Jan Kosinski',
    author_email='jan.kosinski@embl.de',
    packages=['superconfig'
              ],
    url='none',
    license='LICENSE.txt',
    description='Modeling-Software-agnostic class to represent a biological system.',
    # long_description=open('README').read(),
)
