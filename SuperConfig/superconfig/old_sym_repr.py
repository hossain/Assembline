from collections import defaultdict
import random
import copy

import IMP
import IMP.em
import imp_utils


CONNECTIVITY_DATA_TYPE = 'connectivity'


class Representation(imp_utils.MultiRepresentation):
    """
    Should be generic, better leading with symmetry
    """
    def __init__(
            self,
            cfg_filename,
            ini_fits=None):
        imp_utils.MultiRepresentation.__init__(self)
        self.cfg = imp_utils.Config(cfg_filename)

        self.sym_particle_lists = defaultdict(list)

        self.symtr3ds = defaultdict(list)
        self.particle_symtr3ds = {}
        self.chain_generators = {}

        self.particle_classes = self.cfg.particle_classes
        self.particle_base_names_to_names = defaultdict(list)
        self.xlink_restraints = []
        self.sym_restraints = []

        self.cfg.read_fits()

    def get_base_names_from_particle_name(self, particle_name):
        for particle_base_name, particle_names in self.particle_base_names_to_names.items():
            if particle_name in particle_names:
                return particle_base_name

    def get_rigid_bodies(self):
        rbs = []
        for component_name, rb_for_comp in self.rigid_bodies.items():
            rbs.extend(rb_for_comp)

        return rbs

    def iter_movable_rb_sets(self):
        for component_name, subseries in self.sym_particle_lists.items():
            for subserie in subseries:
                sym_particle_names = subserie['particle_names']
                do_not_move = False
                for p in sym_particle_names:
                    if p in self.cfg.frozen_particles:
                        do_not_move = True
                        break
                if do_not_move:
                    continue

                sym_sets = []
                for sym_particle_name in sym_particle_names:
                    rbs = self.rigid_bodies[sym_particle_name]
                    sym_sets.append(rbs)

                all_sets = list(zip(*sym_sets))
                for rb_set in all_sets:
                    yield component_name, sym_particle_names, rb_set, subserie['tr3ds']

    def randomize(self):
        center = self.em_map.get_centroid()
        bbox = IMP.algebra.BoundingBox3D(
            center,
            center + IMP.algebra.Vector3D(300, 300, 300)
        )

        for name, sym_particle_names, rb_set, sym_tr3ds in self.iter_movable_rb_sets():
            random_tr3d = IMP.algebra.Transformation3D(
                IMP.algebra.get_random_rotation_3d(),
                IMP.algebra.get_random_vector_in(bbox)
            )

            rb_ref = rb_set[0]
            IMP.core.transform(rb_ref, random_tr3d)
            proposed_t3d = rb_ref.get_reference_frame().get_transformation_to()
            for rb, sym_tr3d in zip(rb_set[1:], sym_tr3ds[1:]):
                rb.set_reference_frame(IMP.algebra.ReferenceFrame3D(sym_tr3d * proposed_t3d))

    def store_input_pdb_reference_frames(self):
        self.input_pdb_reference_frames = defaultdict(list)
        for name, sym_particle_names, rb_set, sym_tr3ds in self.iter_movable_rb_sets():
            if rb_set[0] in self.rbs_from_pdbs[sym_particle_names[0]]:
                self.input_pdb_reference_frames[name].append(rb_set[0].get_reference_frame())

    def generate_init_pos(self):
        if self.cfg.symmetry_cfg:
            series = self.cfg.symmetry_cfg.get('series')
            if series:
                for serie in series:
                    if 'particles' in serie:
                        for base_particle_name, sym_particle_subseries in serie['particles'].items():
                            for subserie_i, subserie in enumerate(sym_particle_subseries):
                                try:
                                    self.sym_particle_lists[base_particle_name][subserie_i]
                                except IndexError:
                                    self.sym_particle_lists[base_particle_name].append({'particle_names': [], 'tr3ds': []})

                                for i, sym_particle_name in enumerate(subserie):
                                    if i == 0:
                                        if serie['inipos'] == 'input':
                                            sym_tr3d = IMP.algebra.get_identity_transformation_3d()
                                        else:
                                            sym_tr3d = self.cfg.get_tr3d_by_name(serie['inipos'])['tr3d']

                                    else:
                                        sym_tr3d = sym_tr3d * self.cfg.get_tr3d_by_name(serie['tr3d'])['tr3d']

                                    rbs = self.rigid_bodies[sym_particle_name]
                                    for rb in rbs:
                                        IMP.core.transform(rb, sym_tr3d)

                                    self.sym_particle_lists[base_particle_name][subserie_i]['particle_names'].append(sym_particle_name)
                                    self.sym_particle_lists[base_particle_name][subserie_i]['tr3ds'].append(sym_tr3d)

    def get_ori_chains(self, name):
        '''
        name - component or particle name
        '''
        try:
            return self.cfg.component_chains[name]
        except KeyError:
            chains = []
            for comp in self.cfg.particles_to_components[name]:
                chains.extend(self.cfg.component_chains[comp])
            return chains

    def create_copy_name(self, name, i):
        return name + '_' + str(i)

    def add_particle_and_create_copies(self, particle_base_name, add_missing_as_beads=False, resi_per_bead=100):
        full_seq = {}
        for comp in self.cfg.particles_to_components[particle_base_name]:
            full_seq[comp] = self.cfg.get_seq(comp)

        if self.cfg.symmetry_cfg:
            series = self.cfg.symmetry_cfg.get('series')
            if series:
                ori_chain_ids = self.get_ori_chains(particle_base_name)
                chain_ids = dict([(ori_chain_id, self.get_chain_generator(particle_base_name, ori_chain_id)) for ori_chain_id in ori_chain_ids])
                p_i = 0
                for serie in series:
                    if particle_base_name in serie['components']:
                        if 'particles' not in serie:
                            serie['particles'] = {}
                        if particle_base_name not in serie['particles']:
                            serie['particles'][particle_base_name] = []

                        cell_copies = serie['cell_count']

                        for i in range(0, cell_copies):

                            for cell_i in range(0, serie['particles_per_cell']):
                                try:
                                    serie['particles'][particle_base_name][cell_i]
                                except IndexError:
                                    serie['particles'][particle_base_name].append([])
                                rename_chains = {}
                                for ori_chain_id, new_chain_ids in chain_ids.items():
                                    rename_chains[ori_chain_id] = next(new_chain_ids)

                                particle_name = self.create_copy_name(particle_base_name, p_i)
                                self.particle_base_names_to_names[particle_base_name].append(particle_name)
                                if particle_base_name in self.cfg.frozen_particles:
                                    self.cfg.frozen_particles.append(particle_name)

                                serie['particles'][particle_base_name][cell_i].append(particle_name)

                                if self.cfg.get_pdbfilenames(particle_base_name) or (not self.cfg.get_pdbfilenames(particle_base_name) and particle_base_name not in self.cfg.domains):
                                    self.add_particle(
                                        self.cfg.get_pdbfilenames(particle_base_name),
                                        particle_name,
                                        rename_chains=rename_chains,
                                        add_missing_as_beads=add_missing_as_beads,
                                        particles_to_components=self.cfg.particles_to_components[particle_base_name],
                                        component_chains=copy.deepcopy(self.cfg.component_chains),
                                        full_seq=full_seq,
                                        resi_per_bead=resi_per_bead)

                                if particle_base_name in self.cfg.domains:
                                    for dom in self.cfg.domains[particle_base_name]:
                                        for span in dom['ranges']:
                                            start = span[0]
                                            end = span[1]
                                            dom_seq = full_seq[particle_base_name][start-1:end]
                                            self.add_particle(
                                                None,
                                                particle_name,
                                                rename_chains=rename_chains,
                                                resi_per_bead=dom['resi_per_bead'],
                                                particles_to_components=self.cfg.particles_to_components[particle_base_name],
                                                component_chains=copy.deepcopy(self.cfg.component_chains),
                                                start_resi=start,
                                                full_seq=dom_seq)

                                p_i = p_i + 1

                                for component_name in self.cfg.particles_to_components[particle_base_name]:
                                    self.cfg.components_to_particles[component_name].append(particle_name)

            if particle_base_name in self.cfg.frozen_particles:
                self.cfg.frozen_particles.pop(self.cfg.frozen_particles.index(particle_base_name))

    def load_em_map(self, em_map_name):
        em_map_cfg = None
        for data_item in self.cfg.data:
            if data_item['type'] == imp_utils.EM_DATA_TYPE and data_item['name'] == em_map_name:
                em_map_cfg = data_item
                break

        self.em_map = imp_utils.load_em_density_map(self.cfg.find_path(em_map_cfg['filename']), em_map_cfg['voxel_size'], em_map_cfg['resolution'])

    def add_em_restraint(self, em_map_name, em_restraint='FitRestraint', weight=100, threshold=None, first_copy_only=False):
        em_map_cfg = None
        for data_item in self.cfg.data:
            if data_item['type'] == imp_utils.EM_DATA_TYPE and data_item['name'] == em_map_name:
                em_map_cfg = data_item
                break

        if threshold is None:
            threshold = em_map_cfg['threshold']
        ps = []

        optimized_components = em_map_cfg['optimized_components']

        if optimized_components:
            for component_name in optimized_components:

                if first_copy_only:
                    first_copies_names = self.get_particle_first_copies_from_component_name(component_name)

                particles = self.get_molecule_names_for_component(component_name)

                for mol_name in particles:
                    if first_copy_only and mol_name not in first_copies_names:
                        continue

                    sele = IMP.atom.Selection(self.universe, molecule=mol_name).get_selected_particles()
                    for p in sele:
                        leaves = IMP.core.get_leaves(IMP.atom.Hierarchy(p))
                        ps.extend(leaves)

        else:
            ps.extend(IMP.core.get_leaves(self.universe))

        if em_restraint == 'EnvelopeFitRestraint':
            penetration_threshold = 30
            threshold = threshold

            em_r = IMP.em.EnvelopeFitRestraint(ps, self.em_map, threshold, penetration_threshold)

        elif em_restraint == 'EnvelopePenetrationRestraint':

            threshold = threshold

            # em_r= IMP.em.EnvelopePenetrationRestraint(IMP.core.get_leaves(self.universe), em_map, threshold)
            # ps = self.get_rigid_bodies()

            ps = [p.get_particle() for p in ps]
            em_r = IMP.em.EnvelopePenetrationRestraint(ps, self.em_map, threshold)

        elif em_restraint == 'FitRestraint':
            ps = [p.get_particle() for p in ps]
            k = 100
            em_r = IMP.em.FitRestraint(
                ps,
                self.em_map,
                [0., 0.],
                IMP.atom.Mass.get_mass_key(),
                k,
                False)
                # IMP.em.BINARIZED_SPHERE)

        self.em_restraint = em_r

        imp_utils.add_restraint(self.model, em_r, name=em_restraint, weight=weight)

    def write_model_map(self, filename):
        dmap = self.em_restraint.get_model_dens_map()
        print('sim. EM map resol', dmap.get_header().get_resolution())
        IMP.em.write_map(dmap, filename)

    def add_connectivity_restraints(self):
        '''
        For every fragment of "from domain",
        create connectivity_restraint to all fragments of "to domain" from all symmetrical copies.
        This is equivalent to use of OR restraint, since create_connectivity_restraint uses close pairs.
        '''
        for data in self.cfg.data:
            if data['type'] == CONNECTIVITY_DATA_TYPE and data['active']:
                for comp1, cfg in data['data'].items():
                    comp2 = cfg['connect_to']['name']
                    resi1 = cfg['resi']
                    resi2 = cfg['connect_to']['resi']
                    ps1 = self.get_particles_to_restrain_for_to_markers_restraints(comp1, resi1, first_copy_only=True)
                    ps2 = self.get_particles_to_restrain_for_to_markers_restraints(comp2, resi2, first_copy_only=False)
                    sel1 = IMP.atom.Selection(hierarchies=[IMP.atom.Hierarchy(p) for p in ps1])
                    sel2 = IMP.atom.Selection(hierarchies=[IMP.atom.Hierarchy(p) for p in ps2])
                    x0 = 0
                    if len(sel1.get_selected_particles()) > 0 and len(sel2.get_selected_particles()) > 0:
                        r = IMP.atom.create_connectivity_restraint([sel1, sel2], x0)
                        imp_utils.add_restraint(self.model, r, name='connectivity ' + data['name'] + ' from ' + comp1 + ' to ' + comp2, weight=data['weight'])

    def add_symmetry_restraints(self, weight=1000):
        ''''TODO: if related rigid_bodies don't have the same number of members,
        this will not work'''
        self.sym_restraints = []

        for name, sym_particle_names, rb_set, sym_tr3ds in self.iter_movable_rb_sets():
            rb_ref = rb_set[0]
            rb_ref_members = rb_ref.get_rigid_members()

            for rb, sym_tr3d in zip(rb_set[1:], sym_tr3ds[1:]):
                pl = IMP.container.ListPairContainer(self.model)

                rb_members = rb.get_rigid_members()

                all_pairs = list(zip(rb_ref_members, rb_members))
                if len(all_pairs) > 3:
                    random_pairs = random.sample(all_pairs, 3)
                else:
                    random_pairs = all_pairs

                for p1, p2 in random_pairs:
                    pl.add_particle_pair((p2, p1))

                tp = IMP.core.TransformedDistancePairScore(IMP.core.Harmonic(0, 1), sym_tr3d)
                pr = IMP.container.PairsRestraint(tp, pl, 'Symmetry restraint')

                pr.set_weight(weight)
                self.model.add_restraint(pr)
                self.sym_restraints.append(pr)
